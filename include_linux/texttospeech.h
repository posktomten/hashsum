// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          texttospeech
//          Copyright (C) 2022 -2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This library is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef LIBTEXTTOSPEECH_H
#define LIBTEXTTOSPEECH_H


#include <QTextToSpeech>
#include <QLocale>
#include <QObject>
#include <QDebug>
#include <QVoice>


//#if defined(Q_OS_LINUX)
//#if defined(ABOUT_LIBRARY)
//#include "about_global.h"
//class ABOUT_EXPORT About : public QWidget
//#else
//class About : public QWidget
//#endif
//#else
//class About : public QWidget
//#endif // Q_OS_LINUX

#if defined(Q_OS_LINUX)
#if defined(LIBTEXTTOSPEECH_LIBRARY)
#include "texttospeech_global.h"
class LIBTEXTTOSPEECH_EXPORT Libtexttospeech : public QObject
#else
class Libtexttospeech : public QObject
#endif // LIBTEXTTOSPEECH_LIBRARY
#else
class Libtexttospeech : public QObject
#endif // Q_OS_LINUX)
{
    Q_OBJECT
public:

    Libtexttospeech();
    ~Libtexttospeech();
    void setText(const QString *text);
    void setPitch(const int inputPitch);
    void setRate(const int inputRate);
    void setVolume(const int inputVolume);
    void setStop();
    void setPause();
    void setResume();
    void setLocale(QLocale locale);

    void setVoice(QVoice voice);
//            void setEngine(QTextToSpeech endgine);
    QVector<QLocale> getLocales();
    QVector<QVoice> getVoices();



private slots:
    void getStateHasChenged(QTextToSpeech::State state);
public slots:
    void finish();


signals:
    void stateHasChenged(QString *state);

private:
    QTextToSpeech *speech;

};

#endif // LIBTEXTTOSPEECH_H
