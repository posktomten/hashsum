<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="41"/>
        <location filename="../checkforupdates.cpp" line="85"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="51"/>
        <source> is newer than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>
There is a later version of </source>
        <oldsource>
There is a newer version of </oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Please </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <source>
There is a newer version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="51"/>
        <source>
Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <source>
You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="59"/>
        <location filename="../checkforupdates.cpp" line="92"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="129"/>
        <source>New updates:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="764"/>
        <source>Double-click to copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="135"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="145"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="154"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="163"/>
        <source>&amp;Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="407"/>
        <source>Hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="195"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="183"/>
        <source>Check file or files</source>
        <oldsource>Check file</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="204"/>
        <location filename="../hash.ui" line="207"/>
        <location filename="../hash.ui" line="210"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="218"/>
        <source>Check for program updates at program start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="227"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="230"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="238"/>
        <source>MD4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="246"/>
        <source>MD5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="254"/>
        <source>SHA1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="259"/>
        <source>Compare two files</source>
        <oldsource>Compare</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="268"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="277"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="282"/>
        <location filename="../hash.ui" line="285"/>
        <source>Create hash sum list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="293"/>
        <source>Save hash sum list where the files are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="298"/>
        <source>Default file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="303"/>
        <source>Create hash sum lists, recursively</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="311"/>
        <source>Write date and time in the hash sum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="320"/>
        <source>Find changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="328"/>
        <source>Use native dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="336"/>
        <source>Allways open the home directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="341"/>
        <location filename="../hash.ui" line="412"/>
        <source>Compare with a given hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="349"/>
        <source>Save as text (*.*)</source>
        <oldsource>Save as text (*)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="357"/>
        <source>Save as pdf (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="365"/>
        <source>Color pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="377"/>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="389"/>
        <source>Copy Hash Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="397"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <oldsource>Save as text and pdf (* and *.pdf)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="402"/>
        <source>Hash sum from text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="421"/>
        <location filename="../hash.ui" line="424"/>
        <location filename="../hash.ui" line="427"/>
        <source>Help...</source>
        <oldsource>Help</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="433"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="442"/>
        <source>Create Debian md5sums</source>
        <oldsource>Create debian md5sums</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="450"/>
        <source>Show full path in the hash sum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="459"/>
        <source>Create Debian md5sums (auto)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="468"/>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="476"/>
        <source>SHA224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="484"/>
        <source>SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="492"/>
        <source>SHA384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="500"/>
        <source>SHA512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="508"/>
        <source>SHA3_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="516"/>
        <source>SHA3_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="524"/>
        <source>SHA3_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="535"/>
        <source>SHA3_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="544"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="553"/>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="558"/>
        <source>Open Hash Sum List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="563"/>
        <source>Open Comparison list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="572"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="580"/>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="588"/>
        <source>Keccak_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="596"/>
        <source>Keccak_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="604"/>
        <source>Keccak_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="612"/>
        <source>Keccak_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="617"/>
        <source>Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="622"/>
        <source>Are there duplicates?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="630"/>
        <source>Always open the display window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="771"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="800"/>
        <location filename="../hash.cpp" line="1615"/>
        <source>Open a file to calculate the hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="548"/>
        <location filename="../hash.cpp" line="802"/>
        <location filename="../hash.cpp" line="1617"/>
        <location filename="../hash.cpp" line="1735"/>
        <location filename="../hash.cpp" line="1777"/>
        <location filename="../hash.cpp" line="1842"/>
        <location filename="../hash.cpp" line="2024"/>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="834"/>
        <location filename="../hash.cpp" line="1658"/>
        <location filename="../hash.cpp" line="1667"/>
        <location filename="../hash.cpp" line="2263"/>
        <location filename="../hash.cpp" line="3494"/>
        <location filename="../hash.cpp" line="3501"/>
        <location filename="../isthereaduplicate.cpp" line="78"/>
        <source>No hash sum could be calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="837"/>
        <location filename="../hash.cpp" line="840"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="837"/>
        <source>&quot; and they are the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="838"/>
        <source>The hash sums are equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="840"/>
        <source>&quot; and they are NOT the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="841"/>
        <source>The hash sums are NOT equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1027"/>
        <source>Phone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1034"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1034"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1058"/>
        <location filename="../hash.cpp" line="1094"/>
        <location filename="../hash.cpp" line="1130"/>
        <location filename="../hash.cpp" line="1166"/>
        <location filename="../hash.cpp" line="1202"/>
        <location filename="../hash.cpp" line="1238"/>
        <location filename="../hash.cpp" line="1274"/>
        <location filename="../hash.cpp" line="1310"/>
        <location filename="../hash.cpp" line="1346"/>
        <location filename="../hash.cpp" line="1382"/>
        <location filename="../hash.cpp" line="1418"/>
        <location filename="../hash.cpp" line="1453"/>
        <location filename="../hash.cpp" line="1491"/>
        <location filename="../hash.cpp" line="1527"/>
        <location filename="../hash.cpp" line="1563"/>
        <location filename="../hash.cpp" line="1697"/>
        <location filename="../hash.cpp" line="2329"/>
        <location filename="../hash.cpp" line="2638"/>
        <location filename="../hash.cpp" line="2706"/>
        <location filename="../hash.cpp" line="2791"/>
        <location filename="../hash.cpp" line="3025"/>
        <location filename="../hash.cpp" line="3030"/>
        <location filename="../hash.cpp" line="3374"/>
        <location filename="../hash.cpp" line="3528"/>
        <location filename="../hash.cpp" line="3648"/>
        <source>Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1078"/>
        <location filename="../hash.cpp" line="1114"/>
        <location filename="../hash.cpp" line="1150"/>
        <location filename="../hash.cpp" line="1186"/>
        <location filename="../hash.cpp" line="1222"/>
        <location filename="../hash.cpp" line="1258"/>
        <location filename="../hash.cpp" line="1294"/>
        <location filename="../hash.cpp" line="1330"/>
        <location filename="../hash.cpp" line="1366"/>
        <location filename="../hash.cpp" line="1402"/>
        <location filename="../hash.cpp" line="1438"/>
        <location filename="../hash.cpp" line="1473"/>
        <location filename="../hash.cpp" line="1511"/>
        <location filename="../hash.cpp" line="1547"/>
        <location filename="../hash.cpp" line="1583"/>
        <location filename="../hash.cpp" line="1991"/>
        <location filename="../hash.cpp" line="1998"/>
        <location filename="../hash.cpp" line="3373"/>
        <source>Default file name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1733"/>
        <source>Open two files to compare their hash sums.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1758"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1775"/>
        <source>Open the second file to compare with.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1792"/>
        <source>You must select exactly one file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1803"/>
        <source>You have compared the same file with itself.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1817"/>
        <source>The files could not be compared.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1840"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1871"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1881"/>
        <location filename="../hash.cpp" line="1904"/>
        <location filename="../hash.cpp" line="1928"/>
        <location filename="../hash.cpp" line="1952"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1882"/>
        <location filename="../hash.cpp" line="1905"/>
        <location filename="../hash.cpp" line="1929"/>
        <location filename="../hash.cpp" line="1953"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="636"/>
        <location filename="../hash.cpp" line="653"/>
        <location filename="../hash.cpp" line="713"/>
        <location filename="../hash.cpp" line="781"/>
        <location filename="../hash.cpp" line="1883"/>
        <location filename="../hash.cpp" line="1906"/>
        <location filename="../hash.cpp" line="1930"/>
        <location filename="../hash.cpp" line="1954"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="692"/>
        <location filename="../hash.cpp" line="694"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="711"/>
        <source>Calculate the hash sum of this text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="637"/>
        <location filename="../hash.cpp" line="654"/>
        <location filename="../hash.cpp" line="714"/>
        <location filename="../hash.cpp" line="782"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="219"/>
        <location filename="../hash.cpp" line="346"/>
        <source> is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="148"/>
        <location filename="../hash.cpp" line="231"/>
        <location filename="../hash.cpp" line="358"/>
        <source>Open the hashSum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="150"/>
        <location filename="../hash.cpp" line="233"/>
        <location filename="../hash.cpp" line="360"/>
        <source>Text files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="183"/>
        <location filename="../hash.cpp" line="267"/>
        <location filename="../hash.cpp" line="307"/>
        <location filename="../hash.cpp" line="401"/>
        <location filename="../hash.cpp" line="436"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="468"/>
        <source>The number of recently opened files to be displayed...</source>
        <oldsource>Set number of recently opened files shown...</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="471"/>
        <source>Clear the list of recently opened files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="479"/>
        <source>The file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="479"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="510"/>
        <source>Set number of Recent Files: </source>
        <oldsource>Set number of Recent Files: (0-30)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="546"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <oldsource>Select the &quot;usr&quot; directory to start create checksums recursively</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="580"/>
        <location filename="../hash.cpp" line="2161"/>
        <location filename="../hash.cpp" line="2541"/>
        <location filename="../hash.cpp" line="2634"/>
        <location filename="../hash.cpp" line="2702"/>
        <location filename="../hash.cpp" line="3455"/>
        <location filename="../hash.cpp" line="3584"/>
        <location filename="../hash.cpp" line="3641"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="594"/>
        <source>md5sums file has been successfully created!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="597"/>
        <source>md5sums file has been successfully created in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="625"/>
        <source>Help with the program and all settings and menus can be found here:</source>
        <oldsource>Help with the program and all settings and menus can be found here:&lt;br&gt;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="625"/>
        <source>The program&apos;s website can be found here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="634"/>
        <source>Compare with this hash sum...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="651"/>
        <source>...with hash sum for this text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="663"/>
        <location filename="../hash.cpp" line="727"/>
        <source> can handle texts up to </source>
        <oldsource> can handle texts of up to </oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="663"/>
        <location filename="../hash.cpp" line="727"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="671"/>
        <location filename="../hash.cpp" line="692"/>
        <location filename="../hash.cpp" line="694"/>
        <location filename="../hash.cpp" line="697"/>
        <location filename="../hash.cpp" line="699"/>
        <location filename="../hash.cpp" line="735"/>
        <location filename="../hash.cpp" line="744"/>
        <location filename="../hash.cpp" line="746"/>
        <location filename="../hash.cpp" line="749"/>
        <location filename="../hash.cpp" line="751"/>
        <source>Hash sum calculated for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="671"/>
        <location filename="../hash.cpp" line="735"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="676"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="677"/>
        <source>Hash sums are NOT equal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="681"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="682"/>
        <source>Hash sums are equal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="692"/>
        <location filename="../hash.cpp" line="697"/>
        <location filename="../hash.cpp" line="744"/>
        <location filename="../hash.cpp" line="749"/>
        <source>characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="694"/>
        <location filename="../hash.cpp" line="699"/>
        <location filename="../hash.cpp" line="746"/>
        <location filename="../hash.cpp" line="751"/>
        <source>characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="702"/>
        <source>Hash sums are compared!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="744"/>
        <location filename="../hash.cpp" line="746"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="754"/>
        <source>Hash sum has been calculated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="779"/>
        <source>Compare with this hash sum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="932"/>
        <source>The license file is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="979"/>
        <source>The version history file is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1024"/>
        <source> for the Greek translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1024"/>
        <location filename="../hash.cpp" line="1025"/>
        <source>Many thanks to </source>
        <oldsource>Many thanks to &lt;a href=&quot;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1025"/>
        <source> for the German translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1035"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="22"/>
        <source>This program uses Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="22"/>
        <source> running on </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="24"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="37"/>
        <location filename="../system.cpp" line="41"/>
        <location filename="../system.cpp" line="45"/>
        <location filename="../system.cpp" line="121"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="53"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1078"/>
        <location filename="../hash.cpp" line="1114"/>
        <location filename="../hash.cpp" line="1150"/>
        <location filename="../hash.cpp" line="1186"/>
        <location filename="../hash.cpp" line="1222"/>
        <location filename="../hash.cpp" line="1258"/>
        <location filename="../hash.cpp" line="1294"/>
        <location filename="../hash.cpp" line="1330"/>
        <location filename="../hash.cpp" line="1366"/>
        <location filename="../hash.cpp" line="1402"/>
        <location filename="../hash.cpp" line="1438"/>
        <location filename="../hash.cpp" line="1473"/>
        <location filename="../hash.cpp" line="1511"/>
        <location filename="../hash.cpp" line="1547"/>
        <location filename="../hash.cpp" line="1583"/>
        <source>(Click to change)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1987"/>
        <source>Default file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1991"/>
        <location filename="../hash.cpp" line="1998"/>
        <location filename="../hash.cpp" line="3373"/>
        <source> (Click to change)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2022"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2073"/>
        <location filename="../hash.cpp" line="2095"/>
        <location filename="../hash.cpp" line="2113"/>
        <source>Did you select all files you want to choose?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2074"/>
        <location filename="../hash.cpp" line="2096"/>
        <location filename="../hash.cpp" line="2114"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2075"/>
        <location filename="../hash.cpp" line="2097"/>
        <location filename="../hash.cpp" line="2115"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2135"/>
        <source>Save the Debian md5sums file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2169"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="43"/>
        <location filename="../isthereaduplicate.cpp" line="91"/>
        <source>Select a folder to Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="45"/>
        <location filename="../isthereaduplicate.cpp" line="93"/>
        <source>All Files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="121"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2221"/>
        <location filename="../hash.cpp" line="2227"/>
        <location filename="../hash.cpp" line="2229"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source>I have been looking into </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2227"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2221"/>
        <location filename="../hash.cpp" line="2227"/>
        <location filename="../hash.cpp" line="2229"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source>(Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2221"/>
        <location filename="../hash.cpp" line="2229"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="111"/>
        <source>Path: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="226"/>
        <location filename="../isthereaduplicate.cpp" line="183"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="228"/>
        <source>Recursively from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="251"/>
        <source>The number of files with one or more duplicates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="255"/>
        <location filename="../isthereaduplicate.cpp" line="203"/>
        <source>Duplicate files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3563"/>
        <location filename="../hash.cpp" line="3620"/>
        <source>Save the hash summary list as a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3563"/>
        <location filename="../hash.cpp" line="3620"/>
        <source>Any file (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1696"/>
        <location filename="../hash.cpp" line="2223"/>
        <location filename="../hash.cpp" line="3527"/>
        <location filename="../isthereaduplicate.cpp" line="154"/>
        <source>Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2231"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="122"/>
        <location filename="../hash.cpp" line="2234"/>
        <location filename="../isthereaduplicate.cpp" line="165"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="122"/>
        <location filename="../hash.cpp" line="2234"/>
        <location filename="../isthereaduplicate.cpp" line="165"/>
        <source> files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2589"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="855"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2378"/>
        <location filename="../hash.cpp" line="2608"/>
        <source>Text Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="24"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="117"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="126"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2175"/>
        <location filename="../hash.cpp" line="2341"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2718"/>
        <source> No hash sum could be calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2246"/>
        <source>This is not a valid path to build a Debian package!</source>
        <oldsource>This is not a valid path to build a debian package!!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2332"/>
        <source>Files in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2350"/>
        <location filename="../hash.cpp" line="2589"/>
        <location filename="../hash.cpp" line="2657"/>
        <location filename="../hash.cpp" line="2674"/>
        <location filename="../hash.cpp" line="2725"/>
        <location filename="../hash.cpp" line="2734"/>
        <source>Hash sum for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2350"/>
        <location filename="../hash.cpp" line="2589"/>
        <location filename="../hash.cpp" line="2657"/>
        <location filename="../hash.cpp" line="2674"/>
        <location filename="../hash.cpp" line="2725"/>
        <location filename="../hash.cpp" line="2734"/>
        <source> of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2350"/>
        <location filename="../hash.cpp" line="2657"/>
        <location filename="../hash.cpp" line="2725"/>
        <source> files was successfully calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2351"/>
        <location filename="../hash.cpp" line="2573"/>
        <location filename="../hash.cpp" line="2658"/>
        <location filename="../hash.cpp" line="2726"/>
        <source>Copyright </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="227"/>
        <location filename="../hash.cpp" line="2354"/>
        <location filename="../hash.cpp" line="2576"/>
        <location filename="../hash.cpp" line="2661"/>
        <location filename="../hash.cpp" line="2729"/>
        <location filename="../isthereaduplicate.cpp" line="191"/>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2376"/>
        <source>Open the old hash sum file to compare with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2403"/>
        <source>Hash sum could not be estimated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2413"/>
        <source>Could not open &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2413"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2428"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2545"/>
        <source>Unchanged files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2552"/>
        <source>Changed files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2566"/>
        <source>New files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2588"/>
        <location filename="../hash.cpp" line="2673"/>
        <location filename="../hash.cpp" line="2733"/>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2589"/>
        <location filename="../hash.cpp" line="2674"/>
        <location filename="../hash.cpp" line="2734"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2134"/>
        <source>Files (*.*)</source>
        <oldsource>Files (*)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2559"/>
        <source>Files not tested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2609"/>
        <source>Save the file with the hash sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2791"/>
        <source>&quot; has hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3025"/>
        <location filename="../hash.cpp" line="3030"/>
        <source>&quot; and &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3025"/>
        <location filename="../hash.cpp" line="3030"/>
        <source>&quot; have these hash sums:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3025"/>
        <source>. The files are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3026"/>
        <source>The files are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3030"/>
        <source>. The files are NOT identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3031"/>
        <source>The files are NOT identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3034"/>
        <location filename="../hash.cpp" line="3038"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="41"/>
        <source>Open the file you want to find duplicates for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="192"/>
        <source>Identical files with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="193"/>
        <source>Recursively searched from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="199"/>
        <source>I found </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="199"/>
        <source> duplicates.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
