<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="50"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licens:</translation>
    </message>
    <message>
        <source> GPL Version 3</source>
        <translation type="vanished"> GPL Version 3</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">Copyright ©</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>Copyright © </source>
        <translation>Copyright © </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>License: </source>
        <translation>Licens: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>distribueras i hopp om att den ska vara användbar, men UTAN NÅGON GARANTI; utan ens underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR ETT SÄRSKILT SYFTE.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>See the GNU General Public License version 3 for more details.</source>
        <translation>Se GNU General Public License version 3 för mer information.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Website</source>
        <translation>Webbsida</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="55"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <source>Running on: </source>
        <translation type="vanished">Körs på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Compiled on: </source>
        <translation>Kompilerad på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source> is in the folder:</source>
        <translation> finns i foldern:</translation>
    </message>
    <message>
        <source> is in the folder</source>
        <translation type="vanished"> finns i mappen</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Runs on: </source>
        <translation>Körs på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="70"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Kompilator: MinGW (GCC för Windows) version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="76"/>
        <source>Compiler: Micrsoft Visual C++ version </source>
        <translation>Kompilator: Micrsoft Visual C++ version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="77"/>
        <source>&lt;br&gt;Full version number: </source>
        <translation>&lt;br&gt;Fullständigt versionsnummer: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <location filename="../about.cpp" line="82"/>
        <location filename="../about.cpp" line="84"/>
        <location filename="../about.cpp" line="86"/>
        <location filename="../about.cpp" line="88"/>
        <source>Programming language: C++</source>
        <translation>Programmeringsspråk: C++</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <source>C++ version: C++20</source>
        <translation>C++ version: C++20</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="82"/>
        <source>C++ version: C++17</source>
        <translation>C++ version: C++17</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>C++ version: C++14</source>
        <translation>C++ version: C++14</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>C++ version: C++11</source>
        <translation>C++ version: C++11</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="88"/>
        <source>C++ version: Unknown</source>
        <translation>C++ version: Okänd</translation>
    </message>
    <message>
        <source>Version: C++20</source>
        <translation type="vanished">Version: C++20</translation>
    </message>
    <message>
        <source>Version: C++17</source>
        <translation type="vanished">Version: C++17</translation>
    </message>
    <message>
        <source>Version: C++14</source>
        <translation type="vanished">Version: C++14</translation>
    </message>
    <message>
        <source>Version: C++11</source>
        <translation type="vanished">Version: C++11</translation>
    </message>
    <message>
        <source>Version: Unknown</source>
        <translation type="vanished">Version: Okänd</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <source>Application framework: Qt version </source>
        <translation>Applikationsramverk: Qt version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="92"/>
        <source>Processor architecture: 32-bit</source>
        <translation>Processorarkitektur: 32-bitar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="95"/>
        <source>Processor architecture: 64-bit</source>
        <translation>Processorarkitektur: 64-bitar</translation>
    </message>
</context>
</TS>
