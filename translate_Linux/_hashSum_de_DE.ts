<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="41"/>
        <location filename="../checkforupdates.cpp" line="85"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Es wurde keine Internetverbindung gefunden.
Bitte überprüfen Sie Ihre Interneteinstellungen und Firewall.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="51"/>
        <source> is newer than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>
There is a later version of </source>
        <oldsource>
There is a newer version of </oldsource>
        <translation>
Es gibt eine spätere Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Latest version: </source>
        <translation>Neueste Version: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Please </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <location filename="../checkforupdates.cpp" line="90"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="47"/>
        <source>
There is a newer version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="51"/>
        <source>
Your version of </source>
        <translation>
Ihre Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <source>
You have the latest version of </source>
        <translation>
Sie haben die neueste Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="59"/>
        <location filename="../checkforupdates.cpp" line="92"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Es gab einen Fehler bei der Überprüfung der Version.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="129"/>
        <source>New updates:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation>Mission</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumme wurde in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Doppelklicken Sie zum Kopieren in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="967"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Doppelklicken Sie zum Kopieren in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation>&amp;Algorithmus</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="140"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="151"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="160"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="169"/>
        <source>&amp;Text</source>
        <translation>&amp;Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="416"/>
        <source>Hash sum</source>
        <translation>Hashsumme</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="201"/>
        <source>Check for updates</source>
        <translation>Nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation>Letzte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="189"/>
        <source>Check file or files</source>
        <oldsource>Check file</oldsource>
        <translation>Datei prüfen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="210"/>
        <location filename="../hash.ui" line="213"/>
        <location filename="../hash.ui" line="216"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation>Über...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="224"/>
        <source>Check for program updates at program start</source>
        <translation>Beim Programmstart nach Programmaktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="236"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="239"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="247"/>
        <source>MD4</source>
        <translation>MD4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="255"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="263"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="268"/>
        <source>Compare two files</source>
        <oldsource>Compare</oldsource>
        <translation>Zwei Dateien vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="277"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="286"/>
        <source>Swedish</source>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="291"/>
        <location filename="../hash.ui" line="294"/>
        <source>Create hash sum list</source>
        <translation>Hashsummenliste erstellen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="302"/>
        <source>Save hash sum list where the files are</source>
        <translation>Hashsummenliste dort speichern, wo sich die Dateien befinden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="307"/>
        <source>Default file name</source>
        <translation>Standard-Dateiname</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="312"/>
        <source>Create hash sum lists, recursively</source>
        <translation>Hashsummenlisten erstellen, rekursiv</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="320"/>
        <source>Write date and time in the hash sum file</source>
        <translation>Datum und Zeit in die Hashsummendatei schreiben</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="329"/>
        <source>Find changes</source>
        <translation>Änderungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="337"/>
        <source>Use native dialogs</source>
        <translation>Native Dialoge verwenden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="345"/>
        <source>Allways open the home directory</source>
        <translation>Benutzerverzeichnis immer öffnen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="350"/>
        <location filename="../hash.ui" line="421"/>
        <source>Compare with a given hash sum</source>
        <translation>Mit einer gegebenen Hashsumme vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="358"/>
        <source>Save as text (*.*)</source>
        <oldsource>Save as text (*)</oldsource>
        <translation>Als Text speichern (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="366"/>
        <source>Save as pdf (*.pdf)</source>
        <translation>Als pdf speichern (*.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="374"/>
        <source>Color pdf</source>
        <translation>Farb-PDF</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="386"/>
        <source>Copy Path</source>
        <translation>Pfad kopieren</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="398"/>
        <source>Copy Hash Sum</source>
        <translation>Hashsumme kopieren</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="406"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <oldsource>Save as text and pdf (* and *.pdf)</oldsource>
        <translation>Als Text und pdf speichern (*.* und *.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="411"/>
        <source>Hash sum from text</source>
        <translation>Hashsumme aus Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="430"/>
        <location filename="../hash.ui" line="433"/>
        <location filename="../hash.ui" line="436"/>
        <source>Help...</source>
        <oldsource>Help</oldsource>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="442"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="451"/>
        <source>Create Debian md5sums</source>
        <oldsource>Create debian md5sums</oldsource>
        <translation>Debian md5sums erstellen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="459"/>
        <source>Show full path in the hash sum file</source>
        <translation>Vollständigen Pfad in der Hashsummendatei anzeigen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="468"/>
        <source>Create Debian md5sums (auto)</source>
        <translation>Debian md5sums erstellen (automatisch)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="477"/>
        <source>Greek (Not complete)</source>
        <translation>Griechisch (Not complete)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="581"/>
        <source>German (Not complete)</source>
        <translation>Deutsche (Not complete)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="485"/>
        <source>SHA224</source>
        <translation>SHA224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="493"/>
        <source>SHA256</source>
        <translation>SHA256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="501"/>
        <source>SHA384</source>
        <translation>SHA384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="509"/>
        <source>SHA512</source>
        <translation>SHA512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="517"/>
        <source>SHA3_224</source>
        <translation>SHA3_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="525"/>
        <source>SHA3_256</source>
        <translation>SHA3_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="533"/>
        <source>SHA3_384</source>
        <translation>SHA3_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="544"/>
        <source>SHA3_512</source>
        <translation>SHA3_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="553"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="562"/>
        <source>Version history</source>
        <translation>Versionshistorie</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="567"/>
        <source>Open Hash Sum List</source>
        <translation>Hashsummenliste öffnen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="572"/>
        <source>Open Comparison list</source>
        <translation>Vergleichsliste öffnen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="589"/>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="597"/>
        <source>Keccak_224</source>
        <translation>Keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="605"/>
        <source>Keccak_256</source>
        <translation>Keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="613"/>
        <source>Keccak_384</source>
        <translation>Keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="621"/>
        <source>Keccak_512</source>
        <translation>Keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="626"/>
        <source>Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="631"/>
        <source>Are there duplicates?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="639"/>
        <source>Always open the display window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="651"/>
        <location filename="../hash.cpp" line="246"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="662"/>
        <source>Mainteance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="673"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="693"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="973"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumme wurde in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1000"/>
        <location filename="../hash.cpp" line="1778"/>
        <source>Open a file to calculate the hash sum</source>
        <translation>Öffnen Sie eine Datei, um die Hashsumme zu berechnen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="756"/>
        <location filename="../hash.cpp" line="1002"/>
        <location filename="../hash.cpp" line="1780"/>
        <location filename="../hash.cpp" line="1893"/>
        <location filename="../hash.cpp" line="1935"/>
        <location filename="../hash.cpp" line="2000"/>
        <location filename="../hash.cpp" line="2182"/>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>All Files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1034"/>
        <location filename="../hash.cpp" line="1819"/>
        <location filename="../hash.cpp" line="1828"/>
        <location filename="../hash.cpp" line="2421"/>
        <location filename="../hash.cpp" line="3677"/>
        <location filename="../hash.cpp" line="3684"/>
        <location filename="../isthereaduplicate.cpp" line="78"/>
        <source>No hash sum could be calculated</source>
        <translation>Es konnte keine Hashsumme berechnet werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1037"/>
        <location filename="../hash.cpp" line="1040"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation>Und die Hashsumme, die Sie vergleichen, ist: &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1037"/>
        <source>&quot; and they are the same</source>
        <translation>&quot; und sie sind gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1038"/>
        <source>The hash sums are equal</source>
        <translation>Die Hashsummen sind gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1040"/>
        <source>&quot; and they are NOT the same</source>
        <translation>&quot; und sie sind NICHT gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1041"/>
        <source>The hash sums are NOT equal</source>
        <translation>Die Hashsummen sind NICHT gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1211"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1218"/>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1218"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1239"/>
        <location filename="../hash.cpp" line="1274"/>
        <location filename="../hash.cpp" line="1309"/>
        <location filename="../hash.cpp" line="1344"/>
        <location filename="../hash.cpp" line="1379"/>
        <location filename="../hash.cpp" line="1414"/>
        <location filename="../hash.cpp" line="1449"/>
        <location filename="../hash.cpp" line="1484"/>
        <location filename="../hash.cpp" line="1519"/>
        <location filename="../hash.cpp" line="1554"/>
        <location filename="../hash.cpp" line="1589"/>
        <location filename="../hash.cpp" line="1624"/>
        <location filename="../hash.cpp" line="1661"/>
        <location filename="../hash.cpp" line="1696"/>
        <location filename="../hash.cpp" line="1731"/>
        <location filename="../hash.cpp" line="1856"/>
        <location filename="../hash.cpp" line="2487"/>
        <location filename="../hash.cpp" line="2796"/>
        <location filename="../hash.cpp" line="2864"/>
        <location filename="../hash.cpp" line="2949"/>
        <location filename="../hash.cpp" line="3183"/>
        <location filename="../hash.cpp" line="3188"/>
        <location filename="../hash.cpp" line="3548"/>
        <location filename="../hash.cpp" line="3710"/>
        <location filename="../hash.cpp" line="3828"/>
        <source>Algorithm: </source>
        <translation>Algorithmus: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1259"/>
        <location filename="../hash.cpp" line="1294"/>
        <location filename="../hash.cpp" line="1329"/>
        <location filename="../hash.cpp" line="1364"/>
        <location filename="../hash.cpp" line="1399"/>
        <location filename="../hash.cpp" line="1434"/>
        <location filename="../hash.cpp" line="1469"/>
        <location filename="../hash.cpp" line="1504"/>
        <location filename="../hash.cpp" line="1539"/>
        <location filename="../hash.cpp" line="1574"/>
        <location filename="../hash.cpp" line="1609"/>
        <location filename="../hash.cpp" line="1644"/>
        <location filename="../hash.cpp" line="1681"/>
        <location filename="../hash.cpp" line="1716"/>
        <location filename="../hash.cpp" line="1751"/>
        <location filename="../hash.cpp" line="2149"/>
        <location filename="../hash.cpp" line="2156"/>
        <location filename="../hash.cpp" line="3547"/>
        <source>Default file name: </source>
        <translation>Standard-Dateiname: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1891"/>
        <source>Open two files to compare their hash sums.</source>
        <translation>Öffnen Sie zwei Dateien, um ihre Hashsummen zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1916"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Sie müssen genau zwei Dateien auswählen.
Zwei Dateien in diesem Ordner oder eine Datei in diesem Ordner und eine zweite Datei in einem anderen Ordner Ihrer Wahl.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1933"/>
        <source>Open the second file to compare with.</source>
        <translation>Öffnen Sie die zweite Datei, um sie zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1950"/>
        <source>You must select exactly one file.</source>
        <translation>Sie müssen genau eine Datei auswählen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1961"/>
        <source>You have compared the same file with itself.</source>
        <translation>Sie haben die gleiche Datei mit sich selbst verglichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1975"/>
        <source>The files could not be compared.</source>
        <translation>Die Dateien konnten nicht verglichen werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1998"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation>Wählen Sie einen Ordner aus, um Hashsummenlisten rekursiv zu erzeugen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2029"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <translation>Es konnte keine Datei zum Ablegen von Hashsummen gespeichert werden. Überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2039"/>
        <location filename="../hash.cpp" line="2062"/>
        <location filename="../hash.cpp" line="2086"/>
        <location filename="../hash.cpp" line="2110"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Das Programm muss neu gestartet werden, damit die neuen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2040"/>
        <location filename="../hash.cpp" line="2063"/>
        <location filename="../hash.cpp" line="2087"/>
        <location filename="../hash.cpp" line="2111"/>
        <source>Restart Now</source>
        <translation>Jetzt neustarten</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="244"/>
        <location filename="../hash.cpp" line="842"/>
        <location filename="../hash.cpp" line="859"/>
        <location filename="../hash.cpp" line="918"/>
        <location filename="../hash.cpp" line="981"/>
        <location filename="../hash.cpp" line="2041"/>
        <location filename="../hash.cpp" line="2064"/>
        <location filename="../hash.cpp" line="2088"/>
        <location filename="../hash.cpp" line="2112"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="898"/>
        <location filename="../hash.cpp" line="900"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Leerzeichen wird nicht entfernt, wenn die Hashsummen berechnet werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="916"/>
        <source>Calculate the hash sum of this text:</source>
        <translation>Hashsumme dieses Textes berechnen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="843"/>
        <location filename="../hash.cpp" line="860"/>
        <location filename="../hash.cpp" line="919"/>
        <location filename="../hash.cpp" line="982"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="440"/>
        <location filename="../hash.cpp" line="561"/>
        <source> is not found</source>
        <translation> wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="370"/>
        <location filename="../hash.cpp" line="452"/>
        <location filename="../hash.cpp" line="573"/>
        <source>Open the hashSum file</source>
        <translation>hashSum-Datei öffnen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="40"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="240"/>
        <source>To the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="243"/>
        <source>Download the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="248"/>
        <source>Download the new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="269"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="271"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="271"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="317"/>
        <source>&quot;Control Panel\All Control Panel Items\Programs and Features&quot; cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="332"/>
        <source>MaintenanceTool cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="372"/>
        <location filename="../hash.cpp" line="454"/>
        <location filename="../hash.cpp" line="575"/>
        <source>Text files (*)</source>
        <translation>Textdateien (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="405"/>
        <location filename="../hash.cpp" line="488"/>
        <location filename="../hash.cpp" line="528"/>
        <location filename="../hash.cpp" line="616"/>
        <location filename="../hash.cpp" line="651"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="681"/>
        <source>The number of recently opened files to be displayed...</source>
        <oldsource>Set number of recently opened files shown...</oldsource>
        <translation>Die Anzahl der angezeigten zuletzt geöffneten Dateien...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="684"/>
        <source>Clear the list of recently opened files</source>
        <translation>Liste der zuletzt geöffneten Dateien löschen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="691"/>
        <source>The file</source>
        <translation>Die Datei</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="691"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation>kann nicht gefunden werden. Die Datei wird aus der Liste der zuletzt geöffneten Dateien entfernt.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="720"/>
        <source>Set number of Recent Files: </source>
        <oldsource>Set number of Recent Files: (0-30)</oldsource>
        <translation>Anzahl der letzten Dateien festlegen: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="754"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <oldsource>Select the &quot;usr&quot; directory to start create checksums recursively</oldsource>
        <translation>Wählen Sie das Verzeichnis &quot;usr&quot; aus, um Hashsummen rekursiv zu erstellen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="788"/>
        <location filename="../hash.cpp" line="2319"/>
        <location filename="../hash.cpp" line="2699"/>
        <location filename="../hash.cpp" line="2792"/>
        <location filename="../hash.cpp" line="2860"/>
        <location filename="../hash.cpp" line="3640"/>
        <location filename="../hash.cpp" line="3765"/>
        <location filename="../hash.cpp" line="3821"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="802"/>
        <source>md5sums file has been successfully created!</source>
        <translation>Datei md5sums wurde erfolgreich erstellt!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="805"/>
        <source>md5sums file has been successfully created in </source>
        <translation>Datei md5sums wurde erfolgreich erstellt in </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="832"/>
        <source>Help with the program and all settings and menus can be found here:</source>
        <oldsource>Help with the program and all settings and menus can be found here:&lt;br&gt;</oldsource>
        <translation>Hilfe zum Programm und allen Einstellungen und Menüs finden Sie hier:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="832"/>
        <source>The program&apos;s website can be found here:</source>
        <translation>Die Webseite des Programms finden Sie hier:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="840"/>
        <source>Compare with this hash sum...</source>
        <translation>Mit dieser Hashsumme vergleichen...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="857"/>
        <source>...with hash sum for this text:</source>
        <translation>...mit Hashsumme für diesen Text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="869"/>
        <location filename="../hash.cpp" line="932"/>
        <source> can handle texts up to </source>
        <oldsource> can handle texts of up to </oldsource>
        <translatorcomment>Used &apos;character&apos; from next string for correct sentence construction.</translatorcomment>
        <translation> kann Texte bis zu Zeichen </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="869"/>
        <location filename="../hash.cpp" line="932"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translatorcomment>Used &apos;handle&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> verarbeiten.
Bitte reduzieren Sie die Länge des Textes.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="877"/>
        <location filename="../hash.cpp" line="898"/>
        <location filename="../hash.cpp" line="900"/>
        <location filename="../hash.cpp" line="903"/>
        <location filename="../hash.cpp" line="905"/>
        <location filename="../hash.cpp" line="940"/>
        <location filename="../hash.cpp" line="949"/>
        <location filename="../hash.cpp" line="951"/>
        <location filename="../hash.cpp" line="954"/>
        <location filename="../hash.cpp" line="956"/>
        <source>Hash sum calculated for:</source>
        <translation>Hashsumme berechnet für:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="877"/>
        <location filename="../hash.cpp" line="940"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation>konnte nicht berechnet werden. Ein unbekannter Fehler ist aufgetreten.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="882"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation>Es ist nicht die gleiche wie die Hashsumme, die Sie vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="883"/>
        <source>Hash sums are NOT equal!</source>
        <translation>Hashsummen sind NICHT gleich!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="887"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation>Es ist die gleiche wie die Hashsumme, die Sie vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="888"/>
        <source>Hash sums are equal!</source>
        <translation>Hashsummen sind gleich!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="898"/>
        <location filename="../hash.cpp" line="903"/>
        <location filename="../hash.cpp" line="949"/>
        <location filename="../hash.cpp" line="954"/>
        <source>characters.</source>
        <translation>Zeichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="900"/>
        <location filename="../hash.cpp" line="905"/>
        <location filename="../hash.cpp" line="951"/>
        <location filename="../hash.cpp" line="956"/>
        <source>characters</source>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="908"/>
        <source>Hash sums are compared!</source>
        <translation>Hashsummen werden verglichen!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="949"/>
        <location filename="../hash.cpp" line="951"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Leerzeichen wird nicht entfernt, wenn die Hashsummen berechnet werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="959"/>
        <source>Hash sum has been calculated!</source>
        <translation>Hashsumme wurde berechnet!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="979"/>
        <source>Compare with this hash sum:</source>
        <translation>Mit dieser Hashsumme vergleichen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1119"/>
        <source>The license file is not found</source>
        <translation>Die Lizenzdatei wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1165"/>
        <source>The version history file is not found</source>
        <translation>Die Versionshistoriedatei wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1208"/>
        <source> for the Greek translation.</source>
        <translation> für die griechische Übersetzung.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1208"/>
        <location filename="../hash.cpp" line="1209"/>
        <source>Many thanks to </source>
        <oldsource>Many thanks to &lt;a href=&quot;</oldsource>
        <translation>Vielen Dank an </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1209"/>
        <source> for the German translation.</source>
        <translation> für die deutsche Übersetzung.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1219"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation>Ein Programm, um Hashsummen zu berechnen und Dateien zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="22"/>
        <source>This program uses Qt version </source>
        <translation>Dieses Programm verwendet Qt-Version </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="22"/>
        <source> running on </source>
        <translation> läuft auf </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="24"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="37"/>
        <location filename="../system.cpp" line="41"/>
        <location filename="../system.cpp" line="45"/>
        <location filename="../system.cpp" line="121"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="53"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1259"/>
        <location filename="../hash.cpp" line="1294"/>
        <location filename="../hash.cpp" line="1329"/>
        <location filename="../hash.cpp" line="1364"/>
        <location filename="../hash.cpp" line="1399"/>
        <location filename="../hash.cpp" line="1434"/>
        <location filename="../hash.cpp" line="1469"/>
        <location filename="../hash.cpp" line="1504"/>
        <location filename="../hash.cpp" line="1539"/>
        <location filename="../hash.cpp" line="1574"/>
        <location filename="../hash.cpp" line="1609"/>
        <location filename="../hash.cpp" line="1644"/>
        <location filename="../hash.cpp" line="1681"/>
        <location filename="../hash.cpp" line="1716"/>
        <location filename="../hash.cpp" line="1751"/>
        <source>(Click to change)</source>
        <translation>(Klicken Sie, um zu ändern)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2145"/>
        <source>Default file name:</source>
        <translation>Standard-Dateiname:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2149"/>
        <location filename="../hash.cpp" line="2156"/>
        <location filename="../hash.cpp" line="3547"/>
        <source> (Click to change)</source>
        <translation> (Klicken Sie, um zu ändern)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2180"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <translation>Dateien öffnen, um eine Liste der Hashsummen zu erstellen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2231"/>
        <location filename="../hash.cpp" line="2253"/>
        <location filename="../hash.cpp" line="2271"/>
        <source>Did you select all files you want to choose?</source>
        <translation>Haben Sie alle Dateien ausgewählt, die Sie aussuchen möchten?</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2232"/>
        <location filename="../hash.cpp" line="2254"/>
        <location filename="../hash.cpp" line="2272"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2233"/>
        <location filename="../hash.cpp" line="2255"/>
        <location filename="../hash.cpp" line="2273"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2293"/>
        <source>Save the Debian md5sums file</source>
        <translation>Debian md5sums-Datei speichern</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2327"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation>Dies ist kein gültiger Pfad zum Bauen eines Debian-Pakets!</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="43"/>
        <location filename="../isthereaduplicate.cpp" line="91"/>
        <source>Select a folder to Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="45"/>
        <location filename="../isthereaduplicate.cpp" line="93"/>
        <source>All Files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="121"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2377"/>
        <location filename="../hash.cpp" line="2379"/>
        <location filename="../hash.cpp" line="2385"/>
        <location filename="../hash.cpp" line="2387"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source>I have been looking into </source>
        <translatorcomment>Translated &apos;looking into&apos; in next string for correct sentence construction.</translatorcomment>
        <translation>Ich habe in </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../hash.cpp" line="2377"/>
        <location filename="../hash.cpp" line="2385"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translatorcomment>Included &apos;looking into&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> Ordner wegen Dateien nachgesehen. Ich habe es geschafft, die Hashsumme in allen Ordnern mit Dateien zu berechnen.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="107"/>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2377"/>
        <location filename="../hash.cpp" line="2379"/>
        <location filename="../hash.cpp" line="2385"/>
        <location filename="../hash.cpp" line="2387"/>
        <location filename="../isthereaduplicate.cpp" line="150"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source>(Algorithm: </source>
        <translation>(Algorithmus: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="109"/>
        <location filename="../duplicates.cpp" line="117"/>
        <location filename="../hash.cpp" line="2379"/>
        <location filename="../hash.cpp" line="2387"/>
        <location filename="../isthereaduplicate.cpp" line="152"/>
        <location filename="../isthereaduplicate.cpp" line="160"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translatorcomment>Included &apos;looking into&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> Ordnern wegen Dateien nachgesehen. Ich habe es geschafft, die Hashsumme in allen Ordnern mit Dateien zu berechnen.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="111"/>
        <source>Path: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="226"/>
        <location filename="../isthereaduplicate.cpp" line="183"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="228"/>
        <source>Recursively from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="251"/>
        <source>The number of files with one or more duplicates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="255"/>
        <location filename="../isthereaduplicate.cpp" line="203"/>
        <source>Duplicate files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3744"/>
        <location filename="../hash.cpp" line="3800"/>
        <source>Save the hash summary list as a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3744"/>
        <location filename="../hash.cpp" line="3800"/>
        <source>Any file (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1855"/>
        <location filename="../hash.cpp" line="2381"/>
        <location filename="../hash.cpp" line="3709"/>
        <location filename="../isthereaduplicate.cpp" line="154"/>
        <source>Path: </source>
        <translation>Pfad: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2389"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation>Ich habe alle Ordner überprüft und alle Dateien erstellt! Mission erfüllt.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="122"/>
        <location filename="../hash.cpp" line="2392"/>
        <location filename="../isthereaduplicate.cpp" line="165"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation>Mission erfüllt! Ich habe die Hashsumme berechnet von </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="122"/>
        <location filename="../hash.cpp" line="2392"/>
        <location filename="../isthereaduplicate.cpp" line="165"/>
        <source> files.</source>
        <translation> Dateien.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2747"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation>Hashsummenwerte wurden mit alten Werten in der Datei verglichen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2536"/>
        <location filename="../hash.cpp" line="2766"/>
        <source>Text Files (*)</source>
        <translation>Textdateien (*)</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="24"/>
        <source> was created </source>
        <translation> wurde erstellt </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="117"/>
        <source>Unknown version</source>
        <translation>Unbekannte Version</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="126"/>
        <source>Unknown compiler.</source>
        <translation>Unbekannter Kompiler.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2333"/>
        <location filename="../hash.cpp" line="2499"/>
        <location filename="../hash.cpp" line="2808"/>
        <location filename="../hash.cpp" line="2876"/>
        <source> No hash sum could be calculated</source>
        <translation>Es konnte keine Hashsumme berechnet werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2404"/>
        <source>This is not a valid path to build a Debian package!</source>
        <oldsource>This is not a valid path to build a debian package!!</oldsource>
        <translation>Dies ist kein gültiger Pfad zum Bauen eines Debian-Pakets!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2490"/>
        <source>Files in:</source>
        <translation>Dateien in:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2508"/>
        <location filename="../hash.cpp" line="2747"/>
        <location filename="../hash.cpp" line="2815"/>
        <location filename="../hash.cpp" line="2832"/>
        <location filename="../hash.cpp" line="2883"/>
        <location filename="../hash.cpp" line="2892"/>
        <source>Hash sum for </source>
        <translation>Hashsumme für </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2508"/>
        <location filename="../hash.cpp" line="2747"/>
        <location filename="../hash.cpp" line="2815"/>
        <location filename="../hash.cpp" line="2832"/>
        <location filename="../hash.cpp" line="2883"/>
        <location filename="../hash.cpp" line="2892"/>
        <source> of </source>
        <translation> von </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2508"/>
        <location filename="../hash.cpp" line="2815"/>
        <location filename="../hash.cpp" line="2883"/>
        <source> files was successfully calculated</source>
        <translation> Dateien wurden erfolgreich berechnet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2509"/>
        <location filename="../hash.cpp" line="2731"/>
        <location filename="../hash.cpp" line="2816"/>
        <location filename="../hash.cpp" line="2884"/>
        <source>Copyright </source>
        <translation>Urheberrecht </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="227"/>
        <location filename="../hash.cpp" line="2512"/>
        <location filename="../hash.cpp" line="2734"/>
        <location filename="../hash.cpp" line="2819"/>
        <location filename="../hash.cpp" line="2887"/>
        <location filename="../isthereaduplicate.cpp" line="191"/>
        <source>Created: </source>
        <translation>Erstellt: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2534"/>
        <source>Open the old hash sum file to compare with</source>
        <translation>Öffnen Sie die alte Hashsummendatei, um sie zu vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2561"/>
        <source>Hash sum could not be estimated</source>
        <translation>Hashsumme konnte nicht geschätzt werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2571"/>
        <source>Could not open &quot;</source>
        <translatorcomment>Translated &apos;not open&apos; in next string for correct sentence construction.</translatorcomment>
        <translation>Konnte &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2571"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translatorcomment>Included &apos;not open&apos; from last string for correct sentence construction.</translatorcomment>
        <translation>&quot; mit Hashsummen nicht öffnen.
Stellen Sie sicher, dass die Datei vorhanden ist und überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2586"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation>Die alte Hashsummendatei enthält keinen Pfad zu den Dateien. Sie können nicht vergleichen!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2703"/>
        <source>Unchanged files</source>
        <translation>Ungeänderte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2710"/>
        <source>Changed files</source>
        <translation>Geänderte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2724"/>
        <source>New files</source>
        <translation>Neue Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2746"/>
        <location filename="../hash.cpp" line="2831"/>
        <location filename="../hash.cpp" line="2891"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2747"/>
        <location filename="../hash.cpp" line="2832"/>
        <location filename="../hash.cpp" line="2892"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation> Dateien wurde erfolgreich berechnet (Algorithmus: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2292"/>
        <source>Files (*.*)</source>
        <oldsource>Files (*)</oldsource>
        <translation>Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1053"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2717"/>
        <source>Files not tested</source>
        <translation>Dateien nicht getestet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2767"/>
        <source>Save the file with the hash sums</source>
        <translation>Datei mit den Hashsummen speichern</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2949"/>
        <source>&quot; has hash sum</source>
        <translation>&quot; hat Hashsumme</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3183"/>
        <location filename="../hash.cpp" line="3188"/>
        <source>&quot; and &quot;</source>
        <translation>&quot; und &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3183"/>
        <location filename="../hash.cpp" line="3188"/>
        <source>&quot; have these hash sums:</source>
        <translation>&quot; haben diese Hashsummen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3183"/>
        <source>. The files are identical.</source>
        <translation>. Die Dateien sind identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3184"/>
        <source>The files are identical.</source>
        <translation>Die Dateien sind identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3188"/>
        <source>. The files are NOT identical.</source>
        <translation>. Die Dateien sind NICHT identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3189"/>
        <source>The files are NOT identical.</source>
        <translation>Die Dateien sind NICHT identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3192"/>
        <location filename="../hash.cpp" line="3196"/>
        <source> and </source>
        <translation> und </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="41"/>
        <source>Open the file you want to find duplicates for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="192"/>
        <source>Identical files with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="193"/>
        <source>Recursively searched from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="199"/>
        <source>I found </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="199"/>
        <source> duplicates.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
