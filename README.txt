2020-05-22T16:19:30
Algorithm: md5

For Linux operating systems.
For glibc from version 2.31 and later. For example, Ubuntu 20.04 and later. For 64 bit operating systems.
MD5: 404e7b743af94002fb9c87765da94317 hashSum_intel_x86_64.AppImage. (24.5 MB)

For Linux operating systems.
For glibc from version 2.23 and later. For example, Ubuntu 16.04 and later. For 64 bit operating systems.
MD5: 10fe23099406256bee70f01bbfad3b0e hashSum-x86_64.AppImage. (22.8 MB)

For Linux operating systems.
For glibc from version 2.23 and later. For example, Ubuntu 16.04 and later. For 32 bit operating systems.
MD5: 767e2555dc6444dd8567232f8667e9ed hashSum-i386.AppImage. (24.5 MB)

EXPERIMENTAL
Double-click to start the installation.
hashSum will be installed in / opt / hashSum.
Uninstall with "hashSumMaintenanceTool".
Works with Ubuntu 20.04.
Not sure if it works with other distributions.
MD5: 16e81ff862997c9dbb666cac5bb111be install-hashSum_X86_64-2.5.1. (31.6 MB)

For Windows operating systems, 64 bit. Installs on your computer
A reliable installer. But a lot to download.
MD5: 12c9ed84e5a8af4f3a0f82d6ccf04985 install-hashSum_X86_64-2.5.1.exe. (27.1 MB)

For Windows operating systems, 64 bit. Installs on your computer
A not so reliable installation program. But less to download.
MD5: e8fb797e00eba9a6d88efc261039d337 install-hashSum_X86_64-2.5.1.exe. (7.05 MB)

For Windows operating systems, 64 bit. Unpack and run, no installation required.
MD5: f1984dc75e39da904b42d3fe8a811eb8 portable-hashSum_X86_64-2.5.1.zip. (8.75 MB)

For Windows operating systems, 32 bit. Installs on your computer
MD5: 2667ecc0f81c96ecdd4f846689df2087 install-hashSum_X86-2.5.1.exe. (7.28 MB)

For Windows operating systems, 32 bit. Unpack and run, no installation required.
MD5: dd040add46656436da401b65417ab827 portable-hashSum_X86-2.5.1.zip (9.03 MB)
