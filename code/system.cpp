/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "hash.h"




QString Hash::getSystem()
{
    QString v = "";
    v += tr("This program uses Qt version ") + QT_VERSION_STR + tr(" running on ");
    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" + QSysInfo::currentCpuArchitecture() + ").\n";
    v += APPLICATION_NAME " " VERSION + tr(" was created ") + BUILD_DATE_TIME "\n" + tr("by a computer with") + " " + QSysInfo::buildAbi() + ".\n";
    v += "\n";
    // write GCC version
    QString temp;
#if defined _MSC_VER ||  __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined   __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined   __GNUC__ && !defined __clang__
#if defined __GNUC__ && !defined __MINGW32__
#define COMPILER "GCC"
#endif
#ifdef __MINGW32__
#define COMPILER "MinGW GCC"
#endif
    temp = (QString(tr(" Compiled by") + " %1 %2.%3.%4%5").arg(COMPILER).arg(__GNUC__).arg(__GNUC_MINOR__).arg(__GNUC_PATCHLEVEL__).arg("."));
#endif
#ifdef __ICC
#define INTEL "Intel icl"
    temp = (QString(tr(" Compiled by") + " %1 %2 Build date: %3 Update: %4%5").arg(INTEL).arg(__INTEL_COMPILER).arg(__INTEL_COMPILER_BUILD_DATE).arg(__INTEL_COMPILER_UPDATE).arg("."));
#endif
    v += temp;
#if defined __clang__
    v += (QString(tr(" Compiled by") +  " %1 %2.%3.%4%5").arg("Clang").arg(__clang_major__).arg(__clang_minor__).arg(__clang_patchlevel__).arg("."));
#endif
#endif
// MSVC version
#ifdef _MSC_VER
#define COMPILER "MSVC++"
    QString tmp = QString::number(_MSC_VER);
    QString version;
    QString full_version = tr("Full version number ") + QString::number(_MSC_FULL_VER);

    switch(_MSC_VER) {
        case 1500:
            version = "9.0 (Visual Studio 2008)\n" + full_version;
            break;

        case 1600:
            version = "10.0 (Visual Studio 2010)\n" + full_version;
            break;

        case 1700:
            version = "11.0 (Visual Studio 2012)<br>" + full_version;
            break;

        case 1800:
            version = "12.0 (Visual Studio 2013)\n" + full_version;
            break;

        case 1900:
            version = "14.0 (Visual Studio 2015)\n" + full_version;
            break;

        case 1910:
            version = "15.0 (Visual Studio 2017)\n" + full_version;
            break;

        case 1912:
            version = "15.5 (Visual Studio 2017)\n" + full_version;
            break;

        case 1914:
            version = "15.7 (Visual Studio 2017)\n" + full_version;
            break;

        case 1915:
            version = "15.8 (Visual Studio 2017)\n" + full_version;
            break;

        case 1916:
            version = "15.9 (Visual Studio 2017)\n" + full_version;
            break;

        case 1921:
            version = "16.1 (Visual Studio 2019)\n" + full_version;
            break;

        case 1922:
            version = "16.2 (Visual Studio 2019)\n" + full_version;
            break;

        case 1923:
            version = "16.3 (Visual Studio 2019)\n" + full_version;
            break;

        case 1924:
            version = "16.4 (Visual Studio 2019)\n" + full_version;
            break;

        case 1925:
            version = "16.5 (Visual Studio 2019)\n" + full_version;
            break;

        default:
            version = tr("Unknown version") + "\n" + full_version;
            break;
    }

    v += (QString(tr(" Compiled by") +  " %1 %2%3").arg(COMPILER).arg(version).arg("."));
#endif
    v += "\n";
    return v;
#else
    v += tr("Unknown compiler.");
    v += "\n";
    return v;
#endif
}

