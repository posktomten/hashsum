/********************************************************************************
** Form generated from reading UI file 'hash.ui'
**
** Created by: Qt User Interface Compiler version 5.15.14
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HASH_H
#define UI_HASH_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <extqlineedit.h>

QT_BEGIN_NAMESPACE

class Ui_Hash
{
public:
    QAction *actionChecksum;
    QAction *actionCheckForUpdates;
    QAction *actionAbout;
    QAction *actionCheckOnStart;
    QAction *actionExit;
    QAction *actionMD4;
    QAction *actionMD5;
    QAction *actionSHA1;
    QAction *actionCompare;
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionCreateChecksumList;
    QAction *actionSaveChecksumListWhereFilesAre;
    QAction *actionDefaultFileName;
    QAction *actionCreateChecksumLists;
    QAction *actionWriteTimeDateInHashSumFile;
    QAction *actionFindChangedFiles;
    QAction *actionUseNativeDialogs;
    QAction *actionAllwaysOpenHomeDirectory;
    QAction *actionCompareGivenHashSum;
    QAction *actionSaveAsText;
    QAction *actionSaveAsPdf;
    QAction *actionPrintColorPdf;
    QAction *actionCopyPath;
    QAction *actionCopyHashSum;
    QAction *actionSaveAsTxtPdf;
    QAction *actionHashSumFromText;
    QAction *actionHashSumText;
    QAction *actionCompareHashSumText;
    QAction *actionHelp;
    QAction *actionCreateDebianMd5sums;
    QAction *actionShowFullPathInHashSumFile;
    QAction *actionCreateDebianMd5sumsAuto;
    QAction *actionGreek;
    QAction *actionSHA224;
    QAction *actionSHA256;
    QAction *actionSHA384;
    QAction *actionSHA512;
    QAction *actionSHA3_224;
    QAction *actionSHA3_256;
    QAction *actionSHA3_384;
    QAction *actionSHA3_512;
    QAction *actionLicense;
    QAction *actionVersionHistory;
    QAction *actionOpenHashSumList;
    QAction *actionOpenComparisonList;
    QAction *actionGerman;
    QAction *actionSaveToFile;
    QAction *actionKeccak_224;
    QAction *actionKeccak_256;
    QAction *actionKeccak_384;
    QAction *actionKeccak_512;
    QAction *actionFindIdenticalFiles;
    QAction *actionIsThereADuplicate;
    QAction *actionAlwaysOpenTheDisplayWindow;
    QAction *actionUninstall;
    QAction *actionMainteanceTool;
    QAction *actionDesktopShortcut;
    QAction *actionApplicationsMenuShortcut;
    QAction *actionSilent;
    QAction *actionAboutQt;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *progressLabel;
    ExtQLineEdit *lineEdit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuRecentFiles;
    QMenu *menuAlgorithm;
    QMenu *menuTools;
    QMenu *menuHelp;
    QMenu *menuLanguage;
    QMenu *menuEdit;
    QMenu *menuText;
    QMenu *menuSpeech;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Hash)
    {
        if (Hash->objectName().isEmpty())
            Hash->setObjectName(QString::fromUtf8("Hash"));
        Hash->resize(913, 237);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/hashsum.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Hash->setWindowIcon(icon);
        Hash->setDocumentMode(false);
        Hash->setTabShape(QTabWidget::Rounded);
        actionChecksum = new QAction(Hash);
        actionChecksum->setObjectName(QString::fromUtf8("actionChecksum"));
        actionCheckForUpdates = new QAction(Hash);
        actionCheckForUpdates->setObjectName(QString::fromUtf8("actionCheckForUpdates"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/update.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCheckForUpdates->setIcon(icon1);
        actionAbout = new QAction(Hash);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/about.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon2);
        actionCheckOnStart = new QAction(Hash);
        actionCheckOnStart->setObjectName(QString::fromUtf8("actionCheckOnStart"));
        actionCheckOnStart->setCheckable(true);
        actionCheckOnStart->setVisible(false);
        actionExit = new QAction(Hash);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon3);
        actionMD4 = new QAction(Hash);
        actionMD4->setObjectName(QString::fromUtf8("actionMD4"));
        actionMD4->setCheckable(true);
        actionMD5 = new QAction(Hash);
        actionMD5->setObjectName(QString::fromUtf8("actionMD5"));
        actionMD5->setCheckable(true);
        actionSHA1 = new QAction(Hash);
        actionSHA1->setObjectName(QString::fromUtf8("actionSHA1"));
        actionSHA1->setCheckable(true);
        actionCompare = new QAction(Hash);
        actionCompare->setObjectName(QString::fromUtf8("actionCompare"));
        actionEnglish = new QAction(Hash);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon4);
        actionSwedish = new QAction(Hash);
        actionSwedish->setObjectName(QString::fromUtf8("actionSwedish"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSwedish->setIcon(icon5);
        actionCreateChecksumList = new QAction(Hash);
        actionCreateChecksumList->setObjectName(QString::fromUtf8("actionCreateChecksumList"));
        actionSaveChecksumListWhereFilesAre = new QAction(Hash);
        actionSaveChecksumListWhereFilesAre->setObjectName(QString::fromUtf8("actionSaveChecksumListWhereFilesAre"));
        actionSaveChecksumListWhereFilesAre->setCheckable(true);
        actionDefaultFileName = new QAction(Hash);
        actionDefaultFileName->setObjectName(QString::fromUtf8("actionDefaultFileName"));
        actionCreateChecksumLists = new QAction(Hash);
        actionCreateChecksumLists->setObjectName(QString::fromUtf8("actionCreateChecksumLists"));
        actionWriteTimeDateInHashSumFile = new QAction(Hash);
        actionWriteTimeDateInHashSumFile->setObjectName(QString::fromUtf8("actionWriteTimeDateInHashSumFile"));
        actionWriteTimeDateInHashSumFile->setCheckable(true);
        actionFindChangedFiles = new QAction(Hash);
        actionFindChangedFiles->setObjectName(QString::fromUtf8("actionFindChangedFiles"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/find.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFindChangedFiles->setIcon(icon6);
        actionUseNativeDialogs = new QAction(Hash);
        actionUseNativeDialogs->setObjectName(QString::fromUtf8("actionUseNativeDialogs"));
        actionUseNativeDialogs->setCheckable(true);
        actionAllwaysOpenHomeDirectory = new QAction(Hash);
        actionAllwaysOpenHomeDirectory->setObjectName(QString::fromUtf8("actionAllwaysOpenHomeDirectory"));
        actionAllwaysOpenHomeDirectory->setCheckable(true);
        actionCompareGivenHashSum = new QAction(Hash);
        actionCompareGivenHashSum->setObjectName(QString::fromUtf8("actionCompareGivenHashSum"));
        actionSaveAsText = new QAction(Hash);
        actionSaveAsText->setObjectName(QString::fromUtf8("actionSaveAsText"));
        actionSaveAsText->setCheckable(true);
        actionSaveAsPdf = new QAction(Hash);
        actionSaveAsPdf->setObjectName(QString::fromUtf8("actionSaveAsPdf"));
        actionSaveAsPdf->setCheckable(true);
        actionPrintColorPdf = new QAction(Hash);
        actionPrintColorPdf->setObjectName(QString::fromUtf8("actionPrintColorPdf"));
        actionPrintColorPdf->setCheckable(true);
        actionCopyPath = new QAction(Hash);
        actionCopyPath->setObjectName(QString::fromUtf8("actionCopyPath"));
        actionCopyPath->setEnabled(false);
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/copy.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCopyPath->setIcon(icon7);
        actionCopyHashSum = new QAction(Hash);
        actionCopyHashSum->setObjectName(QString::fromUtf8("actionCopyHashSum"));
        actionCopyHashSum->setEnabled(false);
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/hashsum.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCopyHashSum->setIcon(icon8);
        actionSaveAsTxtPdf = new QAction(Hash);
        actionSaveAsTxtPdf->setObjectName(QString::fromUtf8("actionSaveAsTxtPdf"));
        actionSaveAsTxtPdf->setCheckable(true);
        actionHashSumFromText = new QAction(Hash);
        actionHashSumFromText->setObjectName(QString::fromUtf8("actionHashSumFromText"));
        actionHashSumText = new QAction(Hash);
        actionHashSumText->setObjectName(QString::fromUtf8("actionHashSumText"));
        actionCompareHashSumText = new QAction(Hash);
        actionCompareHashSumText->setObjectName(QString::fromUtf8("actionCompareHashSumText"));
        actionHelp = new QAction(Hash);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/help.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelp->setIcon(icon9);
        actionCreateDebianMd5sums = new QAction(Hash);
        actionCreateDebianMd5sums->setObjectName(QString::fromUtf8("actionCreateDebianMd5sums"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/debian.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCreateDebianMd5sums->setIcon(icon10);
        actionShowFullPathInHashSumFile = new QAction(Hash);
        actionShowFullPathInHashSumFile->setObjectName(QString::fromUtf8("actionShowFullPathInHashSumFile"));
        actionShowFullPathInHashSumFile->setCheckable(true);
        actionCreateDebianMd5sumsAuto = new QAction(Hash);
        actionCreateDebianMd5sumsAuto->setObjectName(QString::fromUtf8("actionCreateDebianMd5sumsAuto"));
        actionCreateDebianMd5sumsAuto->setIcon(icon10);
        actionGreek = new QAction(Hash);
        actionGreek->setObjectName(QString::fromUtf8("actionGreek"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/greek.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGreek->setIcon(icon11);
        actionSHA224 = new QAction(Hash);
        actionSHA224->setObjectName(QString::fromUtf8("actionSHA224"));
        actionSHA224->setCheckable(true);
        actionSHA256 = new QAction(Hash);
        actionSHA256->setObjectName(QString::fromUtf8("actionSHA256"));
        actionSHA256->setCheckable(true);
        actionSHA384 = new QAction(Hash);
        actionSHA384->setObjectName(QString::fromUtf8("actionSHA384"));
        actionSHA384->setCheckable(true);
        actionSHA512 = new QAction(Hash);
        actionSHA512->setObjectName(QString::fromUtf8("actionSHA512"));
        actionSHA512->setCheckable(true);
        actionSHA3_224 = new QAction(Hash);
        actionSHA3_224->setObjectName(QString::fromUtf8("actionSHA3_224"));
        actionSHA3_224->setCheckable(true);
        actionSHA3_256 = new QAction(Hash);
        actionSHA3_256->setObjectName(QString::fromUtf8("actionSHA3_256"));
        actionSHA3_256->setCheckable(true);
        actionSHA3_384 = new QAction(Hash);
        actionSHA3_384->setObjectName(QString::fromUtf8("actionSHA3_384"));
        actionSHA3_384->setCheckable(true);
        actionSHA3_512 = new QAction(Hash);
        actionSHA3_512->setObjectName(QString::fromUtf8("actionSHA3_512"));
        actionSHA3_512->setCheckable(true);
        actionSHA3_512->setChecked(false);
        actionLicense = new QAction(Hash);
        actionLicense->setObjectName(QString::fromUtf8("actionLicense"));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/license.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicense->setIcon(icon12);
        actionVersionHistory = new QAction(Hash);
        actionVersionHistory->setObjectName(QString::fromUtf8("actionVersionHistory"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/versionhistory.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionVersionHistory->setIcon(icon13);
        actionOpenHashSumList = new QAction(Hash);
        actionOpenHashSumList->setObjectName(QString::fromUtf8("actionOpenHashSumList"));
        actionOpenComparisonList = new QAction(Hash);
        actionOpenComparisonList->setObjectName(QString::fromUtf8("actionOpenComparisonList"));
        actionGerman = new QAction(Hash);
        actionGerman->setObjectName(QString::fromUtf8("actionGerman"));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/tysk.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGerman->setIcon(icon14);
        actionSaveToFile = new QAction(Hash);
        actionSaveToFile->setObjectName(QString::fromUtf8("actionSaveToFile"));
        actionSaveToFile->setEnabled(false);
        actionKeccak_224 = new QAction(Hash);
        actionKeccak_224->setObjectName(QString::fromUtf8("actionKeccak_224"));
        actionKeccak_224->setCheckable(true);
        actionKeccak_256 = new QAction(Hash);
        actionKeccak_256->setObjectName(QString::fromUtf8("actionKeccak_256"));
        actionKeccak_256->setCheckable(true);
        actionKeccak_384 = new QAction(Hash);
        actionKeccak_384->setObjectName(QString::fromUtf8("actionKeccak_384"));
        actionKeccak_384->setCheckable(true);
        actionKeccak_512 = new QAction(Hash);
        actionKeccak_512->setObjectName(QString::fromUtf8("actionKeccak_512"));
        actionKeccak_512->setCheckable(true);
        actionFindIdenticalFiles = new QAction(Hash);
        actionFindIdenticalFiles->setObjectName(QString::fromUtf8("actionFindIdenticalFiles"));
        actionIsThereADuplicate = new QAction(Hash);
        actionIsThereADuplicate->setObjectName(QString::fromUtf8("actionIsThereADuplicate"));
        actionAlwaysOpenTheDisplayWindow = new QAction(Hash);
        actionAlwaysOpenTheDisplayWindow->setObjectName(QString::fromUtf8("actionAlwaysOpenTheDisplayWindow"));
        actionAlwaysOpenTheDisplayWindow->setCheckable(true);
        actionUninstall = new QAction(Hash);
        actionUninstall->setObjectName(QString::fromUtf8("actionUninstall"));
        actionUninstall->setEnabled(false);
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/uninstall.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUninstall->setIcon(icon15);
        actionUninstall->setVisible(false);
        actionMainteanceTool = new QAction(Hash);
        actionMainteanceTool->setObjectName(QString::fromUtf8("actionMainteanceTool"));
        actionMainteanceTool->setEnabled(false);
        actionMainteanceTool->setVisible(false);
        actionDesktopShortcut = new QAction(Hash);
        actionDesktopShortcut->setObjectName(QString::fromUtf8("actionDesktopShortcut"));
        actionDesktopShortcut->setCheckable(true);
        actionDesktopShortcut->setIconVisibleInMenu(false);
        actionDesktopShortcut->setShortcutVisibleInContextMenu(true);
        actionApplicationsMenuShortcut = new QAction(Hash);
        actionApplicationsMenuShortcut->setObjectName(QString::fromUtf8("actionApplicationsMenuShortcut"));
        actionApplicationsMenuShortcut->setCheckable(true);
        actionApplicationsMenuShortcut->setChecked(true);
        actionApplicationsMenuShortcut->setEnabled(false);
        actionApplicationsMenuShortcut->setVisible(false);
        actionApplicationsMenuShortcut->setIconVisibleInMenu(false);
        actionSilent = new QAction(Hash);
        actionSilent->setObjectName(QString::fromUtf8("actionSilent"));
        actionSilent->setCheckable(true);
        actionAboutQt = new QAction(Hash);
        actionAboutQt->setObjectName(QString::fromUtf8("actionAboutQt"));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/qt256.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAboutQt->setIcon(icon16);
        centralWidget = new QWidget(Hash);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        progressLabel = new QLabel(centralWidget);
        progressLabel->setObjectName(QString::fromUtf8("progressLabel"));

        verticalLayout->addWidget(progressLabel);

        lineEdit = new ExtQLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy);
        lineEdit->setInputMask(QString::fromUtf8(""));
        lineEdit->setText(QString::fromUtf8(""));
        lineEdit->setMaxLength(500);
        lineEdit->setReadOnly(true);

        verticalLayout->addWidget(lineEdit);


        horizontalLayout->addLayout(verticalLayout);

        Hash->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Hash);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 913, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuRecentFiles = new QMenu(menuFile);
        menuRecentFiles->setObjectName(QString::fromUtf8("menuRecentFiles"));
        menuAlgorithm = new QMenu(menuBar);
        menuAlgorithm->setObjectName(QString::fromUtf8("menuAlgorithm"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuLanguage = new QMenu(menuBar);
        menuLanguage->setObjectName(QString::fromUtf8("menuLanguage"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuText = new QMenu(menuBar);
        menuText->setObjectName(QString::fromUtf8("menuText"));
        menuSpeech = new QMenu(menuBar);
        menuSpeech->setObjectName(QString::fromUtf8("menuSpeech"));
        Hash->setMenuBar(menuBar);
        statusBar = new QStatusBar(Hash);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setSizeGripEnabled(false);
        Hash->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuText->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuAlgorithm->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuLanguage->menuAction());
        menuBar->addAction(menuSpeech->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionChecksum);
        menuFile->addAction(actionCompare);
        menuFile->addAction(actionCompareGivenHashSum);
        menuFile->addAction(actionFindIdenticalFiles);
        menuFile->addAction(actionIsThereADuplicate);
        menuFile->addAction(actionCreateChecksumList);
        menuFile->addAction(actionOpenHashSumList);
        menuFile->addAction(actionOpenComparisonList);
        menuFile->addAction(actionCreateChecksumLists);
        menuFile->addAction(menuRecentFiles->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionCreateDebianMd5sums);
        menuFile->addAction(actionCreateDebianMd5sumsAuto);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuAlgorithm->addAction(actionMD4);
        menuAlgorithm->addAction(actionMD5);
        menuAlgorithm->addAction(actionSHA1);
        menuAlgorithm->addAction(actionSHA224);
        menuAlgorithm->addAction(actionSHA256);
        menuAlgorithm->addAction(actionSHA384);
        menuAlgorithm->addAction(actionSHA512);
        menuAlgorithm->addAction(actionSHA3_224);
        menuAlgorithm->addAction(actionSHA3_256);
        menuAlgorithm->addAction(actionSHA3_384);
        menuAlgorithm->addAction(actionSHA3_512);
        menuAlgorithm->addAction(actionKeccak_224);
        menuAlgorithm->addAction(actionKeccak_256);
        menuAlgorithm->addAction(actionKeccak_384);
        menuAlgorithm->addAction(actionKeccak_512);
        menuTools->addAction(actionCheckOnStart);
        menuTools->addAction(actionSaveChecksumListWhereFilesAre);
        menuTools->addAction(actionDefaultFileName);
        menuTools->addAction(actionWriteTimeDateInHashSumFile);
        menuTools->addAction(actionUseNativeDialogs);
        menuTools->addAction(actionAllwaysOpenHomeDirectory);
        menuTools->addAction(actionShowFullPathInHashSumFile);
        menuTools->addAction(actionAlwaysOpenTheDisplayWindow);
        menuTools->addSeparator();
        menuTools->addAction(actionMainteanceTool);
        menuTools->addSeparator();
        menuTools->addAction(actionApplicationsMenuShortcut);
        menuTools->addAction(actionDesktopShortcut);
        menuHelp->addAction(actionHelp);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionAboutQt);
        menuHelp->addAction(actionCheckForUpdates);
        menuHelp->addAction(actionLicense);
        menuHelp->addAction(actionVersionHistory);
        menuHelp->addAction(actionUninstall);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionGerman);
        menuLanguage->addAction(actionGreek);
        menuLanguage->addAction(actionSwedish);
        menuEdit->addAction(actionFindChangedFiles);
        menuEdit->addAction(actionCopyHashSum);
        menuEdit->addAction(actionCopyPath);
        menuEdit->addAction(actionSaveToFile);
        menuText->addAction(actionHashSumText);
        menuText->addAction(actionCompareHashSumText);
        menuSpeech->addAction(actionSilent);

        retranslateUi(Hash);

        QMetaObject::connectSlotsByName(Hash);
    } // setupUi

    void retranslateUi(QMainWindow *Hash)
    {
        Hash->setWindowTitle(QCoreApplication::translate("Hash", "Hash", nullptr));
        actionChecksum->setText(QCoreApplication::translate("Hash", "Check file or files", nullptr));
#if QT_CONFIG(statustip)
        actionChecksum->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
        actionCheckForUpdates->setText(QCoreApplication::translate("Hash", "Check for updates...", nullptr));
        actionAbout->setText(QCoreApplication::translate("Hash", "About...", nullptr));
        actionAbout->setIconText(QCoreApplication::translate("Hash", "About...", nullptr));
#if QT_CONFIG(tooltip)
        actionAbout->setToolTip(QCoreApplication::translate("Hash", "About...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionCheckOnStart->setText(QCoreApplication::translate("Hash", "Check for program updates at program start", nullptr));
        actionExit->setText(QCoreApplication::translate("Hash", "Exit", nullptr));
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("Hash", "F4", nullptr));
#endif // QT_CONFIG(shortcut)
        actionMD4->setText(QCoreApplication::translate("Hash", "MD4", nullptr));
        actionMD5->setText(QCoreApplication::translate("Hash", "MD5", nullptr));
        actionSHA1->setText(QCoreApplication::translate("Hash", "SHA1", nullptr));
        actionCompare->setText(QCoreApplication::translate("Hash", "Compare two files", nullptr));
        actionEnglish->setText(QCoreApplication::translate("Hash", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("Hash", "Swedish", nullptr));
        actionCreateChecksumList->setText(QCoreApplication::translate("Hash", "Create hash sum list", nullptr));
#if QT_CONFIG(tooltip)
        actionCreateChecksumList->setToolTip(QCoreApplication::translate("Hash", "Create hash sum list", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSaveChecksumListWhereFilesAre->setText(QCoreApplication::translate("Hash", "Save hash sum list where the files are", nullptr));
        actionDefaultFileName->setText(QCoreApplication::translate("Hash", "Default file name", nullptr));
        actionCreateChecksumLists->setText(QCoreApplication::translate("Hash", "Create hash sum lists, recursively", nullptr));
        actionWriteTimeDateInHashSumFile->setText(QCoreApplication::translate("Hash", "Write date and time in the hash sum file", nullptr));
        actionFindChangedFiles->setText(QCoreApplication::translate("Hash", "Find changes", nullptr));
        actionUseNativeDialogs->setText(QCoreApplication::translate("Hash", "Use native dialogs", nullptr));
        actionAllwaysOpenHomeDirectory->setText(QCoreApplication::translate("Hash", "Allways open the home directory", nullptr));
        actionCompareGivenHashSum->setText(QCoreApplication::translate("Hash", "Compare with a given hash sum", nullptr));
        actionSaveAsText->setText(QCoreApplication::translate("Hash", "Save as text (*.*)", nullptr));
        actionSaveAsPdf->setText(QCoreApplication::translate("Hash", "Save as pdf (*.pdf)", nullptr));
        actionPrintColorPdf->setText(QCoreApplication::translate("Hash", "Color pdf", nullptr));
        actionCopyPath->setText(QCoreApplication::translate("Hash", "Copy Path", nullptr));
        actionCopyHashSum->setText(QCoreApplication::translate("Hash", "Copy Hash Sum", nullptr));
        actionSaveAsTxtPdf->setText(QCoreApplication::translate("Hash", "Save as text and pdf (*.* and *.pdf)", nullptr));
        actionHashSumFromText->setText(QCoreApplication::translate("Hash", "Hash sum from text", nullptr));
        actionHashSumText->setText(QCoreApplication::translate("Hash", "Hash sum", nullptr));
        actionCompareHashSumText->setText(QCoreApplication::translate("Hash", "Compare with a given hash sum", nullptr));
        actionHelp->setText(QCoreApplication::translate("Hash", "Help...", nullptr));
        actionHelp->setIconText(QCoreApplication::translate("Hash", "Help...", nullptr));
#if QT_CONFIG(tooltip)
        actionHelp->setToolTip(QCoreApplication::translate("Hash", "Help...", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionHelp->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        actionHelp->setShortcut(QCoreApplication::translate("Hash", "F1", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCreateDebianMd5sums->setText(QCoreApplication::translate("Hash", "Create Debian md5sums", nullptr));
        actionShowFullPathInHashSumFile->setText(QCoreApplication::translate("Hash", "Show full path in the hash sum file", nullptr));
        actionCreateDebianMd5sumsAuto->setText(QCoreApplication::translate("Hash", "Create Debian md5sums (auto)", nullptr));
        actionGreek->setText(QCoreApplication::translate("Hash", "Greek (Not complete)", nullptr));
        actionSHA224->setText(QCoreApplication::translate("Hash", "SHA224", nullptr));
        actionSHA256->setText(QCoreApplication::translate("Hash", "SHA256", nullptr));
        actionSHA384->setText(QCoreApplication::translate("Hash", "SHA384", nullptr));
        actionSHA512->setText(QCoreApplication::translate("Hash", "SHA512", nullptr));
        actionSHA3_224->setText(QCoreApplication::translate("Hash", "SHA3_224", nullptr));
        actionSHA3_256->setText(QCoreApplication::translate("Hash", "SHA3_256", nullptr));
        actionSHA3_384->setText(QCoreApplication::translate("Hash", "SHA3_384", nullptr));
        actionSHA3_512->setText(QCoreApplication::translate("Hash", "SHA3_512", nullptr));
        actionLicense->setText(QCoreApplication::translate("Hash", "License...", nullptr));
        actionVersionHistory->setText(QCoreApplication::translate("Hash", "Version history...", nullptr));
        actionOpenHashSumList->setText(QCoreApplication::translate("Hash", "Open Hash Sum List", nullptr));
        actionOpenComparisonList->setText(QCoreApplication::translate("Hash", "Open Comparison list", nullptr));
        actionGerman->setText(QCoreApplication::translate("Hash", "German (Not complete)", nullptr));
        actionSaveToFile->setText(QCoreApplication::translate("Hash", "Save to file", nullptr));
        actionKeccak_224->setText(QCoreApplication::translate("Hash", "Keccak_224", nullptr));
        actionKeccak_256->setText(QCoreApplication::translate("Hash", "Keccak_256", nullptr));
        actionKeccak_384->setText(QCoreApplication::translate("Hash", "Keccak_384", nullptr));
        actionKeccak_512->setText(QCoreApplication::translate("Hash", "Keccak_512", nullptr));
        actionFindIdenticalFiles->setText(QCoreApplication::translate("Hash", "Find identical files", nullptr));
        actionIsThereADuplicate->setText(QCoreApplication::translate("Hash", "Are there duplicates?", nullptr));
        actionAlwaysOpenTheDisplayWindow->setText(QCoreApplication::translate("Hash", "Always open the display window", nullptr));
        actionUninstall->setText(QCoreApplication::translate("Hash", "Uninstall", nullptr));
        actionMainteanceTool->setText(QCoreApplication::translate("Hash", "Mainteance Tool...", nullptr));
        actionDesktopShortcut->setText(QCoreApplication::translate("Hash", "Desktop Shortcut", nullptr));
        actionApplicationsMenuShortcut->setText(QCoreApplication::translate("Hash", "Applications menu Shortcut", nullptr));
        actionSilent->setText(QCoreApplication::translate("Hash", "Silent", nullptr));
        actionAboutQt->setText(QCoreApplication::translate("Hash", "About Qt...", nullptr));
        progressLabel->setText(QString());
        menuFile->setTitle(QCoreApplication::translate("Hash", "&File", nullptr));
        menuRecentFiles->setTitle(QCoreApplication::translate("Hash", "Recent Files", nullptr));
        menuAlgorithm->setTitle(QCoreApplication::translate("Hash", "&Algorithm", nullptr));
        menuTools->setTitle(QCoreApplication::translate("Hash", "&Tools", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("Hash", "&Help", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("Hash", "&Language", nullptr));
        menuEdit->setTitle(QCoreApplication::translate("Hash", "&Edit", nullptr));
        menuText->setTitle(QCoreApplication::translate("Hash", "&Text", nullptr));
        menuSpeech->setTitle(QCoreApplication::translate("Hash", "Speech", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Hash: public Ui_Hash {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HASH_H
