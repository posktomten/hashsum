/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "hash.h"
#include "ui_hash.h"
#include "ui_license.h"
#include "extqlineedit.h"



//#include <algorithm>
void Hash::findDuplicates()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionSaveToFile->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    hurmangaFoldrar = 0;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qs = QString::fromStdString(s);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs + QDir::toNativeSeparators("/");
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Select a folder to Find identical files"),
                                          sokvag,
                                          tr("All Files (*.*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    // dialog->setFileMode(QFileDialog::DirectoryOnly);
    dialog->setOption(QFileDialog::ShowDirsOnly, true);
    // dialog->setOption(QFileDialog::ShowDirsOnly, true);
    dialog->setOption(QFileDialog::DontResolveSymlinks);
    dialog->setReadOnly(true);
    QString directoryName;

    if(dialog->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir = dialog->directory();
        directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
    } else {
        delete k;
        return;
    }

    QVector<Hash::PathHash> v_ph;
    std::string algoritm = k->getConf("algorithm");
    QString alg = QString::fromStdString(algoritm);
    std::string open_path = k->getConf("open_path");
    QString qopen_path = QString::fromStdString(open_path);
    delete k;
    /*  */
    bool lyckats = rekursivt2(directoryName, v_ph, alg, qopen_path);

    if(!lyckats) {
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not check the hash sum. Check your file permissions."));
        tesp(tr("Could not check the hash sum. Check your file permissions."));
    }

    collectDuplicates(v_ph, directoryName);
}

bool Hash::rekursivt2(const QDir &path, QVector<PathHash> &v_ph, const QString &alg, const QString &qopen_path)
{
    // const QString &conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    QFileInfoList list = path.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    PathHash ph;

    foreach(QFileInfo finfo, list) {
        if(finfo.isDir()) {
            rekursivt2(QDir(finfo.absoluteFilePath()), v_ph, alg, qopen_path);
            hurmangaFoldrar++;
        } else {
            ph.path = finfo.absoluteFilePath();
            ph.hash = checksum2(finfo.absoluteFilePath(), alg);
            v_ph.append(ph);
            hurmangaFiler++;
        }
    }

    QString tmp = "";
    QApplication::processEvents();

    if(hurmangaFoldrar < 2) {
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
    } else {
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folders for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
    }

    ui->statusBar->showMessage(tr("Path: \"") + QDir::toNativeSeparators(path.path()) + "\"");

    if(QDir::toNativeSeparators(path.path()) == QDir::toNativeSeparators(qopen_path)) {
        if(hurmangaFoldrar < 2) {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
//            tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
        } else {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
//            tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folders for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
        }

        ui->statusBar->showMessage(tr("I have checked all folders and checked all hash sums! Mission accomplished."));
        tesp(tr("I have checked all folders and checked all hash sums! Mission accomplished."));
        QString s;
        s.setNum(hurmangaFiler);
        ui->lineEdit->setText(tr("Mission accomplished! I have calculated the hash sum of ") + s + tr(" files."));
        tesp(tr("I have calculated the hash sum of ") + s + tr(" files."));
        hurmangaFiler = 0;
    }

    return true;
}



QString Hash::checksum2(const QString &in, const QString &s)
{
    QByteArray ba;
    QFile f(in.toUtf8());

    if(f.open(QFile::ReadOnly)) {
        ba = f.readAll();
        f.close();
    } else
        return "0";

    QByteArray sum;
    QString ut;

    if(s == "md4")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md4);
    else if(s == "md5")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md5);
    else if(s == "sha1")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha1);
    else if(s == "sha224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha224);
    else if(s == "sha256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha256);
    else if(s == "sha384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha384);
    else if(s == "sha512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha512);
    else if(s == "sha3_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_224);
    else if(s == "sha3_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_256);
    else if(s == "sha3_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_384);
    else if(s == "sha3_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_512);
    else if(s == "keccak_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_224);
    else if(s == "keccak_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_256);
    else if(s == "keccak_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_384);
    else if(s == "keccak_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_512);

    sum = sum.toHex();
    ut =  sum;
    return ut;
}

void Hash::collectDuplicates(QVector<PathHash> &v_ph, const QString &directoryNamne)
{
    QStringList path;
    QStringList hash;
    int antalandradefiler = 0;

    foreach(Hash::PathHash hp, v_ph) {
        hash.append(hp.hash);
        path.append(hp.path);
    }

    QStringList tmp = hash;

    for(int i = 0; i < hash.size(); i++) {
        tmp[i] = "0";

        if(!tmp.contains(hash[i])) {
            path[i] = "0";
        }

        tmp = hash;
    }

    hash = tmp;
    QVector<Hash::PathHash> ny;

    for(int i = 0; i < path.size(); i++) {
        if(path[i] != "0") {
            ny.append(v_ph[i]);
        }
    }

    auto sortFunc = [](const PathHash & a, const PathHash & b) -> bool {
        return a.hash < b.hash;
    };
    std::sort(ny.begin(), ny.end(), sortFunc);
    /**** ****/
    d = new QDialog;
    ui2 = new Ui::Lic;
    //Font ui2
    int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont f(family);
    // SLUT Font ui2
    ui2->setupUi(d);
    ui2->textBrowser->setFont(f);
    ui2->pbOpenFile->setText(tr("Save"));
    ui2->textBrowser->append(tr("Created: ") + datumtid());
    ui2->textBrowser->append(tr("Recursively from ") + QDir::toNativeSeparators(directoryNamne) + "\n");
    /* */
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c); // set color "Red" for textedit base
    ui2->textBrowser->setPalette(p); // change textedit palette
    /* */

    /***** *******/
    if(ny.size() > 1)
        antalandradefiler = 1;

    for(int i = 0, j = -1; i < ny.size(); i++, j++) {
        if(j >= 0) {
            if(ny[j].hash != ny[i].hash) {
                ui2->textBrowser->append("\n");
                antalandradefiler++;
            }
        }

        ui2->textBrowser->append(QDir::toNativeSeparators(ny[i].path));
    }

    ui2->textBrowser->append("\n" + tr("The number of files with one or more duplicates: ") + QString::number(antalandradefiler));
    tesp(tr("The number of files with one or more duplicates: ") + QString::number(antalandradefiler));
    /*****      *****/
    d->setWindowModality(Qt::ApplicationModal);
    d->setWindowFlags(d->windowFlags() | Qt::WindowMinMaxButtonsHint);
    d->setWindowTitle(APPLICATION_NAME " " VERSION "  " + tr("Duplicate files"));
    QIcon ikon("/images/hashsum.ico");
    d->setWindowIcon(ikon);
    connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
    connect(ui2->pbOpenFile, SIGNAL(pressed()), this, SLOT(save()));
    QTextCursor cursor = ui2->textBrowser->textCursor();
    cursor.setPosition(0);
    ui2->textBrowser->setTextCursor(cursor);
    d->show();
    /*****      *****/
}
