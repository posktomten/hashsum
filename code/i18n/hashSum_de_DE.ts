<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="48"/>
        <location filename="../checkforupdates.cpp" line="110"/>
        <location filename="../checkforupdates.cpp" line="113"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Es wurde keine Internetverbindung gefunden.
Bitte überprüfen Sie Ihre Interneteinstellungen und Firewall.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <location filename="../checkforupdates.cpp" line="119"/>
        <source>Where is a new version of hashSum. Latest Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <source>, Please download. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>&lt;br&gt;There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>.&lt;br&gt;Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source> is newer than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="71"/>
        <source>You have the latest version of hashsum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="79"/>
        <location filename="../checkforupdates.cpp" line="125"/>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>Latest Version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
There is a later version of </source>
        <oldsource>
There is a newer version of </oldsource>
        <translation type="vanished">
Es gibt eine spätere Version von </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Bitte herunterladen von</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="45"/>
        <source>No Internet connection was found. Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Latest version: </source>
        <translation type="vanished">Neueste Version: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="63"/>
        <source>Your version of hashsum is newer than the latest official version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source>
Your version of </source>
        <translation>
Ihre Version von </translation>
    </message>
    <message>
        <source> is later than the latest official version </source>
        <translation type="vanished"> ist neuer als die neueste offizielle Version </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="74"/>
        <source>
You have the latest version of </source>
        <translation>
Sie haben die neueste Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="82"/>
        <location filename="../checkforupdates.cpp" line="128"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Es gab einen Fehler bei der Überprüfung der Version.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="165"/>
        <source>New updates:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation>Mission</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumme wurde in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Doppelklicken Sie zum Kopieren in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1057"/>
        <location filename="../hash.cpp" line="1058"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Doppelklicken Sie zum Kopieren in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation>&amp;Algorithmus</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="140"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="152"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="161"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="170"/>
        <source>&amp;Text</source>
        <translation>&amp;Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="424"/>
        <source>Hash sum</source>
        <translation>Hashsumme</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation>Letzte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="177"/>
        <source>Speech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="197"/>
        <source>Check file or files</source>
        <oldsource>Check file</oldsource>
        <translation>Datei prüfen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="209"/>
        <source>Check for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="218"/>
        <location filename="../hash.ui" line="221"/>
        <location filename="../hash.ui" line="224"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation>Über...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="232"/>
        <source>Check for program updates at program start</source>
        <translation>Beim Programmstart nach Programmaktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="244"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="247"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="255"/>
        <source>MD4</source>
        <translation>MD4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="263"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="271"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="276"/>
        <source>Compare two files</source>
        <oldsource>Compare</oldsource>
        <translation>Zwei Dateien vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="285"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="294"/>
        <source>Swedish</source>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="299"/>
        <location filename="../hash.ui" line="302"/>
        <source>Create hash sum list</source>
        <translation>Hashsummenliste erstellen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="310"/>
        <source>Save hash sum list where the files are</source>
        <translation>Hashsummenliste dort speichern, wo sich die Dateien befinden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="315"/>
        <source>Default file name</source>
        <translation>Standard-Dateiname</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="320"/>
        <source>Create hash sum lists, recursively</source>
        <translation>Hashsummenlisten erstellen, rekursiv</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="328"/>
        <source>Write date and time in the hash sum file</source>
        <translation>Datum und Zeit in die Hashsummendatei schreiben</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="337"/>
        <source>Find changes</source>
        <translation>Änderungen suchen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="345"/>
        <source>Use native dialogs</source>
        <translation>Native Dialoge verwenden</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="353"/>
        <source>Allways open the home directory</source>
        <translation>Benutzerverzeichnis immer öffnen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="358"/>
        <location filename="../hash.ui" line="429"/>
        <source>Compare with a given hash sum</source>
        <translation>Mit einer gegebenen Hashsumme vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="366"/>
        <source>Save as text (*.*)</source>
        <oldsource>Save as text (*)</oldsource>
        <translation>Als Text speichern (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="374"/>
        <source>Save as pdf (*.pdf)</source>
        <translation>Als pdf speichern (*.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="382"/>
        <source>Color pdf</source>
        <translation>Farb-PDF</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="394"/>
        <source>Copy Path</source>
        <translation>Pfad kopieren</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="406"/>
        <source>Copy Hash Sum</source>
        <translation>Hashsumme kopieren</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="414"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <oldsource>Save as text and pdf (* and *.pdf)</oldsource>
        <translation>Als Text und pdf speichern (*.* und *.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="419"/>
        <source>Hash sum from text</source>
        <translation>Hashsumme aus Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="438"/>
        <location filename="../hash.ui" line="441"/>
        <location filename="../hash.ui" line="444"/>
        <source>Help...</source>
        <oldsource>Help</oldsource>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="450"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="459"/>
        <source>Create Debian md5sums</source>
        <oldsource>Create debian md5sums</oldsource>
        <translation>Debian md5sums erstellen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="467"/>
        <source>Show full path in the hash sum file</source>
        <translation>Vollständigen Pfad in der Hashsummendatei anzeigen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="476"/>
        <source>Create Debian md5sums (auto)</source>
        <translation>Debian md5sums erstellen (automatisch)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="485"/>
        <source>Greek (Not complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="561"/>
        <source>License...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="570"/>
        <source>Version history...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="589"/>
        <source>German (Not complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="659"/>
        <location filename="../hash.cpp" line="308"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="670"/>
        <source>Mainteance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="681"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="701"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="715"/>
        <source>Silent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="724"/>
        <source>About Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Griechisch</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="493"/>
        <source>SHA224</source>
        <translation>SHA224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="501"/>
        <source>SHA256</source>
        <translation>SHA256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="509"/>
        <source>SHA384</source>
        <translation>SHA384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="517"/>
        <source>SHA512</source>
        <translation>SHA512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="525"/>
        <source>SHA3_224</source>
        <translation>SHA3_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="533"/>
        <source>SHA3_256</source>
        <translation>SHA3_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="541"/>
        <source>SHA3_384</source>
        <translation>SHA3_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="552"/>
        <source>SHA3_512</source>
        <translation>SHA3_512</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Lizenz</translation>
    </message>
    <message>
        <source>Version history</source>
        <translation type="vanished">Versionshistorie</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="575"/>
        <source>Open Hash Sum List</source>
        <translation>Hashsummenliste öffnen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="580"/>
        <source>Open Comparison list</source>
        <translation>Vergleichsliste öffnen</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Deutsche</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="597"/>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="605"/>
        <source>Keccak_224</source>
        <translation>Keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="613"/>
        <source>Keccak_256</source>
        <translation>Keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="621"/>
        <source>Keccak_384</source>
        <translation>Keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="629"/>
        <source>Keccak_512</source>
        <translation>Keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="634"/>
        <source>Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="639"/>
        <source>Are there duplicates?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="647"/>
        <source>Always open the display window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1064"/>
        <location filename="../hash.cpp" line="1065"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumme wurde in die Zwischenablage kopiert</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1092"/>
        <location filename="../hash.cpp" line="1888"/>
        <location filename="../hash.cpp" line="1889"/>
        <source>Open a file to calculate the hash sum</source>
        <translation>Öffnen Sie eine Datei, um die Hashsumme zu berechnen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="836"/>
        <location filename="../hash.cpp" line="1094"/>
        <location filename="../hash.cpp" line="1891"/>
        <location filename="../hash.cpp" line="2011"/>
        <location filename="../hash.cpp" line="2055"/>
        <location filename="../hash.cpp" line="2125"/>
        <location filename="../hash.cpp" line="2317"/>
        <location filename="../isthereaduplicate.cpp" line="45"/>
        <source>All Files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1126"/>
        <location filename="../hash.cpp" line="1127"/>
        <location filename="../hash.cpp" line="1930"/>
        <location filename="../hash.cpp" line="1931"/>
        <location filename="../hash.cpp" line="1942"/>
        <location filename="../hash.cpp" line="2562"/>
        <location filename="../hash.cpp" line="3846"/>
        <location filename="../hash.cpp" line="3847"/>
        <location filename="../hash.cpp" line="3855"/>
        <location filename="../hash.cpp" line="3856"/>
        <location filename="../isthereaduplicate.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="81"/>
        <source>No hash sum could be calculated</source>
        <translation>Es konnte keine Hashsumme berechnet werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1130"/>
        <location filename="../hash.cpp" line="1134"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation>Und die Hashsumme, die Sie vergleichen, ist: &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1130"/>
        <source>&quot; and they are the same</source>
        <translation>&quot; und sie sind gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1131"/>
        <location filename="../hash.cpp" line="1132"/>
        <source>The hash sums are equal</source>
        <translation>Die Hashsummen sind gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1134"/>
        <source>&quot; and they are NOT the same</source>
        <translation>&quot; und sie sind NICHT gleich</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1136"/>
        <source>The hash sums are NOT equal</source>
        <translation>Die Hashsummen sind NICHT gleich</translation>
    </message>
    <message>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <oldsource>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </oldsource>
        <translation type="vanished">Dieses Programm ist freie Software; Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weiterverbreiten und/oder modifizieren; entweder Version </translation>
    </message>
    <message>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation type="vanished"> der Lizenz oder (nach Ihrer Wahl) eine spätere Version. Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEGLICHE GEWÄHRLEISTUNG verteilt; ohne auch nur die implizierte Gewährleistung der MARKTGÄNGIGKEIT oder BRAUCHBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Weitere Informationen finden Sie in der GNU General Public License. Sie sollten eine Kopie der GNU General Public License zusammen mit diesem Programm erhalten haben; Wenn nicht, schreiben Sie an die Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <source>Phone: </source>
        <translation type="vanished">Telefon: </translation>
    </message>
    <message>
        <source>About </source>
        <translation type="vanished">Über </translation>
    </message>
    <message>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation type="vanished">&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1334"/>
        <location filename="../hash.cpp" line="1370"/>
        <location filename="../hash.cpp" line="1406"/>
        <location filename="../hash.cpp" line="1442"/>
        <location filename="../hash.cpp" line="1478"/>
        <location filename="../hash.cpp" line="1514"/>
        <location filename="../hash.cpp" line="1550"/>
        <location filename="../hash.cpp" line="1586"/>
        <location filename="../hash.cpp" line="1622"/>
        <location filename="../hash.cpp" line="1658"/>
        <location filename="../hash.cpp" line="1694"/>
        <location filename="../hash.cpp" line="1730"/>
        <location filename="../hash.cpp" line="1768"/>
        <location filename="../hash.cpp" line="1804"/>
        <location filename="../hash.cpp" line="1840"/>
        <location filename="../hash.cpp" line="1973"/>
        <location filename="../hash.cpp" line="2628"/>
        <location filename="../hash.cpp" line="2944"/>
        <location filename="../hash.cpp" line="3014"/>
        <location filename="../hash.cpp" line="3099"/>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <location filename="../hash.cpp" line="3708"/>
        <location filename="../hash.cpp" line="3883"/>
        <location filename="../hash.cpp" line="4003"/>
        <source>Algorithm: </source>
        <translation>Algorithmus: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1354"/>
        <location filename="../hash.cpp" line="1390"/>
        <location filename="../hash.cpp" line="1426"/>
        <location filename="../hash.cpp" line="1462"/>
        <location filename="../hash.cpp" line="1498"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1570"/>
        <location filename="../hash.cpp" line="1606"/>
        <location filename="../hash.cpp" line="1642"/>
        <location filename="../hash.cpp" line="1678"/>
        <location filename="../hash.cpp" line="1714"/>
        <location filename="../hash.cpp" line="1750"/>
        <location filename="../hash.cpp" line="1788"/>
        <location filename="../hash.cpp" line="1824"/>
        <location filename="../hash.cpp" line="1860"/>
        <location filename="../hash.cpp" line="2284"/>
        <location filename="../hash.cpp" line="2291"/>
        <location filename="../hash.cpp" line="3707"/>
        <source>Default file name: </source>
        <translation>Standard-Dateiname: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1941"/>
        <source>No hash sum could be calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1945"/>
        <source>hash sum has been calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1950"/>
        <source>hash sums have been calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2009"/>
        <source>Open two files to compare their hash sums.</source>
        <translation>Öffnen Sie zwei Dateien, um ihre Hashsummen zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2034"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Sie müssen genau zwei Dateien auswählen.
Zwei Dateien in diesem Ordner oder eine Datei in diesem Ordner und eine zweite Datei in einem anderen Ordner Ihrer Wahl.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2035"/>
        <source>You must select exactly two files. Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2052"/>
        <location filename="../hash.cpp" line="2053"/>
        <source>Open the second file to compare with.</source>
        <translation>Öffnen Sie die zweite Datei, um sie zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2070"/>
        <location filename="../hash.cpp" line="2071"/>
        <source>You must select exactly one file.</source>
        <translation>Sie müssen genau eine Datei auswählen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2082"/>
        <location filename="../hash.cpp" line="2083"/>
        <source>You have compared the same file with itself.</source>
        <translation>Sie haben die gleiche Datei mit sich selbst verglichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2098"/>
        <location filename="../hash.cpp" line="2099"/>
        <source>The files could not be compared.</source>
        <translation>Die Dateien konnten nicht verglichen werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2123"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation>Wählen Sie einen Ordner aus, um Hashsummenlisten rekursiv zu erzeugen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="868"/>
        <location filename="../hash.cpp" line="2154"/>
        <location filename="../hash.cpp" line="2155"/>
        <location filename="../hash.cpp" line="2454"/>
        <location filename="../hash.cpp" line="2843"/>
        <location filename="../hash.cpp" line="2939"/>
        <location filename="../hash.cpp" line="3009"/>
        <location filename="../hash.cpp" line="3807"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <translation>Es konnte keine Datei zum Ablegen von Hashsummen gespeichert werden. Überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2168"/>
        <location filename="../hash.cpp" line="2193"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2245"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Das Programm muss neu gestartet werden, damit die neuen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2169"/>
        <location filename="../hash.cpp" line="2194"/>
        <location filename="../hash.cpp" line="2220"/>
        <location filename="../hash.cpp" line="2246"/>
        <source>Restart Now</source>
        <translation>Jetzt neustarten</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="306"/>
        <location filename="../hash.cpp" line="925"/>
        <location filename="../hash.cpp" line="942"/>
        <location filename="../hash.cpp" line="1005"/>
        <location filename="../hash.cpp" line="1073"/>
        <location filename="../hash.cpp" line="2170"/>
        <location filename="../hash.cpp" line="2195"/>
        <location filename="../hash.cpp" line="2221"/>
        <location filename="../hash.cpp" line="2247"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="987"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Leerzeichen wird nicht entfernt, wenn die Hashsummen berechnet werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1003"/>
        <source>Calculate the hash sum of this text:</source>
        <translation>Hashsumme dieses Textes berechnen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="926"/>
        <location filename="../hash.cpp" line="943"/>
        <location filename="../hash.cpp" line="1006"/>
        <location filename="../hash.cpp" line="1074"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="520"/>
        <location filename="../hash.cpp" line="641"/>
        <source> is not found</source>
        <translation> wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="450"/>
        <location filename="../hash.cpp" line="532"/>
        <location filename="../hash.cpp" line="653"/>
        <source>Open the hashSum file</source>
        <translation>hashSum-Datei öffnen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source>Welcome to hashSum. I use the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source> algorithm when calculating hash sums.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="72"/>
        <source>My name is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="102"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="302"/>
        <source>To the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="305"/>
        <source>Download the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="310"/>
        <source>Download the new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="331"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="397"/>
        <source>&quot;Control Panel\All Control Panel Items\Programs and Features&quot; cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="412"/>
        <source>MaintenanceTool cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="452"/>
        <location filename="../hash.cpp" line="534"/>
        <location filename="../hash.cpp" line="655"/>
        <source>Text files (*)</source>
        <translation>Textdateien (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="485"/>
        <location filename="../hash.cpp" line="568"/>
        <location filename="../hash.cpp" line="608"/>
        <location filename="../hash.cpp" line="696"/>
        <location filename="../hash.cpp" line="731"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="761"/>
        <source>The number of recently opened files to be displayed...</source>
        <oldsource>Set number of recently opened files shown...</oldsource>
        <translation>Die Anzahl der angezeigten zuletzt geöffneten Dateien...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="764"/>
        <source>Clear the list of recently opened files</source>
        <translation>Liste der zuletzt geöffneten Dateien löschen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="771"/>
        <source>The file</source>
        <translation>Die Datei</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="771"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation>kann nicht gefunden werden. Die Datei wird aus der Liste der zuletzt geöffneten Dateien entfernt.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="800"/>
        <source>Set number of Recent Files: </source>
        <oldsource>Set number of Recent Files: (0-30)</oldsource>
        <translation>Anzahl der letzten Dateien festlegen: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="834"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <oldsource>Select the &quot;usr&quot; directory to start create checksums recursively</oldsource>
        <translation>Wählen Sie das Verzeichnis &quot;usr&quot; aus, um Hashsummen rekursiv zu erstellen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="869"/>
        <location filename="../hash.cpp" line="2455"/>
        <location filename="../hash.cpp" line="2844"/>
        <location filename="../hash.cpp" line="2940"/>
        <location filename="../hash.cpp" line="3010"/>
        <location filename="../hash.cpp" line="3808"/>
        <location filename="../hash.cpp" line="3938"/>
        <location filename="../hash.cpp" line="3939"/>
        <location filename="../hash.cpp" line="3995"/>
        <location filename="../hash.cpp" line="3996"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="884"/>
        <source>md5sums file has been successfully created!</source>
        <translation>Datei md5sums wurde erfolgreich erstellt!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="887"/>
        <source>md5sums file has been successfully created in </source>
        <translation>Datei md5sums wurde erfolgreich erstellt in </translation>
    </message>
    <message>
        <source>Help with the program and all settings and menus can be found here:</source>
        <oldsource>Help with the program and all settings and menus can be found here:&lt;br&gt;</oldsource>
        <translation type="vanished">Hilfe zum Programm und allen Einstellungen und Menüs finden Sie hier:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="914"/>
        <source>The program&apos;s website can be found here:</source>
        <translation>Die Webseite des Programms finden Sie hier:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="923"/>
        <source>Compare with this hash sum...</source>
        <translation>Mit dieser Hashsumme vergleichen...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="940"/>
        <source>...with hash sum for this text:</source>
        <translation>...mit Hashsumme für diesen Text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="952"/>
        <location filename="../hash.cpp" line="953"/>
        <location filename="../hash.cpp" line="1019"/>
        <location filename="../hash.cpp" line="1020"/>
        <source> can handle texts up to </source>
        <oldsource> can handle texts of up to </oldsource>
        <translatorcomment>Used &apos;character&apos; from next string for correct sentence construction.</translatorcomment>
        <translation> kann Texte bis zu Zeichen </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="953"/>
        <location filename="../hash.cpp" line="1020"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translatorcomment>Used &apos;handle&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> verarbeiten.
Bitte reduzieren Sie die Länge des Textes.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="952"/>
        <location filename="../hash.cpp" line="1019"/>
        <source> characters. Please reduce the length of the text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="961"/>
        <location filename="../hash.cpp" line="962"/>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="990"/>
        <location filename="../hash.cpp" line="992"/>
        <location filename="../hash.cpp" line="1029"/>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1040"/>
        <location filename="../hash.cpp" line="1043"/>
        <location filename="../hash.cpp" line="1045"/>
        <source>Hash sum calculated for:</source>
        <translation>Hashsumme berechnet für:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="961"/>
        <location filename="../hash.cpp" line="962"/>
        <location filename="../hash.cpp" line="1029"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation>konnte nicht berechnet werden. Ein unbekannter Fehler ist aufgetreten.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="967"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation>Es ist nicht die gleiche wie die Hashsumme, die Sie vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="968"/>
        <location filename="../hash.cpp" line="969"/>
        <source>Hash sums are NOT equal!</source>
        <translation>Hashsummen sind NICHT gleich!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="973"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation>Es ist die gleiche wie die Hashsumme, die Sie vergleichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="974"/>
        <location filename="../hash.cpp" line="975"/>
        <source>Hash sums are equal!</source>
        <translation>Hashsummen sind gleich!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="990"/>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1043"/>
        <source>characters.</source>
        <translation>Zeichen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="992"/>
        <location filename="../hash.cpp" line="1040"/>
        <location filename="../hash.cpp" line="1045"/>
        <source>characters</source>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="995"/>
        <source>Hash sums are compared!</source>
        <translation>Hashsummen werden verglichen!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1028"/>
        <source>Hashsum could not be calculated. An unknown error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1040"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Leerzeichen wird nicht entfernt, wenn die Hashsummen berechnet werden.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1048"/>
        <location filename="../hash.cpp" line="1049"/>
        <source>Hash sum has been calculated!</source>
        <translation>Hashsumme wurde berechnet!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1071"/>
        <source>Compare with this hash sum:</source>
        <translation>Mit dieser Hashsumme vergleichen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1135"/>
        <source>The hash sums are not equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1214"/>
        <source>The license file is not found</source>
        <translation>Die Lizenzdatei wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1260"/>
        <source>The version history file is not found</source>
        <translation>Die Versionshistoriedatei wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source> for the Greek translation.</source>
        <translation> für die griechische Übersetzung.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source>Many thanks to </source>
        <oldsource>Many thanks to &lt;a href=&quot;</oldsource>
        <translation>Vielen Dank an </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source> for the German translation.</source>
        <translation> für die deutsche Übersetzung.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1292"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation>Ein Programm, um Hashsummen zu berechnen und Dateien zu vergleichen.</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source>This program uses Qt version </source>
        <translation>Dieses Programm verwendet Qt-Version </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source> running on </source>
        <translation> läuft auf </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="40"/>
        <location filename="../system.cpp" line="44"/>
        <location filename="../system.cpp" line="48"/>
        <location filename="../system.cpp" line="124"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="56"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1354"/>
        <location filename="../hash.cpp" line="1390"/>
        <location filename="../hash.cpp" line="1426"/>
        <location filename="../hash.cpp" line="1462"/>
        <location filename="../hash.cpp" line="1498"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1570"/>
        <location filename="../hash.cpp" line="1606"/>
        <location filename="../hash.cpp" line="1642"/>
        <location filename="../hash.cpp" line="1678"/>
        <location filename="../hash.cpp" line="1714"/>
        <location filename="../hash.cpp" line="1750"/>
        <location filename="../hash.cpp" line="1788"/>
        <location filename="../hash.cpp" line="1824"/>
        <location filename="../hash.cpp" line="1860"/>
        <source>(Click to change)</source>
        <translation>(Klicken Sie, um zu ändern)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1357"/>
        <source>I use the algorithm md4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1393"/>
        <source>I use the algorithm md5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1429"/>
        <source>I use the algorithm sha1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1465"/>
        <source>I use the algorithm sha224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1501"/>
        <source>I use the algorithm sha256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1537"/>
        <source>I use the algorithm sha384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1573"/>
        <source>I use the algorithm sha512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1609"/>
        <source>I use the algorithm sha3_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1645"/>
        <source>I use the algorithm sha3_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1681"/>
        <source>I use the algorithm sha3_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1717"/>
        <source>I use the algorithm sha3_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1753"/>
        <source>I use the algorithm keccak_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1791"/>
        <source>I use the algorithm keccak_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1827"/>
        <source>I use the algorithm keccak_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1863"/>
        <source>I use the algorithm keccak_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2165"/>
        <source>Please restart hashsum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2280"/>
        <source>Default file name:</source>
        <translation>Standard-Dateiname:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2284"/>
        <location filename="../hash.cpp" line="2291"/>
        <location filename="../hash.cpp" line="3707"/>
        <source> (Click to change)</source>
        <translation> (Klicken Sie, um zu ändern)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2315"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <translation>Dateien öffnen, um eine Liste der Hashsummen zu erstellen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2366"/>
        <location filename="../hash.cpp" line="2388"/>
        <location filename="../hash.cpp" line="2406"/>
        <source>Did you select all files you want to choose?</source>
        <translation>Haben Sie alle Dateien ausgewählt, die Sie aussuchen möchten?</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2367"/>
        <location filename="../hash.cpp" line="2389"/>
        <location filename="../hash.cpp" line="2407"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2368"/>
        <location filename="../hash.cpp" line="2390"/>
        <location filename="../hash.cpp" line="2408"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2428"/>
        <source>Save the Debian md5sums file</source>
        <translation>Debian md5sums-Datei speichern</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2463"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation>Dies ist kein gültiger Pfad zum Bauen eines Debian-Pakets!</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="46"/>
        <location filename="../isthereaduplicate.cpp" line="94"/>
        <source>Select a folder to Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="48"/>
        <location filename="../isthereaduplicate.cpp" line="96"/>
        <source>All Files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="83"/>
        <location filename="../duplicates.cpp" line="84"/>
        <location filename="../isthereaduplicate.cpp" line="124"/>
        <location filename="../isthereaduplicate.cpp" line="125"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>I have been looking into </source>
        <translatorcomment>Translated &apos;looking into&apos; in next string for correct sentence construction.</translatorcomment>
        <translation>Ich habe in </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translatorcomment>Included &apos;looking into&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> Ordner wegen Dateien nachgesehen. Ich habe es geschafft, die Hashsumme in allen Ordnern mit Dateien zu berechnen.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>(Algorithm: </source>
        <translation>(Algorithmus: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translatorcomment>Included &apos;looking into&apos; from last string for correct sentence construction.</translatorcomment>
        <translation> Ordnern wegen Dateien nachgesehen. Ich habe es geschafft, die Hashsumme in allen Ordnern mit Dateien zu berechnen.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <source>Path: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="130"/>
        <location filename="../duplicates.cpp" line="131"/>
        <location filename="../isthereaduplicate.cpp" line="173"/>
        <location filename="../isthereaduplicate.cpp" line="174"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="239"/>
        <location filename="../isthereaduplicate.cpp" line="196"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="241"/>
        <source>Recursively from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="264"/>
        <location filename="../duplicates.cpp" line="265"/>
        <source>The number of files with one or more duplicates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="269"/>
        <location filename="../isthereaduplicate.cpp" line="222"/>
        <source>Duplicate files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3917"/>
        <location filename="../hash.cpp" line="3974"/>
        <source>Save the hash summary list as a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3917"/>
        <location filename="../hash.cpp" line="3974"/>
        <source>Any file (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1972"/>
        <location filename="../hash.cpp" line="2520"/>
        <location filename="../hash.cpp" line="3882"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>Path: </source>
        <translation>Pfad: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2529"/>
        <location filename="../hash.cpp" line="2530"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation>Ich habe alle Ordner überprüft und alle Dateien erstellt! Mission erfüllt.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation>Mission erfüllt! Ich habe die Hashsumme berechnet von </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source> files.</source>
        <translation> Dateien.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2894"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation>Hashsummenwerte wurden mit alten Werten in der Datei verglichen</translation>
    </message>
    <message>
        <source>CPU architecture:</source>
        <oldsource>CPU architecture</oldsource>
        <translation type="vanished">CPU-Architektur:</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10</source>
        <translation type="vanished">Windows-Betriebssystemversion 10.0, entspricht Windows 10</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2678"/>
        <location filename="../hash.cpp" line="2913"/>
        <source>Text Files (*)</source>
        <translation>Textdateien (*)</translation>
    </message>
    <message>
        <source>Could not save a file to store Recent File list. Check your file permissions.</source>
        <translation type="vanished">Es konnte keine Datei zum Ablegen der Liste letzter Dateien gespeichert werden. Überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <source>Unknown Windows version.</source>
        <translation type="vanished">Unbekannte Windows-Version.</translation>
    </message>
    <message>
        <source>MS-DOS-based version of Windows.</source>
        <translation type="vanished">MS-DOS-basierte Version von Windows.</translation>
    </message>
    <message>
        <source>NT-based version of Windows.</source>
        <translation type="vanished">NT-basierte Version von Windows.</translation>
    </message>
    <message>
        <source>CE-based version of Windows.</source>
        <translation type="vanished">CE-basierte Version von Windows.</translation>
    </message>
    <message>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation type="vanished">Windows 3.1 mit Win-32-Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows 95 operating system.</source>
        <translation type="vanished">Windows 95 Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows 98 operating system.</source>
        <translation type="vanished">Windows 98 Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows Me operating system.</source>
        <translation type="vanished">Windows Me Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation type="vanished">Windows-Betriebssystemversion 4.0, entspricht Windows NT.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation type="vanished">Windows-Betriebssystemversion 5.0, entspricht Windows 2000.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation type="vanished">Windows-Betriebssystemversion 5.1, entspricht Windows XP.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation type="vanished">Windows-Betriebssystemversion 5.2, entspricht Windows Server 2003, Windows Server 2003 R2, Windows Home Server und Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation type="vanished">Windows-Betriebssystemversion 6.0, entspricht Windows Vista und Windows Server 2008.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation type="vanished">Windows-Betriebssystemversion 6.1, entspricht Windows 7 und Windows Server 2008 R2.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.2, corresponds to Windows 8.</source>
        <translation type="vanished">Windows-Betriebssystemversion 6.2, entspricht Windows 8.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.3, corresponds to Windows 8.1.</source>
        <translation type="vanished">Windows-Betriebssystemversion 6.3, entspricht Windows 8.1.</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10.</source>
        <translation type="obsolete">Windows-Betriebssystemversion 10.0, entspricht Windows 10.</translation>
    </message>
    <message>
        <source>Windows CE operating system.</source>
        <translation type="vanished">Windows CE Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows CE .NET operating system.</source>
        <translation type="vanished">Windows CE .NET Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows CE 5.x operating system.</source>
        <translation type="vanished">Windows CE 5.x Betriebssystem.</translation>
    </message>
    <message>
        <source>Windows CE 6.x operating system.</source>
        <translation type="vanished">Windows CE 6.x Betriebssystem.</translation>
    </message>
    <message>
        <source>Architecture instruction set: </source>
        <translation type="vanished">Architektur-Befehlssatz: </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source> was created </source>
        <translation> wurde erstellt </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompiliert von</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="120"/>
        <source>Unknown version</source>
        <translation>Unbekannte Version</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="129"/>
        <source>Unknown compiler.</source>
        <translation>Unbekannter Kompiler.</translation>
    </message>
    <message>
        <source>Screen resolution:</source>
        <translation type="vanished">Bildschirmauflösung:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2469"/>
        <location filename="../hash.cpp" line="2640"/>
        <location filename="../hash.cpp" line="2956"/>
        <location filename="../hash.cpp" line="3026"/>
        <source> No hash sum could be calculated</source>
        <translation>Es konnte keine Hashsumme berechnet werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2545"/>
        <source>This is not a valid path to build a Debian package!</source>
        <oldsource>This is not a valid path to build a debian package!!</oldsource>
        <translation>Dies ist kein gültiger Pfad zum Bauen eines Debian-Pakets!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2631"/>
        <source>Files in:</source>
        <translation>Dateien in:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3033"/>
        <location filename="../hash.cpp" line="3042"/>
        <source>Hash sum for </source>
        <translation>Hashsumme für </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3033"/>
        <location filename="../hash.cpp" line="3042"/>
        <source> of </source>
        <translation> von </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="3033"/>
        <source> files was successfully calculated</source>
        <translation> Dateien wurden erfolgreich berechnet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2651"/>
        <location filename="../hash.cpp" line="2876"/>
        <location filename="../hash.cpp" line="2964"/>
        <location filename="../hash.cpp" line="3034"/>
        <source>Copyright </source>
        <translation>Urheberrecht </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="240"/>
        <location filename="../hash.cpp" line="2654"/>
        <location filename="../hash.cpp" line="2879"/>
        <location filename="../hash.cpp" line="2967"/>
        <location filename="../hash.cpp" line="3037"/>
        <location filename="../isthereaduplicate.cpp" line="205"/>
        <source>Created: </source>
        <translation>Erstellt: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2676"/>
        <source>Open the old hash sum file to compare with</source>
        <translation>Öffnen Sie die alte Hashsummendatei, um sie zu vergleichen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2703"/>
        <source>Hash sum could not be estimated</source>
        <translation>Hashsumme konnte nicht geschätzt werden</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source>Could not open </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source> with hash sums. Make sure the file exists and check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2714"/>
        <source>Could not open &quot;</source>
        <translatorcomment>Translated &apos;not open&apos; in next string for correct sentence construction.</translatorcomment>
        <translation>Konnte &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2714"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translatorcomment>Included &apos;not open&apos; from last string for correct sentence construction.</translatorcomment>
        <translation>&quot; mit Hashsummen nicht öffnen.
Stellen Sie sicher, dass die Datei vorhanden ist und überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2729"/>
        <location filename="../hash.cpp" line="2730"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation>Die alte Hashsummendatei enthält keinen Pfad zu den Dateien. Sie können nicht vergleichen!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2848"/>
        <source>Unchanged files</source>
        <translation>Ungeänderte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2855"/>
        <source>Changed files</source>
        <translation>Geänderte Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2869"/>
        <source>New files</source>
        <translation>Neue Dateien</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2892"/>
        <source>Hash Sum values have been compared to old values in the file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2893"/>
        <location filename="../hash.cpp" line="2979"/>
        <location filename="../hash.cpp" line="3041"/>
        <source>File: </source>
        <translation>Datei: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3042"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation> Dateien wurde erfolgreich berechnet (Algorithmus: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2427"/>
        <source>Files (*.*)</source>
        <oldsource>Files (*)</oldsource>
        <translation>Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1148"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2008"/>
        <source>Open the first file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2862"/>
        <source>Files not tested</source>
        <translation>Dateien nicht getestet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2914"/>
        <source>Save the file with the hash sums</source>
        <translation>Datei mit den Hashsummen speichern</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2980"/>
        <source> files was successfully calculated. Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3099"/>
        <source>&quot; has hash sum</source>
        <translation>&quot; hat Hashsumme</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <source>&quot; and &quot;</source>
        <translation>&quot; und &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <source>&quot; have these hash sums:</source>
        <translation>&quot; haben diese Hashsummen:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <source>. The files are identical.</source>
        <translation>. Die Dateien sind identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3333"/>
        <location filename="../hash.cpp" line="3335"/>
        <source>The files are identical.</source>
        <translation>Die Dateien sind identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3340"/>
        <source>. The files are NOT identical.</source>
        <translation>. Die Dateien sind NICHT identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3339"/>
        <location filename="../hash.cpp" line="3341"/>
        <source>The files are NOT identical.</source>
        <translation>Die Dateien sind NICHT identisch.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3344"/>
        <location filename="../hash.cpp" line="3348"/>
        <source> and </source>
        <translation> und </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>Open the file you want to find duplicates for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source>I have calculated the hash sum of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="207"/>
        <source>Identical files with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="209"/>
        <source>Recursively searched from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source>I found </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source> duplicates.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Datei öffnen</translation>
    </message>
</context>
</TS>
