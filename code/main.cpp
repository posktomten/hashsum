/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include <QTranslator>
#include <QLocale>



#include <string>
#include "config.h"
#include "hash.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //Font
    QFontDatabase::addApplicationFont(":/fonts/PTSans-Regular.ttf");
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QFont f = QFont("PT Sans", WINDOWS_FONT);
#endif
#ifdef Q_OS_LINUX     // Linux
    QFont f = QFont("PT Sans", LINUX_FONT);
#endif
    //  a.setFont(f);
    QApplication::setFont(f);
    // End Font
    // const QString INSTALL_DIR = QDir::toNativeSeparators(a.applicationDirPath() + "/");
    QTranslator translator;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    Config *k = new Config(conf_file_name);
    std::string s = k->getConf("language");
    // QString sprak = QString::fromStdString(s);
    QString sprak = QString::fromStdString(s);
#ifdef Q_OS_LINUX
    const QString translationPath = ":/i18n/linux/hashSum";
#endif
#ifdef Q_OS_WINDOWS
    const QString translationPath = ":/i18n/windows/hashSum";
#endif

    if(s  == "nothing") {
        QString locale = QLocale::system().name();

        // if(translator.load(INSTALL_DIR + APPLICATION_NAME + "_" + locale)) {
        if(translator.load(translationPath + "_" + locale + ".qm")) {
            QApplication::installTranslator(&translator);
            k->setConf("language", locale.toStdString());
        } else {
            if(locale != "en_US")
                QMessageBox::information(nullptr, APPLICATION_NAME " " VERSION, APPLICATION_NAME " is unfortunately not yet translated into your language, the program will start with English menus.");

            k->setConf("language", "en_US");
        }
    } else {
        // if(translator.load(INSTALL_DIR + APPLICATION_NAME + "_" + sprak))
        if(translator.load(translationPath + "_" + sprak + ".qm")) {
            // if(translator.load(":/i18n/hashSum_sv_SE.qm")) {
            QApplication::installTranslator(&translator);
        }
    }

    const QString qtBasetranslationsPath(":/i18n/qttranslations");
    QTranslator qtBaseTranslator;
//    qDebug() << qtBasetranslationsPath + "/qtbase_" + sprak + ".qm";

    if(qtBaseTranslator.load(qtBasetranslationsPath + "/qtbase_" + sprak + ".qm")) {
        QApplication::installTranslator(&qtBaseTranslator);
//        qDebug() << "LOADED !";
    }

    delete k;
    Hash w;
    w.show();
    return a.exec();
}
