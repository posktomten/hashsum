/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "config.h"
#include "hash.h"
#include<fstream>
#include<string>
#include<vector>
#include<iostream>



const QString configure_path = QDir::toNativeSeparators(QDir::homePath() + "/." + APPLICATION_NAME);


const string my_configure_path = configure_path.toStdString();



using namespace std;



Config::Config(QString namn)
{
    //Lser in configurationsfilen i minnet
    //vid skapandet
    namn = namn.trimmed();
    //Hela skvgen
    QString configure = configure_path + QDir::toNativeSeparators(namn);
    string configure_s = configure.toStdString();
    conf_file = new string;
    *conf_file = configure_s;
    cr = new Createuser;
    checkConf(configure_s);
    readConf(configure_s);
    delete cr;
}



Config::~Config()
{
    //Skriver configurationsfilen innan dden
    writeConf();
}

// Lmnar ut "values" grundade p "name"
string Config::getConf(const string &name)
{
    vector<string>::iterator it;
    string s;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        if(it->substr(0, 1) != "#") { // hoppa ver kommaterade rader direkt
            // Sker efter samma delstrng som skickas hit med samma lngd
            // som hitskickade strngen. Samt att nsta tecken r "="
            // Fr att undvika fel typ: gustav gustavson
            if((it->substr(0, name.size()) == name) && (it->substr(name.size(), 1) == "=")) {
                // Returnerar frn tecknet efter "=" tecknet och til radens slut
                s = it->substr(name.size() + 1, it->size());
                return s;
            }
        }
    }

    s = "nothing";
    return s;
}

string Config::getConf(const string &name, const string &default_value)
{
    vector<string>::iterator it;
    string s;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        if(it->substr(0, 1) != "#") { // hoppa ver kommaterade rader direkt
            // Sker efter samma delstrng som skickas hit med samma lngd
            // som hitskickade strngen. Samt att nsta tecken r "="
            // Fr att undvika fel typ: gustav gustavson
            if((it->substr(0, name.size()) == name) && (it->substr(name.size(), 1) == "=")) {
                // Returnerar frn tecknet efter "=" tecknet och til radens slut
                s = it->substr(name.size() + 1, it->size());
                return s;
            }
        }
    }

    s = default_value;
    newConf(name, default_value);
    return s;
}

bool Config::setConf(string name, const string &value)
{
    vector<string>::iterator it;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        if(it->substr(0, 1) != "#") { // hoppa ver kommaterade rader direkt
            // Sker efter strngen "name" som skickas hit med samma lngd
            // som hitskickade strngen. Samt att nsta tecken r "="
            // Fr att undvika fel typ: gustav gustavson
            if((it->substr(0, name.size()) == name) && (it->substr(name.size(), 1) == "=")) {
                // Returnerar frn tecknet efter "=" tecknet och til radens slut
                // "name" har hittats, nu skall nya vrdet lggaas in
                name.append("=");
                name.append(value);
                *it = name;
                return true;
            }
        }
    }

    newConf(name, value);
    return true;
}



void Config::newConf(const string &name, const string &value)
{
    string pair = name + "=" + value;
    name_values->push_back(pair);
}



// Lser configurationsvektorn
bool Config::readConf(const string &conf)
{
    name_values = new vector<string>;
    string conf_values;
    ifstream infil;
    infil.open(conf.c_str());

    if(!(infil.good()))
        return false;

    while(getline(infil, conf_values))
        name_values->push_back(conf_values);

    infil.close();
    return true;
}



// skriver configurationsvektorn till fil
bool Config::writeConf()
{
    ofstream utfil;
    utfil.open(conf_file->c_str(), ios::out);

    if(!(utfil.is_open()))
        return false;

    vector<string>::iterator it;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        utfil << *it;
        utfil << "\n";
    }

    delete conf_file;
    return true;
}
// kollar om konfigurationsfilen finns
void Config::checkConf(const string &conf)
{
    ifstream infil2;
    infil2.open(conf.c_str());
    infil2.seekg(0, ios::end);

    if(infil2.tellg() < 1) {
        infil2.close();
        createConffile(conf);
    }
}
// skapar en default konfigurationsfil
bool Config::createConffile(const string &conf)
{
    cr->mkDir();
    ofstream utfil2;
    utfil2.open(conf.c_str(), ios::out);

    if(utfil2.good()) {
        utfil2.close();
        return true;
    }

    return false;
}


