/********************************************************************************
** Form generated from reading UI file 'license.ui'
**
** Created by: Qt User Interface Compiler version 5.15.14
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LICENSE_H
#define UI_LICENSE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Lic
{
public:
    QVBoxLayout *verticalLayout_4;
    QTextBrowser *textBrowser;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pbClose;
    QPushButton *pbOpenFile;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *Lic)
    {
        if (Lic->objectName().isEmpty())
            Lic->setObjectName(QString::fromUtf8("Lic"));
        Lic->setWindowModality(Qt::NonModal);
        Lic->resize(590, 426);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Lic->sizePolicy().hasHeightForWidth());
        Lic->setSizePolicy(sizePolicy);
        Lic->setMinimumSize(QSize(0, 0));
        Lic->setMaximumSize(QSize(16777215, 16777215));
        Lic->setContextMenuPolicy(Qt::DefaultContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/hashsum.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Lic->setWindowIcon(icon);
        Lic->setSizeGripEnabled(true);
        Lic->setModal(true);
        verticalLayout_4 = new QVBoxLayout(Lic);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        textBrowser = new QTextBrowser(Lic);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy1);
        textBrowser->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_4->addWidget(textBrowser);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        pbClose = new QPushButton(Lic);
        pbClose->setObjectName(QString::fromUtf8("pbClose"));
        pbClose->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pbClose->sizePolicy().hasHeightForWidth());
        pbClose->setSizePolicy(sizePolicy2);

        horizontalLayout_6->addWidget(pbClose);

        pbOpenFile = new QPushButton(Lic);
        pbOpenFile->setObjectName(QString::fromUtf8("pbOpenFile"));
        sizePolicy2.setHeightForWidth(pbOpenFile->sizePolicy().hasHeightForWidth());
        pbOpenFile->setSizePolicy(sizePolicy2);

        horizontalLayout_6->addWidget(pbOpenFile);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout_6);


        retranslateUi(Lic);

        QMetaObject::connectSlotsByName(Lic);
    } // setupUi

    void retranslateUi(QDialog *Lic)
    {
        Lic->setWindowTitle(QCoreApplication::translate("Lic", "Dialog", nullptr));
        pbClose->setText(QCoreApplication::translate("Lic", "Close", nullptr));
        pbOpenFile->setText(QCoreApplication::translate("Lic", "Save", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Lic: public Ui_Lic {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LICENSE_H
