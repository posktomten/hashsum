

#include "hash.h"
void Hash::shortCuts()
{
    QFile file(path);
    //    const QString source = QDir::toNativeSeparators(qgetenv("APPIMAGE"));
    const QString source = QCoreApplication::applicationDirPath() + "/streamcapture2";
    const QString pngpath = QCoreApplication::applicationDirPath() + "/streamcapture2.png";
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    const QString iconpath = fi.absolutePath() + "/streamcapture2.png";

    if(QFile::exists(iconpath)) {
        QFile::remove(iconpath);
    }

    if(QFile::copy(pngpath, iconpath)) {
        QFile file(iconpath);
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
    }

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);

        if(QFile::exists(iconpath)) {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=streamCapture2\nIcon=" + iconpath + "\nTerminal=false\nComment=" + tr("Download video streams.") + "\nExec=\"" + source + "\"\nCategories=AudioVideo;\n";
        } else {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=streamCapture2\nTerminal=false\nComment=" + tr("Download video streams.") + "\nExec=\"" + source + "\"\nCategories=AudioVideo;\n";
        }

        file.close();
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                            QFileDevice::WriteUser | QFileDevice::ReadOther |
                            QFileDevice::ExeOther);
    }
