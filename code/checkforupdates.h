/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

/*
Parameters (QString)
program Name                    [ progName ]
Current version (x.y.z)         [ currentVersion ]
URL of the latest version file  [ versionPath ]
Download                        [ downloadPath ]

connect(ui->actionCheckForUpdates, &QAction::triggered, [this] {
    myCheckForUpdates.check(PROG_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
});

Eller

    connect(ui->actionCheckForUpdates, &QAction::triggered, [] {
        CheckForUpdates *cu = new CheckForUpdates;
        cu->check(PROG_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
    });


*/

#ifndef CHECKFORUPDATES_H
#define CHECKFORUPDATES_H
#include <QMessageBox>
#include <QNetworkReply>
#include "texttospeech.h"
#include "texttospeech_global.h"

class CheckForUpdates : public QWidget
{

    Q_OBJECT

private:

    int jfrVersion(const QString &currentVersion, const QString &newVersion);
    QString moreInfo(const QString &infoVersion);
    void tesp(QString t);
    Libtexttospeech *mLibtexttospeech;

public:
    void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath, QVoice currentVoice, bool *verysilent);
    void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath, bool *verysilent);


    ~CheckForUpdates();
    CheckForUpdates();
};

#endif // CHECKFORUPDATES_H
