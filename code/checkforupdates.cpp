/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/



#include "checkforupdates.h"


void CheckForUpdates::check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath, QVoice currentVoice, bool *verysilent)
{
    mLibtexttospeech = new Libtexttospeech;
    mLibtexttospeech->setVoice(currentVoice);
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    QObject::connect(reply, &QNetworkReply::finished, [this, reply, versionPath, progName, currentVersion, downloadPath, verysilent]() {
        QByteArray bytes = reply->readAll();  // bytes
        QString newVersion(bytes); // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index);
        QString info = "";

        if(index > 0) {
            info = moreInfo(infoVersion);
        }

        if(onlyNewVersion == "") {
            if(!*verysilent) {
                tesp(tr("No Internet connection was found. Please check your Internet settings and firewall."));
            }

            QMessageBox::critical(this, progName + " " + currentVersion, tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
        } else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            switch(ver) {
                case 0:
                    if(!*verysilent) {
                        tesp(tr("Where is a new version of hashSum. Latest Version: ") + onlyNewVersion + tr(", Please download. ") + info);
                    }

                    QMessageBox::information(this, progName + "<p>" + currentVersion, tr("<br>There is a new version of ") + progName + tr(".<br>Latest version: ") + onlyNewVersion + "<br>" + info + "</p>");
                    break;

                case 1:
                    if(!*verysilent) {
                        tesp(tr("Your version of hashsum is newer than the latest official version."));
                    }

                    QMessageBox::information(this, progName + " " + currentVersion, tr("\nYour version of ") + progName + tr(" is newer than the latest official version ") + "(" + onlyNewVersion + ").");
                    break;

                case 2:
                    if(!*verysilent) {
                        tesp(tr("You have the latest version of hashsum."));
                    }

                    QMessageBox::information(this, progName + " " + currentVersion, tr("\nYou have the latest version of ") + progName + ".");
                    break;

                default:
                    if(!*verysilent) {
                        tesp(tr("There was an error when the version was checked."));
                    }

                    QMessageBox::information(this, progName + " " + currentVersion, tr("\nThere was an error when the version was checked."));
                    break;
            }
        }
    });
}

void CheckForUpdates::checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath, bool *verysilent)
{
    mLibtexttospeech = new Libtexttospeech;
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    QObject::connect(reply, &QNetworkReply::finished, [this, reply, versionPath, progName, currentVersion, downloadPath, verysilent]() {
        QByteArray bytes = reply->readAll();  // bytes
        QString newVersion(bytes); // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index);
        QString info = "";

        if(index > 0) {
            info = moreInfo(infoVersion);
        }

        if(onlyNewVersion == "")  {
            if(!*verysilent) {
                tesp(tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
            }

            QMessageBox::critical(this, progName + " " + currentVersion, tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
        } else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            if(ver == 0) {
                if(!*verysilent) {
                    tesp(tr("Where is a new version of hashSum. Latest Version: ") + onlyNewVersion + ". " + info);
                }

                QMessageBox::information(this, progName + " " + currentVersion, "<p><br>" + tr("There is a new version of ") + progName + ".<br>" + tr("Latest Version ") + onlyNewVersion + "<br>" + info + "</p>");
            } else if(ver == 3) {
                if(!*verysilent) {
                    tesp(tr("There was an error when the version was checked."));
                }

                QMessageBox::information(this, progName + " " + currentVersion, tr("\nThere was an error when the version was checked."));
            }
        }
    });
}

int CheckForUpdates::jfrVersion(const QString &currentVersion, const QString &newVersion)
{
    int newVer;
    int currentVer;
    bool ok = true;
    QStringList listCurrentVersion = currentVersion.split(".");
    QStringList listNewVersion = newVersion.split(".");

    for(int i = 0; i <= 2; i++) {
        currentVer = listCurrentVersion.at(i).toInt(&ok, 10);
        newVer = listNewVersion.at(i).toInt(&ok, 10);

        if(!ok)
            return 3;

        if(newVer > currentVer)
            return 0;

        if(newVer < currentVer)
            return 1;
    }

    return 2;
}
QString CheckForUpdates::moreInfo(const QString &infoVersion)
{
    QList<QString> listInfo;
    listInfo = infoVersion.split('\n');
    QString news;

    if(!listInfo.empty()) {
        news = "<br><i>" + tr("New updates:") + "</i>";
        QList<QString>::iterator it;

        for(it = listInfo.begin() + 1; it != listInfo.end(); ++it)
            news.append("<br>" + *it);
    } else {
        news = "";
    }

    return news;
}

void CheckForUpdates::tesp(QString t)
{
    QString *_t = new QString(t);
    mLibtexttospeech->setText(_t);
}
CheckForUpdates::CheckForUpdates()
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
}
CheckForUpdates::~CheckForUpdates()
{
    delete this;
}
