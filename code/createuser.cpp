/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "createuser.h"
#include "hash.h"



const QString CONF_DIR = QDir::toNativeSeparators(QDir::homePath() + "/." APPLICATION_NAME);
const QString CONF_FILE = CONF_DIR + QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");



bool Createuser::mkDir()
{
    QDir dir;
    return dir.mkdir(CONF_DIR);
}




bool Createuser::removeDir()
{
    QDir dir;

    if(dir.remove(CONF_FILE) && dir.remove(CONF_FILE)) {
        if(dir.rmdir(CONF_DIR))
            return true;
    }

    if(dir.remove(CONF_FILE)) {
        if(dir.rmdir(CONF_DIR))
            return true;
    }

    return dir.rmdir(CONF_DIR);
}
