######################################################################
#
#    hashSum
#    Copyright (C) 2011-2023 Ingemar Ceicer
#    https://gitlab.com/posktomten/hashsum/-/wikis/Home
#    programming@ceicer.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
######################################################################


QT += core gui network texttospeech

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets



CONFIG += c++14
# CONFIG += static

# MSVC
# TEMPLATE = vcapp
# GCC
  TEMPLATE = app

TARGET = hashSum
#DEPENDPATH += .
#INCLUDEPATH += .

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
# DEFINES += QT_DEPRECATED_WARNINGS
  DEFINES += QT_NO_DEPRECATED_WARNINGS




# Input
HEADERS += checkforupdates.h \
           config.h \
           createuser.h \
           hash.h \
           extqlineedit.h

FORMS += hash.ui \
         license.ui

SOURCES += checkforupdates.cpp \
           config.cpp \
           createuser.cpp \
           hash.cpp main.cpp \
           extqlineedit.cpp \
           duplicates.cpp \
           isthereaduplicate.cpp \
           system.cpp \
           texttospeech.cpp

RC_FILE = myapp.rc
#CODECFORSRC = UTF-8


RESOURCES += myres.qrc



TRANSLATIONS += i18n/_hashSum_template_xx_XX.ts \
                i18n/hashSum_sv_SE.ts \
                i18n/hashSum_el_GR.ts \
                i18n/hashSum_de_DE.ts




DESTDIR=../build-executable5
#unix:UI_DIR = ../code
win32:UI_DIR = ../code
win32:RC_ICONS += images/hashsum.ico

unix:INCLUDEPATH += "../include_linux"
unix:DEPENDPATH += "../include_linux"
win32:INCLUDEPATH += "../include_windows"
win32:DEPENDPATH += "../include_windows"

contains(QT_ARCH, x86_64) {

#unix: LIBS += -L../lib5/ -lcrypto
#unix: LIBS += -L../lib5/ -lssl

win32: CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownload_installd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -ltexttospeech  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ltexttospeechd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug

}
contains(QT_ARCH, i386) {

#unix: LIBS += -L../lib5_32/ -lcrypto
#unix: LIBS += -L../lib5_32/ -lssl

win32: CONFIG (release, debug|release): LIBS += -L../lib5_32/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -ldownload_installd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lupdateappimaged # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcreateshortcut # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcreateshortcutd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcheckupdated # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_32/ -ltexttospeech  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -ltexttospeechd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_32/ -labout  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -laboutd # Debug

}
message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)


