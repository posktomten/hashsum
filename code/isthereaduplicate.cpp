/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "hash.h"
#include "ui_hash.h"
#include "ui_license.h"



void Hash::isThereADuplicate()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string open_path = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    std::string algorithm = k->getConf("algorithm");
    QString qsalgorithm = QString::fromStdString(algorithm);
    QString qs = QString::fromStdString(open_path);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs;
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Open the file you want to find duplicates for"),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFile);
    QStringList fileNames;
    QString fileName;
    QString directoryName;

    if(dialog->exec()) {
        ui->actionCopyPath->setDisabled(true);
        ui->actionSaveToFile->setDisabled(true);
        ui->actionCopyHashSum->setDisabled(true);
        ui->progressLabel->clear();
        //ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        fileName = dialog->selectedFiles().at(0);
        QDir dir;
        dir = dialog->directory();
        directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        fileNames = dialog->selectedFiles();
    } else {
        delete k;
        return;
    }

    delete k;
    bool lyckades = doChecksum(fileName);

    if(!lyckades) {
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
        tesp(tr("No hash sum could be calculated"));
        return;
    }

    QString hashsumma = ui->lineEdit->text();
    /*   OPEN RECURSIVE */
    hurmangaFoldrar = 0;

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs + QDir::toNativeSeparators("/");
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog2 = new QFileDialog(this, tr("Select a folder to Find identical files"),
                                           sokvag,
                                           tr("All Files (*.*)"));

    if(dialogruta == "0")
        dialog2->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog2->setOption(QFileDialog::DontUseNativeDialog, false);

    // dialog2->setFileMode(QFileDialog::DirectoryOnly);
    dialog2->setOption(QFileDialog::ShowDirsOnly, true);
    dialog2->setOption(QFileDialog::DontResolveSymlinks);
    dialog2->setReadOnly(true);
    QString qsokvag = "";

    if(dialog2->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir = dialog2->directory();
        qsokvag = dir.path();
    } else {
        return;
    }

    /*   SLUT RECURSIVE */
    // QString qopen_path = QString::fromStdString(open_path);
    QStringList likadana;
    bool lyckats = rekursivt3(qsokvag, qsalgorithm, qsokvag, hashsumma, fileNames.at(0), likadana);

    if(!lyckats) {
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not check the hash sum. Check your file permissions."));
        tesp(tr("Could not check the hash sum. Check your file permissions."));
    }

    printDuplicates(likadana, fileNames.at(0), qsokvag);
}

bool Hash::rekursivt3(const QDir &qsokvag, const QString &alg, const QString &qopen_path, const QString &hashsumma, const QString &jfrFile, QStringList &likadana)
{
    QString nyHashsumma;
    QFileInfoList list = qsokvag.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

    foreach(QFileInfo finfo, list) {
        if(finfo.isDir()) {
            rekursivt3(QDir(finfo.absoluteFilePath()), alg, qopen_path, hashsumma, jfrFile, likadana);
            hurmangaFoldrar++;
        } else {
            nyHashsumma = checksum2(finfo.absoluteFilePath(), alg);

            if((nyHashsumma == hashsumma) && (finfo.absoluteFilePath() != jfrFile)) {
                likadana.append(finfo.absoluteFilePath());
            }

            hurmangaFiler++;
        }
    }

    QString tmp = "";
    QApplication::processEvents();

    if(hurmangaFoldrar < 2) {
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
    } else {
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folders for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
    }

    ui->statusBar->showMessage(tr("Path: ") + QDir::toNativeSeparators(qsokvag.path()));

    if(QDir::toNativeSeparators(qsokvag.path()) == QDir::toNativeSeparators(qopen_path)) {
        if(hurmangaFoldrar < 2) {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
//            tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
        } else {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
//            tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folders for files. I have managed to calculate the hash sum in all folders containing files. Algorithm: " + alg);
        }

        ui->statusBar->showMessage(tr("I have checked all folders and checked all hash sums! Mission accomplished."));
        tesp(tr("I have checked all folders and checked all hash sums! Mission accomplished."));
        QString s;
        s.setNum(hurmangaFiler);
        ui->lineEdit->setText(tr("Mission accomplished! I have calculated the hash sum of ") + s + tr(" files."));
        tesp(tr("I have calculated the hash sum of ") + s + tr(" files."));
        hurmangaFiler = 0;
    }

    return true;
}

bool Hash::printDuplicates(const QStringList &likadana, const QString &jfrFile, const QString &directoryName)
{
    /**** ****/
    d = new QDialog;
    ui2 = new Ui::Lic;
    //Font ui2
//    int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
//    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
//    QFont f(family);
    // SLUT Font ui2
    ui2->setupUi(d);
//    ui2->textBrowser->setFont(f);
    ui2->pbOpenFile->setText(tr("Save"));
    /* */
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c); // set color "Red" for textedit base
    ui2->textBrowser->setPalette(p); // change textedit palette
    /* */
    /***** *******/
//    tesp("Identical files with " + jfrFile + " Recursively searched from " + directoryName + " I found " + QString::number(likadana.count()) + " duplicates.");
    ui2->textBrowser->append(tr("Created: ") + datumtid());
//    tesp("Created: " + datumtid());
    ui2->textBrowser->append(tr("Identical files with ") + "\"" + jfrFile + "\"");
//    tesp("Identical files with " + jfrFile);
    ui2->textBrowser->append(tr("Recursively searched from ") + "\"" + directoryName + "\"");
//    tesp("Recursively searched from " + directoryName);

    foreach(QString s, likadana) {
        ui2->textBrowser->append(s);
//        tesp(s);
    }

    ui2->textBrowser->append(tr("I found ") + QString::number(likadana.count()) + tr(" duplicates."));
    tesp(tr("I found ") + QString::number(likadana.count()) + tr(" duplicates."));
    /*****      *****/
    d->setWindowModality(Qt::ApplicationModal);
    d->setWindowFlags(d->windowFlags() | Qt::WindowMinMaxButtonsHint);
    d->setWindowTitle(APPLICATION_NAME " " VERSION "  " + tr("Duplicate files"));
    QIcon ikon("/images/hashsum.ico");
    d->setWindowIcon(ikon);
    connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
    connect(ui2->pbOpenFile, SIGNAL(pressed()), this, SLOT(save()));
    QTextCursor cursor = ui2->textBrowser->textCursor();
    cursor.setPosition(0);
    ui2->textBrowser->setTextCursor(cursor);
    d->show();
    /*****      *****/
    return true;
}
