cmake_minimum_required(VERSION 3.16)
project(hashSum VERSION 1.0 LANGUAGES C CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Set up AUTOMOC and some sensible defaults for runtime execution
# When using Qt 6.3, you can replace the code block below with
# qt_standard_project_setup()
set(CMAKE_AUTOMOC ON)
include(GNUInstallDirs)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(QT NAMES Qt5 Qt6 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Gui Network TextToSpeech)
find_package(Qt${QT_VERSION_MAJOR} OPTIONAL_COMPONENTS Widgets)

# Resources:
set(MY_RESOURCE
     myres.qrc
)

add_executable(hashSum WIN32 MACOSX_BUNDLE
    checkforupdates.cpp checkforupdates.h
    config.cpp config.h
    createuser.cpp createuser.h
    duplicates.cpp
    extqlineedit.cpp extqlineedit.h
    hash.cpp hash.h hash.ui
    isthereaduplicate.cpp
    license.ui
    main.cpp
    system.cpp
    texttospeech.cpp
    ${MY_RESOURCE}
)
if((CMAKE_SIZEOF_VOID_P GREATER 4) OR (_bigendian)) # 64-bit
      message("64-bit")
if( (WIN32 AND NOT MSVC) OR UNIX)
      if(CMAKE_BUILD_TYPE STREQUAL "Debug")
      message("Qt6 WIN32/Unix Debug")
          set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libaboutd.a)
          set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libcheckupdated.a)
          set(libtexttospeech ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libtexttospeechd.a)
          set(libcreateshortcut ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libcreateshortcutd.a)
          if(UNIX)
              set(libupdateappimage ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libupdateappimaged.a)
          endif()
          if(WIN32 AND NOT MSVC)
               set(libdownload_install ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libdownload_installd.a)
          endif()
      endif()
      if(CMAKE_BUILD_TYPE STREQUAL "Release")
          message("Qt6 WIN32/Unix Release")
              set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libabout.a)
              set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libcheckupdate.a)
              set(libtexttospeech ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libtexttospeech.a)
              set(libcreateshortcut ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libcreateshortcut.a)
              if(UNIX)
                  set(libupdateappimage ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libupdateappimage.a)
              endif()
              if(WIN32 AND NOT MSVC)
                  set(libdownload_install ${CMAKE_CURRENT_SOURCE_DIR}/../lib5/libdownload_install.a)
              endif()
          endif()
         endif()
else() # 32-bit
    message("32-bit")
    if( (WIN32 AND NOT MSVC) OR UNIX)
          if(CMAKE_BUILD_TYPE STREQUAL "Debug")
          message("Qt6 WIN32/Unix Debug")
              set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libaboutd.a)
              set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libcheckupdated.a)
              set(libtexttospeech ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libtexttospeechd.a)
              set(libcreateshortcut ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libcreateshortcutd.a)
              if(UNIX)
                  set(libupdateappimage ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libupdateappimaged.a)
              endif()
              if(WIN32 AND NOT MSVC)
                   set(libdownload_install ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libdownload_installd.a)
              endif()
          endif()
          if(CMAKE_BUILD_TYPE STREQUAL "Release")
           message("Qt6 WIN32/Unix Release")
                  set(libabout ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libabout.a)
                  set(libcheckupdate ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libcheckupdate.a)
                  set(libtexttospeech ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libtexttospeech.a)
                  set(libcreateshortcut ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libcreateshortcut.a)
                  if(UNIX)
                      set(libupdateappimage ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libupdateappimage.a)
                  endif()
                  if(WIN32 AND NOT MSVC)
                      set(libdownload_install ${CMAKE_CURRENT_SOURCE_DIR}/../lib5_32/libdownload_install.a)
                  endif()
              endif()
             endif()

endif()




target_compile_definitions(hashSum PRIVATE
    QT_NO_DEPRECATED_WARNINGS
)
if(UNIX)
target_link_libraries(hashSum PRIVATE
    Qt::Core
    Qt::Gui
    Qt::Network
    Qt::TextToSpeech
    ${libabout}
    ${libcheckupdate}
    ${libtexttospeech}
    ${libcreateshortcut}
    ${libupdateappimage}
)
endif(UNIX)
if(WIN32 AND NOT MSVC)
target_link_libraries(hashSum PRIVATE
    Qt::Core
    Qt::Gui
    Qt::Network
    Qt::TextToSpeech
    ${libabout}
    ${libcheckupdate}
    ${libtexttospeech}
    ${libcreateshortcut}
    ${libdownload_install}
)
endif(WIN32 AND NOT MSVC)


# add_resources(hashSum
#     PREFIX
#         "/"
#     FILES
#         ${myres_resource_files}
# )

if((QT_VERSION_MAJOR GREATER 4))
    target_link_libraries(hashSum PRIVATE
        Qt::Widgets
    )
endif()

if(WIN32)
    target_include_directories(hashSum PRIVATE
        ../include_windows
    )
endif()

if(UNIX)
    target_include_directories(hashSum PRIVATE
        ../include_linux
    )
endif()


# install(TARGETS hashSum
#     BUNDLE DESTINATION .
#     RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
# )

# Consider using qt_generate_deploy_app_script() for app deployment if
# the project can use Qt 6.3. In that case rerun qmake2cmake with
# --min-qt-version=6.3.
