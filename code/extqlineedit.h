/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef EXTQLINEEDIT_H
#define EXTQLINEEDIT_H
#include "texttospeech.h"
#include <QtWidgets>

class ExtQLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    ExtQLineEdit(QWidget *parent = 0);
    ~ExtQLineEdit();

protected:
    void mouseDoubleClickEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);

private:



};

# endif // EXTQLINEEDIT_H
