/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef HASH_H
#define HASH_H

#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include "checkforupdates.h"
#include "config.h"
#include "texttospeech.h"
#include "texttospeech_global.h"
#include "config.h"
#include "extqlineedit.h"
#include "about.h"
#include <QSysInfo>
#include <QtWidgets>
#include <QTextToSpeech>

// EDIT
#define OFFLINE_INSTALLER

#ifdef Q_OS_LINUX
#include "updatedialog.h"
#include "update.h"
#endif


#ifdef Q_OS_WINDOWS
#ifdef OFFLINE_INSTALLER
#include "download_install.h"
#include "download_install_global.h"
#define PATH "https://bin.ceicer.com/hashSum/bin/windows/current/bin/"

#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define FILENAME "hashSum_64-bit_setup.exe"
#endif

#ifdef Q_PROCESSOR_X86_32 // 32-bit
#define FILENAME "hashSum_32-bit_setup.exe"
#endif

#endif   // OFFLINE_INSTALLER
#endif   // Q_OS_WINDOWS

#ifdef Q_OS_LINUX

#if defined(Q_PROCESSOR_X86_64) // 64 bit

#if (__GLIBC_MINOR__ == 27)
#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#define ARG1 "hashSum-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/hashSum/GLIBC2.27/hashSum-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 27

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#define ARG1 "hashSum-x86_64.AppImagee"
#define ARG2 "http://bin.ceicer.com/zsync/hashSum/GLIBC2.31/hashSum-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.4 LTS 64-bit, GLIBC 2.35"
#define ARG1 "hashSum-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/hashSum/GLIBC2.35/hashSum-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 35

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Lubuntu 24.04 LTS 64-bit, GLIBC 2.39"
#define ARG1 "hashSum-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/hashSum/GLIBC2.39/hashSum-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 35

#elif defined(Q_PROCESSOR_X86_32) // 32 bit

#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#define ARG1 "hashSum-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/hashSum/GLIBC2.27/hashSum-i386.AppImage.zsync"

#endif // Q_PROCESSOR_
#endif

#include "createshortcut.h"
#include "createshortcut_global.h"
/* draganddrop */
#include <QDropEvent>
#include <QUrl>
#include <QDebug>

#define VERSION_PATH "https://bin.ceicer.com/hash/version2.txt"

#define APPLICATION_NAME "hashSum"

#define DISPLAY_NAME "hashSum"
#define EXECUTABLE_NAME "hashsum"
#define VERSION "2.7.1"
#define DISPLAY_VERSION VERSION
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define SOURCECODE "https://gitlab.com/posktomten/hashsum"
#define WEBSITE "https://gitlab.com/posktomten/hashsum/-/wikis/home"
#define CHANGELOG "https://gitlab.com/posktomten/hashsum/-/blob/texttospeech/CHANGELOG"
#define LICENSE "GNU General Public License v3.0"
#define LICENSE_LINK "https://gitlab.com/posktomten/hashsum/-/blob/texttospeech/LICENSE"
#define COPYRIGHT "Ingemar Ceicer"
#define EMAIL "programming@ceicer.com"
#define COPYRIGHT_YEAR "2011"

#define APPLICATION_HOMEPAGE "https://gitlab.com/posktomten/hashsum/-/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG "https://gitlab.com/posktomten/hashsum/-/wikis/Home"
#define DOWNLOAD_PATH "https://gitlab.com/posktomten/hashsum/-/wikis/DOWNLOADS"
#define elGR_TRANSLATOR "https://www.geogeo.gr"
#define deDE_TRANSLATOR "Benjamin Weis"
#define QTINSTALLERFRAMEWORK false

#if defined(Q_OS_WINDOWS) // WINDOWS

#define FONT 11

#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 64-bit operating system, x64-based processor"

#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 32-bit operating system, x86-based processor"

#endif // Q_PROCESSOR

#endif // Q_OS

#define QLINEEDIT_MAX 32767
#define SIZE_OF_RECENT_FILES 50
#define RED 218
#define GREEN 255
#define BLUE 221

#define LINUX_FONT 13
#define WINDOWS_FONT 11

namespace Ui
{
class Hash;
}

namespace Ui
{
class Lic;
}

class Hash : public QMainWindow
{
    Q_OBJECT

public:
    explicit Hash(QWidget *parent = nullptr);
    ~Hash();

protected:
    /* draganddrop */
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);

private:
    Ui::Hash *ui{};
    Ui::Lic *ui2{};
    Config  *k{};
    QDialog *d{};
#ifdef Q_OS_LINUX // Linux
    UpdateDialog *ud;
#endif

    void setStartConfig();
    void setEndConfig();
    bool doChecksum(const QString &);
    bool doChecksum(const QString &, QStringList *lista);
    bool doChecksumText(const QString &);
    bool doChecksumList(const QString &, QString &, const string &);
    bool doCompare(const QString &, const QString &);
    bool askForFileName(const QString &, const string &, QStringList);
    bool askForFileNameCheck(const QString &, const string &, QStringList);
    bool dontAskForFileName(const string &, QStringList);
    QString datumtid();
    bool rekursivt(const QDir &, QString);

    bool rekursivtPrint(const QString &, const QString &, QDir, const string &);
    int hurmangaFoldrar{};
    void checkSumList(int);
    int hurmangaFiler{};
    bool createDeb(const QString &sokvag2, QStringList fileNames);
    QString getSystem();
    bool rekursivtDeb(const QDir &path, QString &indent, QStringList &allt);
    bool rekursivtPrintDeb(QDir path, QStringList &allt);

    bool recentFileList(QString);
    QStringList recentFiles;
    int MAX_RECENT_FILES{};

    QStringList algoritmer;
    QStringList *lista{};

    /* DUPLICATES */
    void findDuplicates();
    QString checksum2(const QString &in, const QString &s);
    struct PathHash {
        QString path;
        QString hash;

    };

    void collectDuplicates(QVector<PathHash> &v_ph, const QString &directoryNamne);
    bool rekursivt2(const QDir &, QVector<PathHash> &v_ph, const QString &alg, const QString &qopen_path);
    static bool compareAlphabet(const Hash::PathHash & a, const Hash::PathHash & b);
    /* IS THERE A DUPLICATE */
    void isThereADuplicate();
    bool rekursivt3(const QDir &qsokvag, const QString &alg, const QString &qopen_path, const QString &hashsumma, const QString &jfrFile, QStringList &likadana);
    bool printDuplicates(const QStringList &likadana, const QString &jfrFile, const QString &directoryName);
    void tesp(QString t);
    Libtexttospeech *mLibtexttospeech;
    QVoice currentVoice;

private slots:
    void about();
    void checkOnStart();
    void saveChecksumListWhereFilesAre();
    void md4();
    void md5();
    void sha1();

    void sha224();
    void sha256();
    void sha384();
    void sha512();
    void sha3_224();
    void sha3_256();
    void sha3_384();
    void sha3_512();

    void keccak_224();
    void keccak_256();
    void keccak_384();
    void keccak_512();

    void compare();
    void checksum();

    void swedish();
    void english();
    void german();
    void greek();
    void checkSumListInit();
    void checkSumLists();
    void compareGivenHashSum();
    void defaultFileName();
    void writeTimeDateInHashSumFile();
    void findChangedFiles();
    void useNativeDialogs();
    void allwaysOpenHomeDirectory();
    void copyPath();
    void copyHashSum();
    void hashSumText();
    void compareHashSumText();
    void help();
    void license();
    void versionhistory();
    void createDebianMd5sums();
    void createDebianMd5sumsAuto();
    void showFullPathInHashSumFile();

    void slotRecentFiles();
    void recentFileRequested(const QString &path);
    void setRecentFiles();
    void clearRecentFiles();
    void stang();

    void openHashSumList();
    void openComparisonList();
    void openFile();

    void save();
    void saveOne();

public slots:

};

#endif // HASH_H
