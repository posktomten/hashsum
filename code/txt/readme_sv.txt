22:00 28 januari 2023
Ändringar från version 2.7.0 till 2.7.1
"Om Qt" dialog.
Uppdaterad till Qt5.15.8

13:00 29 juni 2022
Ändringar från version 2.6.1 till 2.7.0
Text till tal funktionalitet.

14:00 17 juni 2022
Ändringar från version 2.6.0 till 2.6.1
Mindre buggfixar.
Förbättrade översättningar.

01:30 17 juni 2022
Ändringar från version 2.5.1 till 2.6.0
Uppdaterar sig själv.
Skapar genvägar i programmenyn och på skrivbordet.

11:19 22 maj 2020
Ändringar från version 2.5.0 till 2.5.1
Möjlighet att alltid öppna visningsfönstret.

0:29 29 februari 2020
Ändringar från version 2.4.1 till 2.5.0
Möjlighet att söka efter dubletter till en
specifik fil.

00:18 14 februari 2020
Ändringar från version 2.4.0 till 2.4.1
Mindre buggrättningar och prestandaförbättringar.

21:37 12 februari 2020
Ändringar från version 2.3.2 till 2.4.0
Möjlighet att söka rekursivt efter fildubletter.

00:46 6 augusti 2018
Ändringar från version 2.3.1 till 2.3.2
Monospace-teckensnitt.

14:41 26 juni 2018
Ändringar från version 2.3.0 till 2.3.1
Inbäddade teckensnitt.

11:16 28 januari 2018
Ändringar från version 2.2.6 till 2.3.0
Lagt till algoritmer: Keccak_224, Keccak_256
Keccak_384 och Keccak_512.

18:48 15 oktober 2017
Ändringar från version 2.2.5 till 2.2.6
Dynamiskt användargränssnitt.

23:13 10 augusti 2017
Ändringar från version 2.2.4 till 2.2.5
Rättat en bugg.

14:24 10 augusti 2017
Ändringar från version 2.2.3 till 2.2.4
Möjlighet att spar hashsummor i textfiler.

23:30 22 juli 2017
Ändringar från version 2.2.1 till 2.2.3
Drag and drop funktionalitet.
Möjlighet att öppna flera filer samtidigt
Visar versionshistoriken.

20:23 15 januari 2017
Ändringar från version 2.2.0 till 2.2.1
Tysk översättning.

23:21 18 maj 2016
Ändringar från version 2.1.2 till 2.2.0
Möjligt att öppna sparade hashsummelistor direkt från programmet.
Möjligt att öppna sparade listor på förändrade filer direkt i programmet.
Visar sökväg och antalet filer vid hashsummelista lokalt.

18:20 22 februari 2016
Ändringar från version 2.1.1 till 2.1.2
Möjligt att öppna sparade hashsummelistor direkt från programmet.
Visar sökväg och antalet filer vid hashsummelista lokalt.

14:20 17 februari 2016
Ändringar från version 2.1.0 till 2.1.1
Programmet är lite bredare för att få bättre plats med långa hashsummor.
Support för Windows 10 i "Om...".
Visar licensen.
Visar versionshistoriken.
Finns för 32-bitars och för 64-bitars Windows.

13:25 2 maj 2015
Ändringar från version 2.0.4 till 2.1.0
Tillagda algoritmer: SHA224, SHA256, SHA384,
SHA512, SHA3_224, SHA3_256, SHA3_384,
SHA3_512.
Lista på nyligen öppnade filer.

15:52 11 oktober 2014
Ändringar från version 2.0.3 till 2.0.4
Grekisk översättning

16:14 1 januari 2013
Ändringar från version 2.0.2a till 2.0.3
Optimerad kod.

20:41 7 september 2012
Ändringar från version 2.0.2 till 2.0.2a
Kompletterat med saknade biblioteksfiler.
Gäller endast Windowsversionen.

01:00 25 juli 2012
Ändringar från version 2.0.1 till 2.0.2
Om inte användarens språk stöds, ges information
om att programmets menyer blir engelskspråkiga.
Om man byter skärmupplösning och programmet vill hamna
utanför, justeras detta så att programmet hamnar
på skärmen.Tydligare menyer.

22:49 16 maj 2012
Ändringar från version 2.0.0 till 2.0.1
Bugg i "Hitta förändringar" fixad.

22:16 12 maj 2012
Ändringar från version 1.0.2 till 2.0.0
Man kan bekvämt skapa Debian md5sums listor.
Möjlighet att visa hela sökvägen i listorna.
Möjlighet att välja filer från olika mappar till samma hashsummelista.
"Whats's this" help borttagen (fungerade inte med Ubuntu).
Direkt länk till manualsidor.
Bättre formaterade pdf-listor.

00:15 22 april 2012
Ändringar från version 1.0.1 till 1.0.2
"Whats's this" hjälp (Shift + F1).

06:29 9 april 2012
Ändringar från version 1.0.0 till 1.0.1
Ingen hashsumma på "standard filnamn" eller "standard filnamn.pdf".
när programmet skapar rekursiva hashsummelistor.
Rättad svensk översättning.

20:47 24 mars 2012
Ändringar från version 0.0.14 till 1.0.0
Möjlighet att beräkna hashsummor för text.
Möjlighet att spara som textfil och pdf-fil samtidigt.
Bättre funktionalitet i klippbordshanteringen.

15:53 17 mars 2012
Ändringar från version 0.0.13 till 0.0.14
Kopiera hashsumman till klippbordet.

19:32 13 mars 2012
Ändringar från version 0.0.12 till 0.0.13
Kopiera sökvägen till klippbordet.
Mindre förbättringar.
Exakt samma källkod för alla operativsystem.

01:43 26 februari 2012
Ändringar från version 0.0.11 till 0.0.12
Automatisk omstart av programmet vid språkbyte.

23:33 1 februari 2012
Ändringar från version 0.0.10 till 0.0.11
Du kan spara listor på hashsummor i pdf-format.

17:31 28 januari 2012
Ändringar från version 0.0.9 till 0.0.10
Du kan jämföra mot en vald hashsumma.

13:45 18 juli 2011
Ändringar från version 0.0.8 till 0.0.9
Du kan välja att programmet alltid ska kolla efter uppdateringar vid start.
Du kan välja filer från olika mappar när du jämför filer.
Du kan välja att alltid starta i din hemmamapp. (Standard är 
senast öppnade mapp).

23:36 18 juni 2011
Ändringar från version 0.0.7 till 0.0.8
Du kan kolla vilka filer som har ändrats, vilka som
är nya och vilka som försvunnit sedan förra
gången du kollade.

21:25 17 juni 2011
Ändringar från version 0.0.5 till 0.0.7
Du kan rekursivt skapa listor med hashsummor.
Du kan välja om datum och tid ska visas i hashsummefilen.

00:57 12 juni 2011
Ändringar från version 0.0.4 till 0.0.5
Buggrättning. Nu går det att öppna filer utan filändelse.

00:57 12 juni 2011
Ändringar från version 0.0.3 till 0.0.4
Listorna med checksummor kan sparas automatiskt.
Du kan välja standard filnamn på filen med checksummor.

21:46 10 juni 2011
Ändringar från version 0.0.2 till 0.0.3
Möjlighet att skapa listor med checksummor.
Svenskspråkigt.

17:54 5 juni 2011
Ändringar från version 0.0.1 till 0.0.2
Tydligare meddelanden från programmet.

15:21 5 juni 2011
Release version 0.0.1
