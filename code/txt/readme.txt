22:00 January 28, 2023
Changes from version 2.7.0 to 2.7.1
"About Qt" dialog.
Updated to Qt5.15.8

13:00 June 29, 2022
Changes from version 2.6.1 to 2.7.0
Text to speech functionality.

14:00 June 17, 2022
Changes from version 2.6.0 to 2.6.1
Minor bug fixes.
Improved translations.

01:30 June 17, 2022
Changes from version 2.5.1 to 2.6.0
Updates itself.
Creates shortcuts in the application menu and on the desktop.

11:19 May 22, 2020
Changes from version 2.5.0 to 2.5.1
Ability to always open the display window.

10:29 February 29, 2020
Changes from version 2.4.1 to 2.5.0
Ability to search for duplicates for a
specific file.

00:18 February 14, 2020
Changes from version 2.4.0 to 2.4.1
Minor bug fixes and performance
improvements.

21:37 February 12, 2020
Changes from version 2.3.2 to 2.4.0
Ability to search recursively for file
duplicates.

00:46 August 6, 2018
Changes from version 2.3.1 to 2.3.2
Monospace fonts.

11:16 June 26, 2019
Changes from version 2.3.0 to 2.3.1
Embeded fonts.

11:16 January 28, 2018
Changes from version 2.2.6 to 2.3.0
Added algorithms: Keccak_224, Keccak_256
Keccak_384 and Keccak_512.

18:48 October 15, 2017
Changes from version 2.2.5 to 2.2.6
Dynamic user interface.

23:13 August 10, 2017
Changes from version 2.2.4 to 2.2.5
Fixed a bug.

14:24 August 10, 2017
Changes from version 2.2.3 to 2.2.4
Ability to save hash sums in text files.

23:30 July 22, 2017
Changes from version 2.2.1 to 2.2.3
Drag and drop functionality.
Ability to open multiple files at once.
Displays version history.

20:23 January 15, 2017
Changes from version 2.2.0 to 2.2.1
German translation.

23:21 May 18, 2016
Changes from version 2.1.1 to 2.2.0
Possible to open saved hash sum lists directly from the program.
Possible to open saved lists of changed files directly in the program.
Displays the path and the number of files when using local hash sum list.

18:20 February 22, 2016
Changes from version 2.1.1 to 2.1.2
Possible to open saved hash sum lists directly from the program.
Displays the path and the number of files when using local hash sum list.

14:20 February 17, 2016
Changes from version 2.1.0 to 2.1.1
The program is a little wider to accommodate long hash sums.
Support for Windows 10 in "About...".
Displays license.
Displays version history.
Available in versions for 32-bit and 64-bit Windows.

13:25 May 2, 2015
Changes from version 2.0.4 to 2.1.0
Added algorithms: SHA224, SHA256, SHA384,
SHA512, SHA3_224, SHA3_256, SHA3_384,
SHA3_512.
Recent Files List.

15:52 October 11, 2014
Changes from version 2.0.3 to 2.0.4
Greek translation.

16:14 January 1, 2013
Changes from version 2.0.2a to 2.0.3
Optimized code.

20:41 September 7, 2012
Changes from version 2.0.2 to 2.0.2a
Supplemented with missing library files.
Applies only to windows version.

01:00 July 25, 2012
Changes from version 2.0.1 to 2.0.2
Unless the user's language is supported, given information
that the program's menus are in English.
If you change screen resolution and the program would end up
outside, this is adjusted so that the program ends up
on the screen. 
Clearer menus.

22:49 May 16, 2012
Changes from version 2.0.0 to 2.0.1
Bug in "Find changes" fixed.

22:16 May 12, 2012
Changes from version 1.0.2 to 2.0.0
One can conveniently create Debian md5sums listings. (Only the Linux version).
The ability to display full path in the lists.
Ability to select files from different folders to the same hash sum list.
"Whats's this" help removed (did not work with Ubuntu).
Direct link to the manual pages.
Better formatted PDF lists.

00:15 April 22, 2012
Changes from version 1.0.1 to 1.0.2
"Whats's this" help (Shift + F1).

06:29 April 9, 2012
Changes from version 1.0.0 to 1.0.1
No hash sum of the "default file name" or "default file name.pdf".
when the program creates recursive hash sum lists.
Corrected Swedish translation.

20:47 Mars 24, 2012
Changes from version 0.0.14 to 1.0.0
Ability to calculate hash sums for text.
Ability to save as a text file and pdf file at once.
Improved functionality of the clipboard handling.

15:53 Mars 17, 2012
Changes from version 0.0.13 to 0.0.14
Copy Hash Sum to clipboard.

19:32 Mars 13, 2012
Changes from version 0.0.12 to 0.0.13
Copy path to clipboard.
Minor improvements.
The exact same source code for all operating systems.

01:43 February 26, 2012
Changes from version 0.0.11 to 0.0.12
Automatic restart of the program in language change.

23:33 February 1, 2012
Changes from version 0.0.10 to 0.0.11
You can save lists of hash sums in PDF format.

17:31 January 28, 2012
Changes from version 0.0.9 to 0.0.10
You can compare against a selected hash sum.

13:45 July 18, 2011
Changes from version 0.0.8 to 0.0.9
You can choose this program to always check for updates at startup.
You can select files from different folders when you compare files.
You can choose to always start in your home directory. (The default is
last opened folder).

23:36 June 18, 2011
Changes from version 0.0.7 to 0.0.8
You can check which files have changed, who's 
new and what has been lost since the last time you checked.

21:25 June 17, 2011
Changes from version 0.0.5 to 0.0.7
You can recursively create lists of hash sum files.
You can choose the date and time to appear in the hash summ file.

02:05 June 13, 2011
Changes from version 0.0.4 to 0.0.5
Bug fix. You can now open files without extension.

00:57 June 12, 2011
Changes from version 0.0.3 to 0.0.4
Lists with check sums can be saved automatically.
You can select the default file name on file with the checksums.

21:46 June 10, 2011
Changes from version 0.0.2 to 0.0.3
Ability to create lists of checksums.
Swedish-speaking.

17:54 June 05, 2011
Changes from version 0.0.1 to 0.0.2
Clearer messages from the program.

15:21 June 05, 2011
Release version 0.0.1
