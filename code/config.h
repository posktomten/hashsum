/*
    Hash
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#ifndef CONFIGH
#define CONFIGH
#include<string>
#include<vector>

#include "createuser.h"



using namespace std;


class Config
{


private:


    vector<string> *name_values{};
    bool createConffile(const string &conf);
    void checkConf(const string &conf);
    bool readConf(const string &conf);
    bool writeConf();

    void newConf(const string &name, const string &value);
    string *conf_file;
    Createuser *cr;




public:
    Config(QString namn);
    ~Config();



    string getConf(const string &name);
    string getConf(const string &name, const string &default_value);
    bool setConf(string name, const string &value);


};

#endif // CONFIGH
