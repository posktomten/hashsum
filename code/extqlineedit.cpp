/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "extqlineedit.h"
#include "ui_hash.h"
#include "hash.h"
ExtQLineEdit::ExtQLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

ExtQLineEdit::~ExtQLineEdit() = default;





void ExtQLineEdit::mouseDoubleClickEvent(QMouseEvent * event)
{
    this->setToolTip(tr(""));

    if(event->button() == Qt::LeftButton) {
        if(this->text() != "" &&  this->text().mid(0, 7) != tr("Mission")) {
            QClipboard *clipboard = QApplication::clipboard();
            clipboard->setText(this->text());
            this->setToolTip(tr("Hash sum has been copied to the clipboard"));
        }
    }
}

void ExtQLineEdit::mouseMoveEvent(QMouseEvent * event)
{
    event->accept();

    if(this->text() != "" && this->text().mid(0, 7) != tr("Mission")) {
        if(this->toolTip() != tr("Hash sum has been copied to the clipboard"))
            this->setToolTip(tr("Double-click to copy to clipboard"));
    } else
        this->setToolTip(tr(""));
}




