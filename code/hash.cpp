/*
    hashSum
    Copyright (C) 2011-2023 Ingemar Ceicer
    https://gitlab.com/posktomten/hashsum/-/wikis/Home
    programming@ceicer.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "hash.h"
#include "ui_hash.h"
#include "ui_license.h"

#include "extqlineedit.h"
#include "about.h"


Hash::Hash(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Hash)



{
    /* draganddrop */
    setAcceptDrops(true);
    setStartConfig();
    mLibtexttospeech = new Libtexttospeech;
    // VOICES
    QVector<QVoice> *avaiableVoices = new QVector<QVoice>(mLibtexttospeech->getVoices());
    QVector<QVoice>::iterator ir = avaiableVoices->begin();

    for(avaiableVoices->begin(); ir < avaiableVoices->end(); ir++) {
        QString voice(QString(ir->name()));
        QAction *newAction = new QAction(voice);
        newAction->setCheckable(true);
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(k->getConf("defaultvoice") == newAction->text().toStdString()) {
            newAction->setChecked(true);
            mLibtexttospeech->setVoice(*ir);
            currentVoice = *ir;
            std::string algorithm = k->getConf("algorithm");
            QString _algorithm = QString::fromStdString(algorithm);
            tesp(tr("Welcome to hashSum. I use the ") + _algorithm + tr(" algorithm when calculating hash sums."));
        }

        delete k;
        ui->menuSpeech->addAction(newAction);
        connect(newAction, &QAction::triggered, [this, newAction, ir]() {
            foreach(QAction *a, ui->menuSpeech->actions()) {
                a->setChecked(false);
            }

            QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
            Config *kk = new Config(conf_file_name);
            mLibtexttospeech->setVoice(*ir);
            currentVoice = *ir;
            kk->setConf("silent", "false");
            kk->setConf("defaultvoice", newAction->text().QString::toStdString());
//            ui->actionSilent->setChecked(false);
            newAction->setChecked(true);
            tesp(tr("My name is ") + ir->name());
            delete kk;
        });
    }

    connect(ui->actionSilent, &QAction::triggered, [&, this]() {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionSilent->isChecked()) {
            k->setConf("silent", "true");

            foreach(QAction *a, ui->menuSpeech->actions()) {
                a->setChecked(false);
            }

            k->setConf("defaultvoice", "0");
            ui->actionSilent->setChecked(true);
        } else {
            k->setConf("silent", "false");
        }

        delete k;
    });

    //
#ifdef Q_OS_LINUX
    ui->actionApplicationsMenuShortcut->setEnabled(true);
    ui->actionApplicationsMenuShortcut->setVisible(true);
    ui->actionMainteanceTool->setEnabled(true);
    ui->actionMainteanceTool->setVisible(true);
    ui->actionMainteanceTool->setText(tr("Update"));
#endif
#ifdef Q_OS_WINDOWS
#ifdef OFFLINE_INSTALLER
    const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QFile fil(tmppath + "/" FILENAME);

    if(fil.exists()) {
        fil.remove();
    }

    ui->actionMainteanceTool->setEnabled(true);
    ui->actionMainteanceTool->setVisible(true);
#endif
#endif
#ifdef Q_OS_WINDOWS
#ifndef OFFLINE_INSTALLER
    ui->actionApplicationsMenuShortcut->setEnabled(true);
    ui->actionApplicationsMenuShortcut->setVisible(true);
#endif
#endif
// File
    connect(ui->actionChecksum, SIGNAL(triggered()), this, SLOT(checksum()));
    connect(ui->actionCompare, SIGNAL(triggered()), this, SLOT(compare()));
    connect(ui->actionCompareGivenHashSum, SIGNAL(triggered()), this, SLOT(compareGivenHashSum()));
    connect(ui->actionCreateChecksumList, SIGNAL(triggered()), this, SLOT(checkSumListInit()));
    connect(ui->actionOpenHashSumList, SIGNAL(triggered()), this, SLOT(openHashSumList()));
    connect(ui->actionOpenComparisonList, SIGNAL(triggered()), this, SLOT(openComparisonList()));
    connect(ui->actionCreateChecksumLists, SIGNAL(triggered()), this, SLOT(checkSumLists())); //Rekursivt
    /* DUPLICATES */
    connect(ui->actionFindIdenticalFiles, &QAction::triggered, [this]() {
        findDuplicates();
    });

    /* IS THERE A DUOLICATE */
    connect(ui->actionIsThereADuplicate, &QAction::triggered, [this]() {
        isThereADuplicate();
    });

    // Save
    connect(ui->actionSaveToFile, SIGNAL(triggered()), this, SLOT(saveOne()));
// Recent Files, undermeny
    connect(ui->menuRecentFiles, SIGNAL(aboutToShow()), this, SLOT(slotRecentFiles()));
    connect(ui->actionCreateDebianMd5sums, SIGNAL(triggered()), this, SLOT(createDebianMd5sums()));
    connect(ui->actionCreateDebianMd5sumsAuto, SIGNAL(triggered()), this, SLOT(createDebianMd5sumsAuto()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
// Text
    connect(ui->actionHashSumText, SIGNAL(triggered()), this, SLOT(hashSumText()));
    connect(ui->actionCompareHashSumText, SIGNAL(triggered()), this, SLOT(compareHashSumText()));
// Edit
    connect(ui->actionFindChangedFiles, SIGNAL(triggered()), this, SLOT(findChangedFiles()));
    connect(ui->actionCopyPath, SIGNAL(triggered()), this, SLOT(copyPath()));
    connect(ui->actionCopyHashSum, SIGNAL(triggered()), this, SLOT(copyHashSum()));
// Algorithm
    connect(ui->actionMD4, SIGNAL(triggered()), this, SLOT(md4()));
    connect(ui->actionMD5, SIGNAL(triggered()), this, SLOT(md5()));
    connect(ui->actionSHA1, SIGNAL(triggered()), this, SLOT(sha1()));
    connect(ui->actionSHA224, SIGNAL(triggered()), this, SLOT(sha224()));
    connect(ui->actionSHA256, SIGNAL(triggered()), this, SLOT(sha256()));
    connect(ui->actionSHA384, SIGNAL(triggered()), this, SLOT(sha384()));
    connect(ui->actionSHA512, SIGNAL(triggered()), this, SLOT(sha512()));
    connect(ui->actionSHA3_224, SIGNAL(triggered()), this, SLOT(sha3_224()));
    connect(ui->actionSHA3_256, SIGNAL(triggered()), this, SLOT(sha3_256()));
    connect(ui->actionSHA3_384, SIGNAL(triggered()), this, SLOT(sha3_384()));
    connect(ui->actionSHA3_512, SIGNAL(triggered()), this, SLOT(sha3_512()));
    connect(ui->actionKeccak_224, SIGNAL(triggered()), this, SLOT(keccak_224()));
    connect(ui->actionKeccak_256, SIGNAL(triggered()), this, SLOT(keccak_256()));
    connect(ui->actionKeccak_384, SIGNAL(triggered()), this, SLOT(keccak_384()));
    connect(ui->actionKeccak_512, SIGNAL(triggered()), this, SLOT(keccak_512()));
// Tools
    connect(ui->actionCheckOnStart, SIGNAL(triggered()), this, SLOT(checkOnStart()));
    connect(ui->actionSaveChecksumListWhereFilesAre, SIGNAL(triggered()), this, SLOT(saveChecksumListWhereFilesAre()));
    connect(ui->actionDefaultFileName, SIGNAL(triggered()), this, SLOT(defaultFileName()));
    connect(ui->actionWriteTimeDateInHashSumFile, SIGNAL(triggered()), this, SLOT(writeTimeDateInHashSumFile()));
    connect(ui->actionUseNativeDialogs, SIGNAL(triggered()), this, SLOT(useNativeDialogs()));
    connect(ui->actionAllwaysOpenHomeDirectory, SIGNAL(triggered()), this, SLOT(allwaysOpenHomeDirectory()));
    connect(ui->actionAlwaysOpenTheDisplayWindow, &QAction::triggered, [this]() -> void {

        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionAlwaysOpenTheDisplayWindow->isChecked())
            k->setConf("alwaysopenthedisplaywindow", "1");
        else if(!ui->actionAlwaysOpenTheDisplayWindow->isChecked())
            k->setConf("alwaysopenthedisplaywindow", "0");

        delete k;
    });
    connect(ui->actionShowFullPathInHashSumFile, SIGNAL(triggered()), this, SLOT(showFullPathInHashSumFile()));
    // SHORTCUTS
#ifdef Q_OS_LINUX
    connect(ui->actionApplicationsMenuShortcut, &QAction::triggered, [this]() {
        const QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionApplicationsMenuShortcut->isChecked())
            k->setConf("applicationsmenushortcut", "1");
        else
            k->setConf("applicationsmenushortcut", "0");

        delete k;
        Createshortcut *mCreateshortcut = new Createshortcut;

        if(ui->actionApplicationsMenuShortcut->isChecked()) {
            QString *display_name = new QString(QStringLiteral("hashSum"));
            QString *executable_name = new QString(QStringLiteral("hashSum"));
            QString *comments = new QString(QStringLiteral("Calculates hash sums"));
            QString *categories = new QString(QStringLiteral("Utilitys"));
            QString *icon  = new QString(QStringLiteral("hashSum.png"));
            mCreateshortcut->makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
        } else {
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->removeApplicationShortcut(executable_name);
        }
    });

    connect(ui->actionDesktopShortcut, &QAction::triggered, [this]() {
        const QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionDesktopShortcut->isChecked())
            k->setConf("desktopshortcut", "1");
        else
            k->setConf("desktopshortcut", "0");

        delete k;
        Createshortcut *mCreateshortcut = new Createshortcut;

        if(ui->actionDesktopShortcut->isChecked()) {
            Createshortcut *mCreateshortcut = new Createshortcut;
            QString *display_name = new QString(QStringLiteral("hashSum"));
            QString *executable_name = new QString(QStringLiteral("hashSum"));
            QString *comments = new QString(QStringLiteral("Calculates hash sums"));
            QString *categories = new QString(QStringLiteral("Utilitys"));
            QString *icon  = new QString(QStringLiteral("hashSum.png"));
            mCreateshortcut->makeShortcutFile(display_name, executable_name, comments, categories, icon, false, true);
        } else {
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->removeDesktopShortcut(executable_name);
        }
    });

    connect(ui->actionMainteanceTool, &QAction::triggered, [ this ]() -> void {
        close();
        Update *up = new Update;

        QIcon *icon = new QIcon(":/images/hashsum.png");
        // QDialog class
        ud = new UpdateDialog;
        ud->viewUpdate(icon);
        ud->show();

        up->doUpdate(ARG1, ARG2, APPLICATION_NAME, ud, icon);
    });
#endif
#ifdef Q_OS_WINDOWS
#ifndef OFFLINE_INSTALLER
    connect(ui->actionApplicationsMenuShortcut, &QAction::triggered, [this]() {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionApplicationsMenuShortcut->isChecked())
            k->setConf("applicationsmenushortcut", "1");
        else
            k->setConf("applicationsmenushortcut", "0");

        delete k;
        Createshortcut *mCreateshortcut = new Createshortcut;

        if(ui->actionApplicationsMenuShortcut->isChecked()) {
            QString *display_name = new QString(QStringLiteral(APPLICATION_NAME));
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->makeShortcutFile(display_name, executable_name, true, false);
        } else {
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->removeApplicationShortcut(executable_name);
        }
    });

#endif
    connect(ui->actionDesktopShortcut, &QAction::triggered, [this]() {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);

        if(ui->actionDesktopShortcut->isChecked())
            k->setConf("desktopshortcut", "1");
        else
            k->setConf("desktopshortcut", "0");

        delete k;
        Createshortcut *mCreateshortcut = new Createshortcut;

        if(ui->actionDesktopShortcut->isChecked()) {
            QString *display_name = new QString(QStringLiteral(APPLICATION_NAME));
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->makeShortcutFile(display_name, executable_name, false, true);
        } else {
            QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
            mCreateshortcut->removeDesktopShortcut(executable_name);
        }
    });

#ifdef OFFLINE_INSTALLER
    connect(ui->actionMainteanceTool, &QAction::triggered, [ this ]() -> void {


        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setInformativeText("<a href = \""  APPLICATION_HOMEPAGE_ENG   "\">" + tr("To the website") + "</a>");
        msgBox->setIcon(QMessageBox::Information);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("Download the latest version"));
        QAbstractButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
        pbCancel->setFixedSize(QSize(150, 40));
        QAbstractButton *pbUninstall = msgBox->addButton(tr("Uninstall"), QMessageBox::AcceptRole);
        pbUninstall->setFixedSize(QSize(150, 40));
        QAbstractButton *pbUninstallInstall = msgBox->addButton(tr("Download the new version"), QMessageBox::AcceptRole);
        pbUninstallInstall->setFixedSize(QSize(300, 40));
        pbUninstallInstall->setFocus();
        msgBox->exec();

        if(msgBox->clickedButton() == pbCancel) {
            return;
        }

        if(msgBox->clickedButton() == pbUninstall)
        {
            const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            "uninstall.exe";
            //
            QFileInfo fi(EXECUTE);

            if(!fi.isExecutable()) {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error occurred.<br>") + "\"" + QDir::toNativeSeparators(EXECUTE) + "\"" + tr("<br>can not be found or is not an executable program."));
                msgBox->exec();
                return;
            }

            //
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
            return;
        } else if(msgBox->clickedButton() == pbUninstallInstall)
        {
            close();
            DownloadInstall *mDownloadInstall;
            mDownloadInstall = new DownloadInstall(nullptr);
            QString *path = new QString(QStringLiteral(PATH));
            QString *filename = new QString(QStringLiteral(FILENAME));
            QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
            QString *version = new QString(QStringLiteral(VERSION));
            mDownloadInstall->setValues(path, filename, display_name, version);
            mDownloadInstall->show();
        }
    });
#endif
#endif
// Help
    connect(ui->actionHelp, SIGNAL(triggered()), this, SLOT(help()));
    connect(ui->actionAboutQt, &QAction::triggered, [this]() {
        QMessageBox::aboutQt(this);
    });

    // connect(ui->actionCheckForUpdates, SIGNAL(triggered()), this, SLOT(checkForUpdates()));
    connect(ui->actionCheckForUpdates, &QAction::triggered, [this] {

        CheckForUpdates *cu = new CheckForUpdates;
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        Config *kk = new Config(conf_file_name);
        std::string s = kk->getConf("silent");


        if(s == "true") {
            bool *sant = new bool(true);
            cu->check(APPLICATION_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH, currentVoice, sant);
        } else {
            bool *falskt = new bool(false);
            cu->check(APPLICATION_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH, currentVoice, falskt);
        }
        delete kk;
    });
    connect(ui->actionLicense, SIGNAL(triggered()), this, SLOT(license()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionVersionHistory, SIGNAL(triggered()), this, SLOT(versionhistory()));
//#ifdef Q_OS_WIN // Windows 32- and 64-bit
    /* UNINSTALL */
    connect(ui->actionUninstall, &QAction::triggered, [] {
//        AppWiz.cpl

#ifdef Q_OS_WIN // Windows 32- and 64-bit
        bool b = QProcess::startDetached("control AppWiz.cpl");

        if(b)
            exit(0);
        QMessageBox::critical(nullptr, APPLICATION_NAME " " VERSION, tr("\"Control Panel\\All Control Panel Items\\Programs and Features\" cannot be found"));
#endif



#ifdef Q_OS_LINUX     // Linux
        if(QTINSTALLERFRAMEWORK) {
            QString path = QCoreApplication::applicationDirPath() + "/hashSumMaintenanceTool";
            // qInfo() << path;
            bool b = QProcess::startDetached(path);

            if(b)
                exit(0);

            QMessageBox::critical(nullptr, APPLICATION_NAME " " VERSION, tr("MaintenanceTool cannot be found"));
        }

#endif

    });
//#endif
// Language
    connect(ui->actionSwedish, SIGNAL(triggered()), this, SLOT(swedish()));
    connect(ui->actionGreek, SIGNAL(triggered()), this, SLOT(greek()));
    connect(ui->actionGerman, SIGNAL(triggered()), this, SLOT(german()));
    connect(ui->actionEnglish, SIGNAL(triggered()), this, SLOT(english()));
}

Hash::~Hash()
{
    setEndConfig();
    delete ui;
}

// public slots:
// private slots:
void Hash::openFile()
{
    delete d;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string saveChecksumListWhereFilesAre = k->getConf("saveChecksumListWhereFilesAre");
    std::string usenativedialogs = k->getConf("usenativedialogs");
    QString qpath;

    if(allwaysopenhomedirectory == "0") {
        qpath = QString::fromStdString(k->getConf("save_path"));

        if(qpath == "nothing")
            qpath = QDir::homePath();
    } else if(allwaysopenhomedirectory == "1")
        qpath = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Open the hashSum file"),
                                          qpath,
                                          tr("Text files (*)"));

    if(usenativedialogs == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(usenativedialogs == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setReadOnly(true);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFile);

    if(dialog->exec()) {
        QStringList files = dialog->selectedFiles();
        QFile file2(QDir::toNativeSeparators(files[0]));
        file2.open(QIODevice::ReadOnly);
        QTextStream stream(&file2);
        QString content = stream.readAll();
        file2.close();
        d = new QDialog;
        ui2 = new Ui::Lic;
        //Font ui2
        int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        // SLUT Font ui2
        ui2->setupUi(d);
        ui2->textBrowser->setFont(f);
        ui2->textBrowser->setFixedWidth(800);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        ui2->pbOpenFile->setText(tr("Open"));
        ui2->textBrowser->setPlainText(content);
        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION "  " + QDir::toNativeSeparators(files[0]));
        d->setFixedWidth(800);
        d->setFixedHeight(500);
        QIcon ikon("/images/hashsum.ico");
        d->setWindowIcon(ikon);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        connect(ui2->pbOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
        d->show();
    }
}

void Hash::openHashSumList()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string saveChecksumListWhereFilesAre = k->getConf("saveChecksumListWhereFilesAre");
    std::string path;

    if(saveChecksumListWhereFilesAre == "1")
        path = k->getConf("open_path");
    else if(saveChecksumListWhereFilesAre == "0")
        path = k->getConf("save_path");

    if(path == "nothing")
        path = QDir::homePath().toStdString();

    std::string algorithm = k->getConf("algorithm");
    std::string defaultfilename = k->getConf("defaultfilename", algorithm);
    QString qpath = QString::fromStdString(path);
    QString qdefaultfilename = QString::fromStdString(defaultfilename);
    QFile file(QDir::toNativeSeparators(qpath + '/' + qdefaultfilename));

    if(!file.exists()) {
        QMessageBox::warning(this, APPLICATION_NAME " " VERSION, qpath + '/' + qdefaultfilename + tr(" is not found"));
        QString sokvag;
        std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
        std::string usenativedialogs = k->getConf("usenativedialogs");
        QString qusenativedialogs = QString::fromStdString(usenativedialogs);
        QFileInfo info(qpath);

        if(info.isReadable() && allwaysopenhomedirectory == "0")
            sokvag = qpath;
        else
            sokvag = QDir::homePath();

        QFileDialog *dialog = new QFileDialog(this, tr("Open the hashSum file"),
                                              sokvag,
                                              tr("Text files (*)"));

        if(qusenativedialogs == "0")
            dialog->setOption(QFileDialog::DontUseNativeDialog, true);

        if(qusenativedialogs == "1")
            dialog->setOption(QFileDialog::DontUseNativeDialog, false);

        dialog->setReadOnly(true);
        dialog->setAcceptMode(QFileDialog::AcceptOpen);
        dialog->setFileMode(QFileDialog::ExistingFile);

        if(dialog->exec()) {
            QStringList files = dialog->selectedFiles();
            QFile file2(QDir::toNativeSeparators(files[0]));
            file2.open(QIODevice::ReadOnly);
            QTextStream stream(&file2);
            QString content = stream.readAll();
            file2.close();
            d = new QDialog;
            auto *ui2 = new Ui::Lic;        //Font ui2
            int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
            QString family = QFontDatabase::applicationFontFamilies(id).at(0);
            QFont f(family);
            // SLUT Font ui2
            ui2->setupUi(d);
            ui2->textBrowser->setFont(f);
            ui2->textBrowser->setFixedWidth(800);
            /* COLOR */
            QColor c(RED, GREEN, BLUE);
            QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
            p.setColor(QPalette::Base, c); // set color "Red" for textedit base
            ui2->textBrowser->setPalette(p); // change textedit palette
            /* */
            ui2->pbOpenFile->setText(tr("Open"));
            ui2->textBrowser->setPlainText(content);
            d->setWindowFlags(Qt::WindowSystemMenuHint);
            d->setWindowTitle(APPLICATION_NAME " " VERSION "  " + QDir::toNativeSeparators(files[0]));
            d->setFixedWidth(800);
            d->setFixedHeight(500);
            QIcon ikon("/images/hashsum.ico");
            d->setWindowIcon(ikon);
            connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
            connect(ui2->pbOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
            d->show();
        }
    } else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        d = new QDialog;
        ui2 = new Ui::Lic;
        //Font ui2
        int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        // SLUT Font ui2
        ui2->setupUi(d);
        ui2->textBrowser->setFont(f);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        ui2->textBrowser->setPlainText(content);
        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION  "  " + QDir::toNativeSeparators(qpath + '/' + qdefaultfilename));
        d->setFixedWidth(800);
        d->setFixedHeight(500);
        ui2->textBrowser->setFixedWidth(800);
        //   ui2->pbOpenFile->setGeometry(298,470,100,24);
        ui2->pbOpenFile->setText(tr("Open"));
        //   ui2->pbClose->setGeometry(402,470,100,24);
        QIcon ikon("/images/hashsum.ico");
        d->setWindowIcon(ikon);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        connect(ui2->pbOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
        d->show();
    }

    delete k;
}

void Hash::openComparisonList()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string saveChecksumListWhereFilesAre = k->getConf("saveChecksumListWhereFilesAre");
    std::string path;

    if(saveChecksumListWhereFilesAre == "1")
        path = k->getConf("open_path");
    else if(saveChecksumListWhereFilesAre == "0")
        path = k->getConf("save_path");

    if(path == "nothing")
        path = QDir::homePath().toStdString();

    std::string algorithm = k->getConf("algorithm");
    std::string defaultfilename = k->getConf("defaultfilename", algorithm);
    QString qpath = QString::fromStdString(path);
    QString qdefaultfilename = QString::fromStdString(defaultfilename);
    QFile file(QDir::toNativeSeparators(qpath + "/check-" + qdefaultfilename));

    if(!file.exists()) {
        QMessageBox::warning(this, APPLICATION_NAME " " VERSION, qpath + "/check-" + qdefaultfilename + tr(" is not found"));
        QString sokvag;
        std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
        std::string usenativedialogs = k->getConf("usenativedialogs");
        QString qusenativedialogs = QString::fromStdString(usenativedialogs);
        QFileInfo info(qpath);

        if(info.isReadable() && allwaysopenhomedirectory == "0")
            sokvag = qpath;
        else
            sokvag = QDir::homePath();

        QFileDialog *dialog = new QFileDialog(this, tr("Open the hashSum file"),
                                              sokvag,
                                              tr("Text files (*)"));

        if(qusenativedialogs == "0")
            dialog->setOption(QFileDialog::DontUseNativeDialog, true);

        if(qusenativedialogs == "1")
            dialog->setOption(QFileDialog::DontUseNativeDialog, false);

        dialog->setReadOnly(true);
        dialog->setAcceptMode(QFileDialog::AcceptOpen);
        dialog->setFileMode(QFileDialog::ExistingFile);

        if(dialog->exec()) {
            QStringList files = dialog->selectedFiles();
            QFile file2(QDir::toNativeSeparators(files[0]));
            file2.open(QIODevice::ReadOnly);
            QTextStream stream(&file2);
            QString content = stream.readAll();
            file2.close();
            d = new QDialog;
            ui2 = new Ui::Lic;
            //Font ui2
            int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
            QString family = QFontDatabase::applicationFontFamilies(id).at(0);
            QFont f(family);
            // SLUT Font ui2
            ui2->setupUi(d);
            ui2->textBrowser->setFont(f);
            ui2->textBrowser->setPlainText(content);
            /* COLOR */
            QColor c(RED, GREEN, BLUE);
            QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
            p.setColor(QPalette::Base, c); // set color "Red" for textedit base
            ui2->textBrowser->setPalette(p); // change textedit palette
            /* */
            d->setWindowFlags(Qt::WindowSystemMenuHint);
            d->setWindowTitle(APPLICATION_NAME " " VERSION "  " + QDir::toNativeSeparators(files[0]));
            d->setFixedWidth(800);
            d->setFixedHeight(500);
            ui2->textBrowser->setFixedWidth(800);
            //    ui2->pbOpenFile->setGeometry(298,470,100,24);
            ui2->pbOpenFile->setText(tr("Open"));
            //    ui2->pbClose->setGeometry(402,470,100,24);
            QIcon ikon("/images/hashsum.ico");
            d->setWindowIcon(ikon);
            connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
            connect(ui2->pbOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
            d->show();
        }
    } else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        d = new QDialog;
        ui2 = new Ui::Lic;
        //Font ui2
        int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        // SLUT Font ui2
        ui2->setupUi(d);
        ui2->textBrowser->setFont(f);
        ui2->textBrowser->setPlainText(content);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION  "  " + QDir::toNativeSeparators(qpath + "/check-" + qdefaultfilename));
        d->setFixedWidth(800);
        d->setFixedHeight(500);
        ui2->textBrowser->setFixedWidth(800);
        ui2->pbOpenFile->setText(tr("Open"));
        QIcon ikon("/images/hashsum.ico");
        d->setWindowIcon(ikon);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        connect(ui2->pbOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
        d->show();
    }

    delete k;
}

void Hash::slotRecentFiles()
{
    recentFiles.removeDuplicates();

    while(recentFiles.size() > MAX_RECENT_FILES)
        recentFiles.removeFirst();

    ui->menuRecentFiles->clear();

    for(int i = recentFiles.size() - 1; i >= 0; i--) {
        QString path = recentFiles.at(i);
        auto* actionRF = new QAction(path, this);
        ui->menuRecentFiles->addAction(actionRF);
        auto* signalMapperRF  = new QSignalMapper(this);
        connect(actionRF, SIGNAL(triggered()), signalMapperRF, SLOT(map()));
        signalMapperRF->setMapping(actionRF, path);
        connect(signalMapperRF, SIGNAL(mapped(const QString)), this, SLOT(recentFileRequested(const QString)));
    }

    ui->menuRecentFiles->addSeparator();
    QAction* actionAntalRecentFiles = new QAction(tr("The number of recently opened files to be displayed..."), this);
    ui->menuRecentFiles->addAction(actionAntalRecentFiles);
    connect(actionAntalRecentFiles, SIGNAL(triggered()), this, SLOT(setRecentFiles()));
    QAction* actionClearAntalRecentFiles = new QAction(tr("Clear the list of recently opened files"), this);
    ui->menuRecentFiles->addAction(actionClearAntalRecentFiles);
    connect(actionClearAntalRecentFiles, SIGNAL(triggered()), this, SLOT(clearRecentFiles()));
}

void Hash::recentFileRequested(const QString & path)
{
    if(!doChecksum(path)) {
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("The file") + "\n\"" + path + "\"\n" + tr("can not be found. The file will be removed from the list of recently opened files."));
        recentFiles.removeOne(path);
    } else {
        recentFiles.removeOne(path);
        recentFiles.append(path);
    }
}

void Hash::showFullPathInHashSumFile()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("fullpath");

    if(s == "0") {
        ui->actionShowFullPathInHashSumFile->setChecked(true);
        k->setConf("fullpath", "1");
    }

    if(s == "1") {
        ui->actionShowFullPathInHashSumFile->setChecked(false);
        k->setConf("fullpath", "0");
    }

    delete k;
}

void Hash::setRecentFiles()
{
    bool ok;
    int i = QInputDialog::getInt(this, APPLICATION_NAME " " VERSION,
                                 tr("Set number of Recent Files: ") + "(0-" + QString::number(SIZE_OF_RECENT_FILES) + ")", MAX_RECENT_FILES, 0, SIZE_OF_RECENT_FILES, 1, &ok);

    if(ok)
        MAX_RECENT_FILES = i;
}

void Hash::clearRecentFiles()
{
    recentFiles.clear();
}

void Hash::createDebianMd5sums()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    checkSumList(2);
}

void Hash::createDebianMd5sumsAuto()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    hurmangaFoldrar = 0;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qs = QString::fromStdString(s);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs + QDir::toNativeSeparators("/");
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Select the \"usr\" directory to start create hash sums recursively"),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setFileMode(QFileDialog::Directory);
    dialog->setOption(QFileDialog::ShowDirsOnly);
    dialog->setReadOnly(true);
    QStringList allt;

    if(dialog->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir = dialog->directory();
        QString indent = "";
        bool lyckats = rekursivtDeb(dir, indent, allt);

        if(!lyckats)
            return;

        QString directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        allt.removeDuplicates();
        int pos0, pos1, pos2;
        QFile file(directoryName + "/../DEBIAN/md5sums");
        QTextStream out(&file);
        QString alltihopa;

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions.") + file.errorString());
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
        }

        for(int i = 0; i < allt.count(); i++) {
            alltihopa = allt[i];
            pos0 = alltihopa.indexOf("/");
            pos1 = alltihopa.lastIndexOf("/usr/");
            alltihopa = alltihopa.mid(0, pos0) + alltihopa.mid(pos1 + 1);
            pos2 = alltihopa.indexOf("usr/local/bin/");

            if(pos2 < 0)
                out << alltihopa << "\n";
        }

        file.close();
        ui->statusBar->showMessage(tr("md5sums file has been successfully created!"));
        int pos = directoryName.lastIndexOf("/usr");
        QString placerad = directoryName.mid(0, pos);
        ui->progressLabel->setText(tr("md5sums file has been successfully created in ") + placerad + "/DEBIAN/");
        delete k;
    } else
        delete k;
}

void Hash::help()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("language");
    delete k;
    QMessageBox msgBox(this);
    msgBox.setTextFormat(Qt::RichText);   //this is what makes the links clickable
    const QString applicationdirpath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/");
    msgBox.setIconPixmap(QPixmap(applicationdirpath + "hashsum.png"));
    msgBox.setWindowTitle(APPLICATION_NAME " " VERSION);
    msgBox.setStandardButtons(QMessageBox::Ok);
    QString sprak, applicationHomepage;

    if(s == "sv_SE") {
        sprak = "https://gitlab.com/posktomten/hashsum/-/wikis/Homehash/screenshots.html";
        applicationHomepage = APPLICATION_HOMEPAGE;
    } else {
        sprak = "https://gitlab.com/posktomten/hashsum/-/wikis/Homehash/screenshots_eng.html";
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;
    }

    msgBox.setText(tr("The program's website can be found here:") + "<br><a href=\"" + applicationHomepage + "\">" + applicationHomepage + "</a>");
    // msgBox.setText(tr("Help with the program and all settings and menus can be found here:") + "<br><a href=\"" + sprak + "\">" + sprak + "</a><br>" + tr("The program's website can be found here:") + "<br><a href=\"" + applicationHomepage + "\">" + applicationHomepage + "</a>");
    msgBox.exec();
}

void Hash::compareHashSumText()
{
    QString hashinput, kortHashinput;
    QInputDialog hashin(this);
    hashin.setWindowTitle(APPLICATION_NAME);
    hashin.setLabelText(tr("Compare with this hash sum..."));
    hashin.setInputMode(QInputDialog::TextInput);
    hashin.setCancelButtonText(tr("Cancel"));
    hashin.setOkButtonText(tr("OK"));

    if(hashin.exec())
        hashinput = hashin.textValue();
    else
        return;

    if(hashinput.length() > 40)
        kortHashinput = hashinput.mid(0, 40) + "...";
    else
        kortHashinput = hashinput;

    QInputDialog hashtxt(this);
    hashtxt.setWindowTitle(APPLICATION_NAME);
    hashtxt.setLabelText(tr("...with hash sum for this text:"));
    hashtxt.setInputMode(QInputDialog::TextInput);
    hashtxt.setCancelButtonText(tr("Cancel"));
    hashtxt.setOkButtonText(tr("OK"));

    if(hashtxt.exec()) {
        const QString text = hashtxt.textValue();
        qint64 i = text.size();
        QString qlineeditmax;
        qlineeditmax.setNum(QLINEEDIT_MAX, 10);

        if(i >= QLINEEDIT_MAX) {
            tesp(APPLICATION_NAME " " VERSION + tr(" can handle texts up to ") + qlineeditmax + tr(" characters. Please reduce the length of the text."));
            QMessageBox::information(this, APPLICATION_NAME " " VERSION, APPLICATION_NAME " " VERSION + tr(" can handle texts up to ") + qlineeditmax + tr(" characters.\nPlease reduce the length of the text."));
            return;
        }

        ui->lineEdit->clear();
        ui->lineEdit->setToolTip("");

        if(!(doChecksumText(text))) {
            tesp(tr("Hash sum calculated for:") + " \"" + text + "\" " + tr("could not be calculated. An unknown error has occurred."));
            ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text + "\" " + tr("could not be calculated. An unknown error has occurred."));
        } else {
            QString samma;

            if(hashinput != ui->lineEdit->text()) {
                samma = tr("It is NOT the same as the hash sum you compare to.") + " (" + kortHashinput + ")";
                tesp(tr("Hash sums are NOT equal!"));
                ui->statusBar->showMessage(tr("Hash sums are NOT equal!"));
            }

            if(hashinput == ui->lineEdit->text()) {
                samma = tr("It is the same as the hash sum you compare to.") + " (" + kortHashinput + ")";
                tesp(tr("Hash sums are equal!"));
                ui->statusBar->showMessage(tr("Hash sums are equal!"));
            }

            QString s;
            s.setNum(i);
            QString stext = text.simplified();
            qint64 si = stext.size();

            if(si < i) {
                if(si < 20)
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + stext + "\" (" + s + " " + tr("characters.") + ") " + samma + tr(" Whitespace is not removed when the hash sums is calculated."));
                else
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + stext.mid(0, 20) + "...\" (" + s + " " + tr("characters") + ") " + samma + tr(" Whitespace is not removed when the hash sums is calculated."));
            } else {
                if(i < 20)
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text + "\" (" + s + " " + tr("characters.") + ") " + samma);
                else
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text.mid(0, 20) + "...\" (" + s + " " + tr("characters") + " ) " + samma);
            }

            ui->statusBar->showMessage(tr("Hash sums are compared!"));
        }
    }
}

void Hash::hashSumText()
{
    QInputDialog hashtxt(this);
    hashtxt.setWindowTitle(APPLICATION_NAME);
    hashtxt.setLabelText(tr("Calculate the hash sum of this text:"));
    hashtxt.setInputMode(QInputDialog::TextInput);
    hashtxt.setCancelButtonText(tr("Cancel"));
    hashtxt.setOkButtonText(tr("OK"));

    if(hashtxt.exec()) {
        const QString text = hashtxt.textValue();

        if(text == "aboutqt")
            QMessageBox::aboutQt(this, APPLICATION_NAME " " VERSION);

        qint64 i = text.size();
        QString qlineeditmax;
        qlineeditmax.setNum(QLINEEDIT_MAX, 10);

        if(i >= QLINEEDIT_MAX) {
            tesp(APPLICATION_NAME " " VERSION + tr(" can handle texts up to ") + qlineeditmax + tr(" characters. Please reduce the length of the text."));
            QMessageBox::information(this, APPLICATION_NAME " " VERSION, APPLICATION_NAME " " VERSION + tr(" can handle texts up to ") + qlineeditmax + tr(" characters.\nPlease reduce the length of the text."));
            return;
        }

        ui->lineEdit->clear();
        ui->lineEdit->setToolTip("");

        if(!(doChecksumText(text))) {
            tesp(tr("Hashsum could not be calculated. An unknown error has occurred."));
            ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text + "\" " + tr("could not be calculated. An unknown error has occurred."));
        } else {
            QString s;
            s.setNum(i);
            QString stext = text.simplified();
            qint64 si = stext.size();

            if(si < i) {
                if(si < 20)
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + stext + "\" (" + s + " " + tr("characters.") + ") " + tr("Whitespace is not removed when the hash sums is calculated."));
                else
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + stext.mid(0, 20) + "...\" (" + s + " " + tr("characters") + ") " + tr("Whitespace is not removed when the hash sums is calculated."));
            } else {
                if(i < 20)
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text + "\" (" + s + " " + tr("characters.") + ")");
                else
                    ui->progressLabel->setText(tr("Hash sum calculated for:") + " \"" + text.mid(0, 20) + "...\" (" + s + " " + tr("characters") + ")");
            }

            tesp(tr("Hash sum has been calculated!"));
            ui->statusBar->showMessage(tr("Hash sum has been calculated!"));
        }
    }
}

void Hash::copyPath()
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(QDir::toNativeSeparators(ui->progressLabel->toolTip()));
    tesp(tr("Double-click to copy to clipboard"));
    ui->lineEdit->setToolTip(tr("Double-click to copy to clipboard"));
}

void Hash::copyHashSum()
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(ui->lineEdit->text());
    tesp(tr("Hash sum has been copied to the clipboard"));
    ui->lineEdit->setToolTip(tr("Hash sum has been copied to the clipboard"));
}

void Hash::compareGivenHashSum()
{
    QInputDialog compareGiven(this);
    compareGiven.setWindowTitle(APPLICATION_NAME);
    compareGiven.setLabelText(tr("Compare with this hash sum:"));
    compareGiven.setInputMode(QInputDialog::TextInput);
    compareGiven.setCancelButtonText(tr("Cancel"));
    compareGiven.setOkButtonText(tr("OK"));

    if(compareGiven.exec()) {
        QString given = compareGiven.textValue();
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        QString sokvag;
        std::string s = k->getConf("open_path");
        std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
        std::string dialogruta = k->getConf("usenativedialogs");
        QString qs = QString::fromStdString(s);
        QFileInfo info(qs);

        if(info.isReadable() && allwaysopenhomedirectory == "0")
            sokvag = qs;
        else
            sokvag = QDir::homePath();

        QFileDialog *dialog = new QFileDialog(this, tr("Open a file to calculate the hash sum"),
                                              sokvag,
                                              tr("All Files (*)"));

        if(dialogruta == "0")
            dialog->setOption(QFileDialog::DontUseNativeDialog, true);
        else if(dialogruta == "1")
            dialog->setOption(QFileDialog::DontUseNativeDialog, false);

        dialog->setAcceptMode(QFileDialog::AcceptOpen);
        dialog->setFileMode(QFileDialog::ExistingFile);
        QStringList fileNames;

        if(dialog->exec()) {
            ui->actionCopyPath->setDisabled(true);
            ui->actionSaveToFile->setDisabled(true);
            ui->actionCopyHashSum->setDisabled(true);
            ui->progressLabel->clear();
            //ui->progressLabel->toolTip().clear();
            ui->lineEdit->clear();
            QDir dir;
            dir = dialog->directory();
            QString directoryName = dir.path();
            k->setConf("open_path", directoryName.toStdString());
            fileNames = dialog->selectedFiles();
        } else {
            delete k;
            return;
        }

        delete k;
        bool lyckades = doChecksum(fileNames[0]);

        if(!lyckades) {
            tesp(tr("No hash sum could be calculated"));
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
        } else {
            if(given == ui->lineEdit->text()) {
                ui->progressLabel->setText(ui->progressLabel->text() + tr(" And the hash sum you compare to is: \"") + given + tr("\" and they are the same"));
                tesp(tr("The hash sums are equal"));
                ui->statusBar->showMessage(tr("The hash sums are equal"));
            } else {
                ui->progressLabel->setText(ui->progressLabel->text() + tr(" And the hash sum you compare to is: \"") + given + tr("\" and they are NOT the same"));
                tesp(tr("The hash sums are not equal"));
                ui->statusBar->showMessage(tr("The hash sums are NOT equal"));
            }
        }
    }
}

void Hash::useNativeDialogs()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);

    if(ui->actionUseNativeDialogs->isChecked()) {
        k->setConf("usenativedialogs", "1");
        QMessageBox::warning(this, APPLICATION_NAME " " VERSION, tr("You have chosen to use native dialogs. It may not always work with your operating system."));
    } else if(!ui->actionUseNativeDialogs->isChecked())
        k->setConf("usenativedialogs", "0");

    delete k;
}

void Hash::allwaysOpenHomeDirectory()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);

    if(ui->actionAllwaysOpenHomeDirectory->isChecked())
        k->setConf("allwaysopenhomedirectory", "1");
    else if(!ui->actionAllwaysOpenHomeDirectory->isChecked())
        k->setConf("allwaysopenhomedirectory", "0");

    delete k;
}

void Hash::findChangedFiles()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("saveChecksumListWhereFilesAre");

    if(s == "1") {
        k->setConf("saveChecksumListWhereFilesAre", "0");
//        delete k;
        checkSumList(1);
//        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
//        k = new Config(conf_file_name);
        k->setConf("saveChecksumListWhereFilesAre", "1");
        delete k;
    }

    if(s == "0") {
        delete k;
        checkSumList(1);
    }
}

void Hash::checkSumListInit()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionSaveToFile->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    checkSumList(0);
}

void Hash::writeTimeDateInHashSumFile()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);

    if(ui->actionWriteTimeDateInHashSumFile->isChecked())
        k->setConf("writeDateTimeHashSumFile", "1");
    else if(!ui->actionWriteTimeDateInHashSumFile->isChecked())
        k->setConf("writeDateTimeHashSumFile", "0");

    delete k;
}

void Hash::license()
{
    // const QString INSTALL_DIR = QCoreApplication::applicationDirPath();
    const QString gpl = ":/txt/gpl.txt";
    QFile file(gpl);
    // QFile file(QDir::toNativeSeparators(INSTALL_DIR + "/gpl.txt"));

    if(!file.exists())
        QMessageBox::warning(this, APPLICATION_NAME " " VERSION, tr("The license file is not found"));
    else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        d = new QDialog;
        ui2 = new Ui::Lic;
        ui2->setupUi(d);
        ui2->textBrowser->setPlainText(content);
        d->setWindowFlags(Qt::WindowSystemMenuHint);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        ui2->pbOpenFile->hide();
        //   ui2->pbClose->setGeometry(150,470,100,24);
        d->setWindowTitle(APPLICATION_NAME " " VERSION);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        d->resize(700, 400);
        d->show();
    }
}

void Hash::versionhistory()
{
    // const QString INSTALL_DIR = QCoreApplication::applicationDirPath();
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string sp = k->getConf("language");
    delete k;
    const QString readme = ":/txt/readme.txt";
    const QString readme_sv = ":/txt/readme_sv.txt";
    QFile file;

    if(sp == "sv_SE") {
        // file.setFileName(QDir::toNativeSeparators(INSTALL_DIR + "/readme_sv.txt"));
        file.setFileName(readme_sv);
    } else {
        //file.setFileName(QDir::toNativeSeparators(INSTALL_DIR + "/readme.txt"));
        file.setFileName(readme);
    }

    if(!file.exists())
        QMessageBox::warning(this, APPLICATION_NAME " " VERSION, tr("The version history file is not found"));
    else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        d = new QDialog;
        ui2 = new Ui::Lic;
        ui2->setupUi(d);
        ui2->textBrowser->setPlainText(content);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        ui2->pbOpenFile->hide();
        //   ui2->pbClose->setGeometry(150,470,100,24);
        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        d->show();
    }
}

void Hash::stang()
{
    delete ui2;
    delete d;
}

void Hash::about()
{
    const QString *purpose = new QString(tr("A program to calculate hash sums and compare files."));
    const QString *translator = new QString(tr("Many thanks to ") + "<a href=\"" elGR_TRANSLATOR "\">"  elGR_TRANSLATOR "</a>" + tr(" for the Greek translation.") + "<br>" + tr("Many thanks to ") +   deDE_TRANSLATOR  + tr(" for the German translation."));
    //        const QString *translator = new QString("");
//    const QPixmap *pixmap = new QPixmap(":/image/icon.png");
    const QString *copyright_year = new QString(COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year()));
    const QPixmap *pixmap = nullptr;
    About *myAbout = new About(DISPLAY_NAME,
                               DISPLAY_VERSION,
                               COPYRIGHT,
                               EMAIL,
                               copyright_year,
                               BUILD_DATE_TIME,
                               LICENSE,
                               LICENSE_LINK,
                               CHANGELOG,
                               SOURCECODE,
                               WEBSITE,
                               COMPILEDON,
                               purpose,
                               translator,
                               pixmap,
                               false);
    delete myAbout;
}

void Hash::checkOnStart()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);

    if(ui->actionCheckOnStart->isChecked())
        k->setConf("checkonstart", "1");
    else
        k->setConf("checkonstart", "0");

    delete k;
}

void Hash::md4()
{
    if(ui->actionMD4->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "md4");
        ui->statusBar->showMessage(tr("Algorithm: ") + "md4");
        ui->actionMD4->setChecked(true);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "md4");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "md4 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm md4"));
        delete k;
    }

    if(!ui->actionMD4->isChecked())
        ui->actionMD4->setChecked(true);
}

void Hash::md5()
{
    if(ui->actionMD5->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "md5");
        ui->statusBar->showMessage(tr("Algorithm: ") + "md5");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(true);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "md5");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "md5 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm md5"));
        delete k;
    }

    if(!ui->actionMD5->isChecked())
        ui->actionMD5->setChecked(true);
}

void Hash::sha1()
{
    if(ui->actionSHA1->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha1");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha1");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(true);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha1");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha1 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha1"));
        delete k;
    }

    if(!ui->actionSHA1->isChecked())
        ui->actionSHA1->setChecked(true);
}

void Hash::sha224()
{
    if(ui->actionSHA224->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha224");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha224");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(true);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha224");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha224 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha224"));
        delete k;
    }

    if(!ui->actionSHA224->isChecked())
        ui->actionSHA224->setChecked(true);
}

void Hash::sha256()
{
    if(ui->actionSHA256->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha256");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha256");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(true);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha256");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha256 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha256"));
        delete k;
    }

    if(!ui->actionSHA256->isChecked())
        ui->actionSHA256->setChecked(true);
}

void Hash::sha384()
{
    if(ui->actionSHA384->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha384");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha384");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(true);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha384");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha384 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha384"));
        delete k;
    }

    if(!ui->actionSHA384->isChecked())
        ui->actionSHA384->setChecked(true);
}

void Hash::sha512()
{
    if(ui->actionSHA512->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha512");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha512");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(true);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha512");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha512 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha512"));
        delete k;
    }

    if(!ui->actionSHA512->isChecked())
        ui->actionSHA512->setChecked(true);
}

void Hash::sha3_224()
{
    if(ui->actionSHA3_224->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha3_224");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha3_224");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(true);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha3_224");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha3_224 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha3_224"));
        delete k;
    }

    if(!ui->actionSHA3_224->isChecked())
        ui->actionSHA3_224->setChecked(true);
}

void Hash::sha3_256()
{
    if(ui->actionSHA3_256->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha3_256");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha3_256");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(true);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha3_256");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha3_256 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha3_256"));
        delete k;
    }

    if(!ui->actionSHA3_256->isChecked())
        ui->actionSHA3_256->setChecked(true);
}

void Hash::sha3_384()
{
    if(ui->actionSHA3_384->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha3_384");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha3_384");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(true);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha3_384");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha3_384 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha3_384"));
        delete k;
    }

    if(!ui->actionSHA3_384->isChecked())
        ui->actionSHA3_384->setChecked(true);
}

void Hash::sha3_512()
{
    if(ui->actionSHA3_512->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "sha3_512");
        ui->statusBar->showMessage(tr("Algorithm: ") + "sha3_512");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(true);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "sha3_512");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "sha3_512 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm sha3_512"));
        delete k;
    }

    if(!ui->actionSHA3_512->isChecked())
        ui->actionSHA3_512->setChecked(true);
}

void Hash::keccak_224()
{
    if(ui->actionKeccak_224->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "keccak_224");
        ui->statusBar->showMessage(tr("Algorithm: ") + "keccak_224");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(true);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "keccak_224");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "keccak_224 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm keccak_224"));
        delete k;
    }

    if(!ui->actionKeccak_224->isChecked()) {
        ui->actionKeccak_224->setChecked(true);
        // QMessageBox::information(this,"TEST","Hej");
    }
}

void Hash::keccak_256()
{
    if(ui->actionKeccak_256->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "keccak_256");
        ui->statusBar->showMessage(tr("Algorithm: ") + "keccak_256");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(true);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "keccak_256");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "keccak_256 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm keccak_256"));
        delete k;
    }

    if(!ui->actionKeccak_256->isChecked())
        ui->actionKeccak_256->setChecked(true);
}

void Hash::keccak_384()
{
    if(ui->actionKeccak_384->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "keccak_384");
        ui->statusBar->showMessage(tr("Algorithm: ") + "keccak_384");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(true);
        ui->actionKeccak_512->setChecked(false);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "keccak_384");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "keccak_384 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm keccak_384"));
        delete k;
    }

    if(!ui->actionKeccak_384->isChecked())
        ui->actionKeccak_384->setChecked(true);
}

void Hash::keccak_512()
{
    if(ui->actionKeccak_512->isChecked()) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("algorithm", "keccak_512");
        ui->statusBar->showMessage(tr("Algorithm: ") + "keccak_512");
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(true);
        std::string s = k->getConf("defaultfilename");

        if(algoritmer.contains(QString::fromStdString(s))) {
            k->setConf("defaultfilename", "keccak_512");
            ui->actionDefaultFileName->setText(tr("Default file name: ") + "keccak_512 " + tr("(Click to change)"));
        }

        tesp(tr("I use the algorithm keccak_512"));
        delete k;
    }

    if(!ui->actionKeccak_512->isChecked())
        ui->actionKeccak_512->setChecked(true);
}

void Hash::checksum()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    std::string algorithm = k->getConf("algorithm");
    QString qsalgorithm = QString::fromStdString(algorithm);
    QString qs = QString::fromStdString(s);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs;
    else
        sokvag = QDir::homePath();

    tesp(tr("Open a file to calculate the hash sum"));
    QFileDialog *dialog = new QFileDialog(this, tr("Open a file to calculate the hash sum"),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;

    if(dialog->exec()) {
        ui->actionCopyPath->setDisabled(true);
        ui->actionSaveToFile->setDisabled(true);
        ui->actionCopyHashSum->setDisabled(true);
        ui->progressLabel->clear();
        //ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir;
        dir = dialog->directory();
        QString directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        fileNames = dialog->selectedFiles();
    } else {
        delete k;
        return;
    }

    delete k;
    bool visa = ui->actionAlwaysOpenTheDisplayWindow->isChecked();

    if((fileNames.size() > 1) || visa) {
        lista = new QStringList;
        QStringList fillista;

        for(int i = 0; i < fileNames.size(); i++) {
            bool lyckades = doChecksum(fileNames.at(i), lista);

            if(!lyckades) {
                tesp(tr("No hash sum could be calculated"));
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
            }

            fillista.append(fileNames.at(i));
            recentFileList(fileNames.at(i));
        }
    } else {
        bool lyckades = doChecksum(fileNames[0]);

        if(!lyckades) {
            tesp(tr("No hash sum could be calculated."));
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
        } else {
            recentFileList(fileNames.at(0));
            tesp(tr("hash sum has been calculated."));
        }
    }

    if((fileNames.size() > 1) || visa) {
        tesp(tr("hash sums have been calculated."));
        d = new QDialog;
        ui2 = new Ui::Lic;
        //Font ui2
        int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        // SLUT Font ui2
        ui2->setupUi(d);
        ui2->textBrowser->setFont(f);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        int hitta = fileNames.at(0).lastIndexOf(QDir::toNativeSeparators("/"));
        QDir dir;
        dir = dialog->directory();
        QString spath = dir.canonicalPath();
        // QString spath = QDir::toNativeSeparators(fileNames.at(0));
        // spath.remove(hitta, spath.size());
        ui2->textBrowser->append(tr("Path: ") + "\"" + QDir::toNativeSeparators(spath) + "\"");
        ui2->textBrowser->append(tr("Algorithm: ") + qsalgorithm);

        for(int i = 0; i < lista->size(); i++) {
            spath = fileNames.at(i);
            hitta = spath.lastIndexOf("/");
            spath.remove(0, hitta + 1);
            ui2->textBrowser->append(lista->at(i) + " " + QDir::toNativeSeparators(spath));
        }

        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        d->show();
        connect(ui2->pbOpenFile, SIGNAL(pressed()), this, SLOT(save()));
    }
}

void Hash::compare()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionSaveToFile->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qs = QString::fromStdString(s);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs;
    else
        sokvag = QDir::homePath();

    tesp(tr("Open the first file"));
    QFileDialog *dialog = new QFileDialog(this, tr("Open two files to compare their hash sums."),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;

    if(dialog->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir;
        dir = dialog->directory();
        QString directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        delete k;
        fileNames = dialog->selectedFiles();

        if(fileNames.size() > 2) {
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("You must select exactly two files.\nTwo files in this folder or one file in this folder and a second file in another folder of your choice."));
            tesp(tr("You must select exactly two files. Two files in this folder or one file in this folder and a second file in another folder of your choice."));
            return;
        }

        if(fileNames.size() == 1) {
            QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
            k = new Config(conf_file_name);
            s = k->getConf("open_path");
            std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
            qs = QString::fromStdString(s);
            // QFileInfo info2(qs);

            if(info.isReadable() && allwaysopenhomedirectory == "0")
                sokvag = qs;
            else
                sokvag = QDir::homePath();

            tesp(tr("Open the second file to compare with."));
            QFileDialog *dialog2 = new QFileDialog(this, tr("Open the second file to compare with."),
                                                   sokvag,
                                                   tr("All Files (*)"));

            if(dialogruta == "0")
                dialog2->setOption(QFileDialog::DontUseNativeDialog, true);
            else if(dialogruta == "1")
                dialog2->setOption(QFileDialog::DontUseNativeDialog, false);

            dialog2->setAcceptMode(QFileDialog::AcceptOpen);
            dialog2->setFileMode(QFileDialog::ExistingFiles);
            QStringList fileNames2;

            if(dialog2->exec()) {
                fileNames2 = dialog2->selectedFiles();

                if(fileNames2.size() != 1) {
                    tesp(tr("You must select exactly one file."));
                    QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("You must select exactly one file."));
                    return;
                }

                fileNames.push_back(fileNames2[0]);
                dir = dialog2->directory();
                QString directoryName2 = dir.path();
                k->setConf("open_path", directoryName2.toStdString());
                delete k;

                if(directoryName2 + QDir::toNativeSeparators("/") + fileNames2[0] == directoryName + QDir::toNativeSeparators("/") + fileNames[0]) {
                    tesp(tr("You have compared the same file with itself."));
                    QMessageBox::information(this, APPLICATION_NAME " " VERSION, tr("You have compared the same file with itself."));
                }
            } else {
                delete k;
                return;
            }
        }
    } else {
        delete k;
        return;
    }

    bool lyckades = doCompare(fileNames[0], fileNames[1]);

    if(!lyckades) {
        tesp(tr("The files could not be compared."));
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("The files could not be compared."));
    }
}

// LISTS Rekursivt
void Hash::checkSumLists()
{
    ui->actionCopyPath->setDisabled(true);
    ui->actionSaveToFile->setDisabled(true);
    ui->actionCopyHashSum->setDisabled(true);
    hurmangaFoldrar = 0;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qs = QString::fromStdString(s);
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs + QDir::toNativeSeparators("/");
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Select a folder to recursively generate hash sum lists"),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setFileMode(QFileDialog::Directory);
    dialog->setOption(QFileDialog::ShowDirsOnly);
    dialog->setReadOnly(true);
    QString directoryName;

    if(dialog->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir = dialog->directory();
        directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        delete k;
    } else {
        delete k;
        return;
    }

    QString tmp = "";
    bool lyckats = rekursivt(directoryName, tmp);

    if(!lyckats) {
        tesp(tr("Could not save a file to store hash sums in. Check your file permissions."));
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions."));
    }
}

void Hash::english()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("language");

    if(s != "en_US") {
        tesp(tr("Please restart hashsum"));

        switch(QMessageBox::information(this, APPLICATION_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                k->setConf("language", "en_US");
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
                //  auto *CommProcess = new QProcess(this);
                QProcess::startDetached(EXECUTE);
                //delete CommProcess;
                close();
        }
    }

    delete k;
}

void Hash::swedish()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("language");

    if(s != "sv_SE") {
        tesp("Please restart hashsum");

        switch(QMessageBox::information(this, APPLICATION_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                k->setConf("language", "sv_SE");
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
//                auto *CommProcess = new QProcess(this);
//                CommProcess->startDetached(EXECUTE);
//                delete CommProcess;
                QProcess::startDetached(EXECUTE);
                close();
        }
    }

    delete k;
}

void Hash::greek()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("language");

    if(s != "el_GR") {
        tesp("Please restart hashsum");

        switch(QMessageBox::information(this, APPLICATION_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                k->setConf("language", "el_GR");
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
//                auto *CommProcess = new QProcess(this);
//                CommProcess->startDetached(EXECUTE);
//                delete CommProcess;
                QProcess::startDetached(EXECUTE);
                close();
        }
    }

    delete k;
}

void Hash::german()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("language");

    if(s != "de_DE") {
        tesp("Please restart hashsum");

        switch(QMessageBox::information(this, APPLICATION_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                k->setConf("language", "de_DE");
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
                //  auto *CommProcess = new QProcess(this);
                QProcess::startDetached(EXECUTE);
                //  delete CommProcess;
                close();
        }
    }

    delete k;
}

void Hash::saveChecksumListWhereFilesAre()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);

    if(ui->actionSaveChecksumListWhereFilesAre->isChecked())
        k->setConf("saveChecksumListWhereFilesAre", "1");
    else
        k->setConf("saveChecksumListWhereFilesAre", "0");

    delete k;
}

void Hash::defaultFileName()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string tx = k->getConf("defaultfilename");
    QString qtx = QString::fromStdString(tx);
    bool ok;
    QString text = QInputDialog::getText(this, APPLICATION_NAME " " VERSION,
                                         tr("Default file name:"), QLineEdit::Normal,
                                         qtx, &ok);

    if(ok && !text.isEmpty()) {
        ui->actionDefaultFileName->setText(tr("Default file name: ") + text + tr(" (Click to change)"));
        tx = text.toStdString();
        k->setConf("defaultfilename", tx);
    }

    if(ok && text.isEmpty()) {
        std::string sa = k->getConf("algorithm");
        ui->actionDefaultFileName->setText(tr("Default file name: ") + QString::fromStdString(sa) + tr(" (Click to change)"));
        k->setConf("defaultfilename", sa);
    }

    delete k;
}

// private:
void Hash::checkSumList(int typ)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string s = k->getConf("open_path");
    std::string allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qs = QString::fromStdString(s);
    std::string algoritm = k->getConf("algorithm");
    QFileInfo info(qs);

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag = qs;
    else
        sokvag = QDir::homePath();

    QFileDialog *dialog = new QFileDialog(this, tr("Open Files to Create a List of Hash Sums"),
                                          sokvag,
                                          tr("All Files (*)"));

    if(dialogruta == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    dialog->setReadOnly(true);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;

    if(dialog->exec()) {
        ui->progressLabel->clear();
        // ui->progressLabel->toolTip().clear();
        ui->lineEdit->clear();
        QDir dir;
        dir = dialog->directory();
        QString directoryName = dir.path();
        k->setConf("open_path", directoryName.toStdString());
        k->setConf("save_path", directoryName.toStdString());
        fileNames = dialog->selectedFiles();
    } else {
        delete k;
        return;
    }

    s = k->getConf("save_path");
    allwaysopenhomedirectory = k->getConf("allwaysopenhomedirectory");
    QString qs2 = QString::fromStdString(s);
    QString sokvag2;

    if(info.isReadable() && allwaysopenhomedirectory == "0")
        sokvag2 = qs2;
    else
        sokvag2 = QDir::homePath();

    std::string savewherefilesare = k->getConf("saveChecksumListWhereFilesAre");
    delete k;
    ui->progressLabel->clear();
    //ui->progressLabel->toolTip().clear();
    ui->lineEdit->clear();

    if(typ == 0) {
        if(savewherefilesare == "0") {
            static QStringList allFilenames;
            allFilenames += fileNames;

            switch(QMessageBox::question(this, APPLICATION_NAME " " VERSION,
                                         tr("Did you select all files you want to choose?"),
                                         tr("Yes"),
                                         tr("No"), nullptr, 2)) {
                case 0:
                    askForFileName(sokvag2, algoritm, allFilenames);
                    allFilenames.clear();
                    break;

                case 1:
                    checkSumList(0);
            }
        }

        if(savewherefilesare == "1")
            dontAskForFileName(algoritm, fileNames);
    }

    if(typ == 1) {
        static QStringList allFilenames;
        allFilenames += fileNames;

        switch(QMessageBox::question(this, APPLICATION_NAME " " VERSION,
                                     tr("Did you select all files you want to choose?"),
                                     tr("Yes"),
                                     tr("No"), nullptr, 2)) {
            case 0:
                askForFileNameCheck(sokvag2, algoritm, allFilenames);
                allFilenames.clear();
                break;

            case 1:
                checkSumList(1);
        }
    }

    if(typ == 2) {
        static QStringList allFilenames;
        allFilenames += fileNames;

        switch(QMessageBox::question(this, APPLICATION_NAME " " VERSION,
                                     tr("Did you select all files you want to choose?"),
                                     tr("Yes"),
                                     tr("No"), nullptr, 2)) {
            case 0:
                createDeb(sokvag2, allFilenames);
                allFilenames.clear();
                break;

            case 1:
                checkSumList(2);
        }
    }
}

bool Hash::createDeb(const QString & sokvag2, QStringList fileNames)
{
    const QString &conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string dialogruta = k->getConf("usenativedialogs");
    delete k;
    QString tillbaka;
    QString filTyp;
    filTyp = tr("Files (*.*)");
    QFileDialog *dialogSave = new QFileDialog(this, tr("Save the Debian md5sums file"),
        sokvag2,
        filTyp);

    if(dialogruta == "0")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, false);

    dialogSave->setAcceptMode(QFileDialog::AcceptSave);
    dialogSave->setFileMode(QFileDialog::AnyFile);
    dialogSave->selectFile("md5sums");
    QString filnamnet;
    bool lyckades;
    int antal = 0;

    if(dialogSave->exec()) {
        QStringList tempfilList = dialogSave->selectedFiles();
        const QString tempfil = tempfilList[0];
        std::string algo = "md5";
        int pos = (-1);
        QFile file(tempfil);
        QTextStream out(&file);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            delete k;
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions.") + file.errorString());
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            return false;
        }

        for(int i = 0; i < fileNames.count(); i++) {
            pos = fileNames[i].lastIndexOf("/usr/");

            if(pos < 0)
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("This is not a valid path to build a debian package!"));

            filnamnet = fileNames[i].mid(pos + 1, -5);
            lyckades = doChecksumList(fileNames[i], tillbaka, algo);

            if(!lyckades)
                out << filnamnet << tr(" No hash sum could be calculated") << "\n";
            else {
                out << tillbaka << " " << QDir::toNativeSeparators(filnamnet) << "\n";
                antal++;
            }
        }

        file.close();
    }

    return true;
}

bool Hash::rekursivt(const QDir & path, QString indent)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string fil = k->getConf("defaultfilename");
    QString defaultfn = QString::fromStdString(fil);
    std::string algoritm = k->getConf("algorithm");
    QString alg = QString::fromStdString(algoritm);
    std::string writedatetime = k->getConf("writeDateTimeHashSumFile");
    std::string open_path = k->getConf("open_path");
    QString qopen_path = QString::fromStdString(open_path);
    delete k;
    indent += "\t";
    bool lyckades;
    QFileInfoList list = path.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

    foreach(QFileInfo finfo, list) {
        if(finfo.isDir()) {
            rekursivt(QDir(finfo.absoluteFilePath()), indent);
            hurmangaFoldrar++;
        } else {
            lyckades = rekursivtPrint(defaultfn, alg, path, writedatetime);

            if(!lyckades)
                return false;
        }
    }

    QString tmp = "";
    QApplication::processEvents();

    if(hurmangaFoldrar < 2) {
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm " + alg);
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
    } else {
//        tesp("I have been looking into " + tmp.setNum(hurmangaFoldrar + 1) + " folder for files. I have managed to calculate the hash sum in all folders containing files. Algorithm " + alg);
        ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ") ...");
    }

    ui->statusBar->showMessage(tr("Path: ") + "\"" + QDir::toNativeSeparators(path.path()) + "\"");

    if(QDir::toNativeSeparators(path.path()) == QDir::toNativeSeparators(qopen_path)) {
        if(hurmangaFoldrar < 2) {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folder for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
        } else {
            ui->progressLabel->setText(tr("I have been looking into ") + tmp.setNum(hurmangaFoldrar + 1) + tr(" folders for files. I have managed to calculate the hash sum in all folders containing files.\n") + tr("(Algorithm: ") + alg + ")");
        }

        tesp(tr("I have checked all folders and created all the files! Mission accomplished."));
        ui->statusBar->showMessage(tr("I have checked all folders and created all the files! Mission accomplished."));
        QString s;
        s.setNum(hurmangaFiler);
        ui->lineEdit->setText(tr("Mission accomplished! I have calculated the hash sum of ") + s + tr(" files."));
        hurmangaFiler = 0;
    }

    return true;
}

bool Hash::rekursivtDeb(const QDir & path, QString & indent, QStringList & allt)
{
    QString stringPath = path.path();
    int pos = stringPath.lastIndexOf("/usr");

    if(pos < 0) {
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("This is not a valid path to build a Debian package!"));
        indent.clear();
        allt.clear();
        return false;
    }

    indent += "\t";
    bool lyckades;
    QFileInfoList list = path.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

    foreach(QFileInfo finfo, list) {
        if(finfo.isDir())
            rekursivtDeb(QDir(finfo.absoluteFilePath()), indent, allt);
        else {
            lyckades = rekursivtPrintDeb(path, allt);

            if(!lyckades) {
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
                return false;
            }
        }
    }

    QApplication::processEvents();
    return true;
}

bool Hash::rekursivtPrintDeb(QDir path, QStringList & allt)
{
    QString dirpath = path.canonicalPath();
    QString tillbaka;
    bool lyckades;
    path.setFilter(QDir::Files);
    path.setSorting(QDir::Name);
    QString stringPath = path.path();
    int pos = stringPath.lastIndexOf("/usr)");
    QStringList fileNames = path.entryList();

    for(int i = 0; i < fileNames.count(); i++) {
        lyckades = doChecksumList(QDir::toNativeSeparators(dirpath  + "/") + fileNames[i], tillbaka, "md5");

        if(!lyckades)
            return false;

        allt.push_back(tillbaka + " " + stringPath.mid(pos, -1) + "/" + fileNames[i]);
    }

    return true;
}

bool Hash::rekursivtPrint(const QString & defaultfn, const QString & alg, QDir path, const std::string & writedatetime)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string df = k->getConf("defaultfilename");
    std::string fullpath = k->getConf("fullpath");
    delete k;
    QString qdf = qdf.fromStdString(df);
    QString dirpath = path.canonicalPath();
    QString tillbaka;
    bool lyckades;
    int antal = 0;
    hurmangaFiler++;
    path.setFilter(QDir::Files);
    path.setSorting(QDir::Name);
    QString stringPath = path.path();
    const QString tempfil = QDir::toNativeSeparators(stringPath  + "/") + defaultfn;
    QStringList fileNames = path.entryList();
    QStringList result;

    foreach(const QString &str, fileNames)
        result += str;

    fileNames = result;
    std::string algoritm = alg.toStdString();
    int antalfiler = fileNames.count();
// text file
    QFile file(tempfil);
    QTextStream out(&file);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        delete k;
        return false;
    }

    out << "# " << tr("Algorithm: ") << alg << "\n";

    if(fullpath == "1") {
        out << "# " << tr("Files in:") << "\n";
        out << "# " << dirpath << "\n";
    }

    for(int i = 0; i < fileNames.count(); i++) {
        if(fileNames[i] != defaultfn) {
            lyckades = doChecksumList(QDir::toNativeSeparators(stringPath  + "/") + fileNames[i], tillbaka, algoritm);

            if(!lyckades)
                out << fileNames[i] << tr(" No hash sum could be calculated") << "\n";
            else {
                out << tillbaka << " " << fileNames[i] << "\n";
                antal++;
            }
        } else
            antalfiler--;
    }

    tesp(tr("Hash sum for ") + antal + tr(" of ") + antalfiler + tr(" files was successfully calculated"));
    out << "# " << tr("Hash sum for ") << antal << tr(" of ") << antalfiler << tr(" files was successfully calculated") << "\n";
    out << "# " << APPLICATION_NAME << " " << VERSION << " " << tr("Copyright ") << COPYRIGHT << "\n";

    if(writedatetime == "1")
        out << "# " << tr("Created: ") << datumtid() << "\n";

    antal = 0;
    file.close();
    return true;
}

QString Hash::datumtid()
{
    QDateTime *dt = new QDateTime(QDateTime::currentDateTime());
    QString qdt = dt->toString(Qt::ISODate);
    return qdt;
}

bool Hash::askForFileNameCheck(const QString & sokvag2, const std::string & algoritm, QStringList fileNames)
{
    QString tillbaka;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string defaultfil = k->getConf("defaultfilename");
    std::string dialogruta = k->getConf("usenativedialogs");
    QString qdefaultfil = QString::fromStdString(defaultfil);
    std::string writedatetime = k->getConf("writeDateTimeHashSumFile");
    delete k;
    QFileDialog *dialogSave = new QFileDialog(this, tr("Open the old hash sum file to compare with"),
        sokvag2,
        tr("Text Files (*)"));

    if(dialogruta == "0")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, false);

    dialogSave->setAcceptMode(QFileDialog::AcceptOpen);
    dialogSave->setFileMode(QFileDialog::AnyFile);
    QString filnamnet;
    bool lyckades;
    int antal = 0;
    QStringList fileNamesOld, fileNamesNew, fileNamesDeleted, fileNamesTillkomna, fileNamesChanged, fileNamesOforandrade;
    dialogSave->selectFile(qdefaultfil);

    if(dialogSave->exec()) {
        QStringList tempfilList = dialogSave->selectedFiles();
        const QString tempfil = tempfilList[0];
        int pos;

        for(int i = 0; i < fileNames.count(); i++) {
            filnamnet = fileNames[i];
            lyckades = doChecksumList(fileNames[i], tillbaka, algoritm);

            if(!lyckades)
                fileNamesNew.push_back(filnamnet + " " + tr("Hash sum could not be estimated"));
            else {
                fileNamesNew.push_back(tillbaka + " " + filnamnet);
                antal++;
            }
        }

        QFile oldFil(tempfil);

        if(!oldFil.open(QIODevice::ReadOnly)) {
            tesp(tr("Could not open ") + qdefaultfil + tr(" with hash sums. Make sure the file exists and check your file permissions."));
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not open \"") + qdefaultfil + tr("\" with hash sums.\nMake sure the file exists and check your file permissions."));
            return false;
        }

        QTextStream stream(&oldFil);
        QString content;
        content = stream.readAll();
        oldFil.close();
        QString t;
        int fora = 0;

        for(int i = 0; i < content.size(); i++) {
            pos = content.indexOf('/');

            if(pos < 0) {
                tesp(tr("The old hash sum file contains no path to the files. You can not compare!"));
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("The old hash sum file contains no path to the files. You can not compare!"));
                return false;
            }

            if(content[i] == '\n') {
                t =  content.mid(fora, i - fora);
                fora = i + 1;

                if(t[0] != '#' && t.mid(0, 8) != "Created:" && t.mid(0, 8) != "Hash sum" && t.mid(0, 7) != "hashSum")
                    fileNamesOld.push_back(t);
            }
        }

        // obsolete
        // qSort(fileNamesOld);
        // qSort(fileNamesNew);
        fileNamesOld.sort();
        fileNamesNew.sort();
        QString no, nn, so, sn;
        int pos1;
        QList<QString>::iterator itOld;
        QList<QString>::iterator itNew;

// OFÖRÄNDRADE FILER
        for(itOld = fileNamesOld.begin(); itOld != fileNamesOld.end(); itOld++) {
            for(itNew = fileNamesNew.begin(); itNew != fileNamesNew.end(); itNew++) {
                no = *itOld;
                nn = *itNew;
                no = no.simplified();
                nn = nn.simplified();

                if(no == nn) {
                    pos1 = no.indexOf(' ');
                    no = no.mid(pos1, -1);
                    no = no.simplified();
                    fileNamesOforandrade.push_back(no);
                }
            }
        }

        for(itOld = fileNamesOld.begin(); itOld != fileNamesOld.end(); itOld++) {
            pos1 = itOld->indexOf(' ');
            no = *itOld;
            no = no.mid(pos1, -1);
            no = no.simplified();
            so = itOld->mid(0, pos1);
            so = so.simplified();

            for(itNew = fileNamesNew.begin(); itNew != fileNamesNew.end(); itNew++) {
                pos1 = itNew->indexOf(' ');
                nn = itNew->mid(pos1, -1);
                nn = nn.simplified();
                sn = itNew->mid(0, pos1);
                sn = sn.simplified();

                if((no == nn) && (so != sn))
                    fileNamesChanged.push_back(nn);
            }
        }

// DELETADE FILER
        bool finnsej = true;

        for(itOld = fileNamesOld .begin(); itOld != fileNamesOld.end(); itOld++) {
            finnsej = true;
            pos1 = itOld->indexOf(' ');
            nn = itOld->mid(pos1, -1);
            nn = nn.simplified();

            for(itNew = fileNamesNew.begin(); itNew != fileNamesNew.end(); itNew++) {
                pos1 = itNew->indexOf(' ');
                no = itNew->mid(pos1, -1);
                no = no.simplified();

                if(no == nn)
                    finnsej = false;
            }

            if(finnsej)
                fileNamesDeleted.push_back(nn);
        }

// TILLKOMMNA FILER
        finnsej = true;

        for(itNew = fileNamesNew.begin(); itNew != fileNamesNew.end(); itNew++) {
            finnsej = true;
            pos1 = itNew->indexOf(' ');
            nn = itNew->mid(pos1, -1);
            nn = nn.simplified();

            for(itOld = fileNamesOld.begin(); itOld != fileNamesOld.end(); itOld++) {
                pos1 = itOld->indexOf(' ');
                no = itOld->mid(pos1, -1);
                no = no.simplified();

                if(no == nn)
                    finnsej = false;
            }

            if(finnsej)
                fileNamesTillkomna.push_back(nn);
        }

        int pos2 = tempfil.lastIndexOf('/');
        QString t1 = tempfil.mid(0, pos2);
        QString t2 = tempfil.mid(pos2 + 1, -1);
        QString tempfil2 = t1 + QDir::toNativeSeparators("/") + "check-" + t2;
        QFile fil3(tempfil2);
        QTextStream in(&fil3);

        if(!fil3.open(QIODevice::WriteOnly | QIODevice::Text)) {
            delete k;
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions."));
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + fil3.errorString());
            return false;
        }

        in << "# " << tr("Unchanged files") << "\n";

        if(fileNamesOforandrade.count() > 0) {
            for(int i = 0; i < fileNamesOforandrade.count(); i++)
                in << QDir::toNativeSeparators(fileNamesOforandrade.at(i)) << "\n";
        }

        in << "# " << tr("Changed files") << "\n";

        if(fileNamesChanged.count() > 0) {
            for(int i = 0; i < fileNamesChanged.count(); i++)
                in << QDir::toNativeSeparators(fileNamesChanged.at(i)) << "\n";
        }

        in << "# " << tr("Files not tested") << "\n";

        if(fileNamesDeleted.count() > 0) {
            for(int i = 0; i < fileNamesDeleted.count(); i++)
                in << fileNamesDeleted.at(i) << "\n";
        }

        in << "# " << tr("New files") << "\n";

        if(fileNamesTillkomna.count() > 0) {
            for(int i = 0; i < fileNamesTillkomna.count(); i++)
                in << QDir::toNativeSeparators(fileNamesTillkomna.at(i)) << "\n";
        }

        in << "# " << APPLICATION_NAME << " " << VERSION << " " << tr("Copyright ") << COPYRIGHT << "\n";

        if(writedatetime == "1")
            in << "# " << tr("Created: ") << datumtid() << "\n";

        fil3.close();
        QDir dir;
        dir = dialogSave->directory();
        QString directoryName = dir.path();
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("save_path", directoryName.toStdString());
        delete k;
        QString temp = "";
        QApplication::processEvents();
//        tesp(QDir::toNativeSeparators(tr("File: ") + tempfil2));
        tesp(tr("Hash sum for ") + temp.setNum(antal) + tr(" of ") + temp.setNum(fileNames.count()) + tr(" files was successfully calculated (Algorithm: ") + QString::fromStdString(algoritm) + tr("Hash Sum values have been compared to old values in the file ") +  qdefaultfil);
        ui->statusBar->showMessage(QDir::toNativeSeparators(tr("File: ") + tempfil2));
        ui->progressLabel->setText(tr("Hash sum for ") + temp.setNum(antal) + tr(" of ") + temp.setNum(fileNames.count()) + tr(" files was successfully calculated (Algorithm: ") + QString::fromStdString(algoritm) + ")\n" + tr("Hash Sum values have been compared to old values in the file") + " \"" + qdefaultfil + "\"");
        ui->progressLabel->setToolTip(QDir::toNativeSeparators(tempfil2));
        ui->actionCopyPath->setEnabled(true);
        ui->actionSaveToFile->setEnabled(true);
    }

    return true;
}

bool Hash::askForFileName(const QString & sokvag2, const std::string & algoritm, QStringList fileNames)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string defaultfil = k->getConf("defaultfilename");
    std::string writedatetime = k->getConf("writeDateTimeHashSumFile");
    std::string dialogruta = k->getConf("usenativedialogs");
    std::string fullpath = k->getConf("fullpath");
    delete k;
    QString tillbaka;
    QString filTyp;
    filTyp = tr("Text Files (*)");
    QFileDialog *dialogSave = new QFileDialog(this, tr("Save the file with the hash sums"),
        sokvag2,
        filTyp);

    if(dialogruta == "0")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(dialogruta == "1")
        dialogSave->setOption(QFileDialog::DontUseNativeDialog, false);

    dialogSave->setAcceptMode(QFileDialog::AcceptSave);
    dialogSave->setFileMode(QFileDialog::AnyFile);
    dialogSave->selectFile(QString::fromStdString(defaultfil));
    int pos;
    QString filnamnet;
    bool lyckades;
    int antal = 0;

    if(dialogSave->exec()) {
        QStringList tempfilList = dialogSave->selectedFiles();
        const QString tempfil = tempfilList[0];
        QFile file(tempfil);
        QTextStream out(&file);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            delete k;
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions.") + file.errorString());
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            return false;
        }

        out << "# " << tr("Algorithm: ") << QString::fromStdString(algoritm) << "\n";

        for(int i = 0; i < fileNames.count(); i++) {
            if(fullpath == "0") {
                pos = fileNames[i].lastIndexOf(QDir::toNativeSeparators("/"));
                filnamnet = fileNames[i].mid(pos + 1, -1);
            } else
                filnamnet = fileNames[i];

            lyckades = doChecksumList(fileNames[i], tillbaka, algoritm);

            if(!lyckades)
                out << filnamnet << tr(" No hash sum could be calculated") << "\n";
            else {
                out << tillbaka << " " << filnamnet << "\n";
                antal++;
            }
        }

        out << "# " << tr("Hash sum for ") << antal << tr(" of ") << fileNames.count() << tr(" files was successfully calculated") << "\n";
        out << "# " << APPLICATION_NAME << " " << VERSION << " " << tr("Copyright ") << COPYRIGHT << "\n";

        if(writedatetime == "1")
            out << "# " << tr("Created: ") << datumtid() << "\n";

        file.close();
        QDir dir;
        dir = dialogSave->directory();
        QString directoryName = dir.path();
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        k->setConf("save_path", directoryName.toStdString());
        delete k;
        QString temp = "";
        QApplication::processEvents();
        ui->statusBar->showMessage(QDir::toNativeSeparators(tr("File: ") + tempfil));
        tesp(tr("Hash sum for ") + temp.setNum(antal) + tr(" of ") + temp.setNum(fileNames.count()) + tr(" files was successfully calculated. Algorithm: ") + QString::fromStdString(algoritm));
        ui->progressLabel->setText(tr("Hash sum for ") + temp.setNum(antal) + tr(" of ") + temp.setNum(fileNames.count()) + tr(" files was successfully calculated (Algorithm: ") + QString::fromStdString(algoritm) + ")");
        ui->progressLabel->setToolTip(QDir::toNativeSeparators(tempfil));
        ui->actionCopyPath->setEnabled(true);
        ui->actionSaveToFile->setEnabled(true);
    }

    return true;
}

bool Hash::dontAskForFileName(const std::string & algoritm, QStringList fileNames)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string path = k->getConf("open_path");
    std::string fil = k->getConf("defaultfilename");
    std::string writedatetime = k->getConf("writeDateTimeHashSumFile");
    std::string fullpath = k->getConf("fullpath");
    delete k;
    QString tillbaka;
    int pos;
    QString filnamnet;
    bool lyckades;
    int antal = 0;
    const QString tempfil = QDir::toNativeSeparators(QString::fromStdString(path + '/') + QString::fromStdString(fil));
    QFile file(tempfil);
    QTextStream out(&file);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        delete k;
        tesp(tr("Could not save a file to store hash sums in. Check your file permissions.") + file.errorString());
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
        return false;
    }

    out << "# " << tr("Algorithm: ") << QString::fromStdString(algoritm) << "\n";

    for(int i = 0; i < fileNames.count(); i++) {
        if(fullpath == "0") {
            pos = fileNames[i].lastIndexOf("/");
            filnamnet = fileNames[i].mid(pos + 1, -1);
        } else
            filnamnet = fileNames[i];

        lyckades = doChecksumList(fileNames[i], tillbaka, algoritm);

        if(!lyckades)
            out << filnamnet << tr(" No hash sum could be calculated") << "\n";
        else {
            out << tillbaka << " " << filnamnet << "\n";
            antal++;
        }
    }

    out << "# " << tr("Hash sum for ") << antal << tr(" of ") << fileNames.count() << tr(" files was successfully calculated") << "\n";
    out << "# " << APPLICATION_NAME << " " << VERSION << " " << tr("Copyright ") << COPYRIGHT << "\n";

    if(writedatetime == "1")
        out << "# " << tr("Created: ") << datumtid() << "\n";

    file.close();
    QString temp;
    ui->statusBar->showMessage(QDir::toNativeSeparators(tr("File: ") + tempfil));
    ui->progressLabel->setText(tr("Hash sum for ") + temp.setNum(antal) + tr(" of ") + temp.setNum(fileNames.count()) + tr(" files was successfully calculated (Algorithm: ") + QString::fromStdString(algoritm) + ")");
    ui->progressLabel->setToolTip(QDir::toNativeSeparators(tempfil));
    ui->actionCopyPath->setEnabled(true);
    ui->actionSaveToFile->setEnabled(true);
    return true;
}

bool Hash::doChecksum(const QString & in)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("algorithm");
    delete k;
    QByteArray ba;
    QFile f(in.toUtf8());

    if(f.open(QFile::ReadOnly)) {
        ba = f.readAll();
        f.close();
    } else
        return false;

    int pos = in.lastIndexOf("/");
    QString filnamnet = in.mid(pos + 1, -1);
    QByteArray sum;

    if(s == "md4")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md4);
    else if(s == "md5")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md5);
    else if(s == "sha1")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha1);
    else if(s == "sha224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha224);
    else if(s == "sha256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha256);
    else if(s == "sha384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha384);
    else if(s == "sha512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha512);
    else if(s == "sha3_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_224);
    else if(s == "sha3_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_256);
    else if(s == "sha3_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_384);
    else if(s == "sha3_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_512);
    else if(s == "keccak_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_224);
    else if(s == "keccak_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_256);
    else if(s == "keccak_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_384);
    else if(s == "keccak_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_512);

    sum = sum.toHex();
    ui->progressLabel->setText("\"" + filnamnet + tr("\" has hash sum") + " (" + tr("Algorithm: ") + QString::fromStdString(s) + ")");
    ui->progressLabel->setToolTip(QDir::toNativeSeparators(in));
    ui->actionCopyPath->setEnabled(true);
    ui->actionSaveToFile->setEnabled(true);
    ui->actionCopyHashSum->setEnabled(true);
    ui->lineEdit->setText(sum);
    return true;
}

bool Hash::doChecksum(const QString & in, QStringList * lista)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("algorithm");
    delete k;
    QByteArray ba;
    QFile f(in.toUtf8());

    if(f.open(QFile::ReadOnly)) {
        ba = f.readAll();
        f.close();
    } else
        return false;

    QByteArray sum;

    if(s == "md4")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md4);
    else if(s == "md5")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md5);
    else if(s == "sha1")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha1);
    else if(s == "sha224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha224);
    else if(s == "sha256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha256);
    else if(s == "sha384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha384);
    else if(s == "sha512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha512);
    else if(s == "sha3_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_224);
    else if(s == "sha3_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_256);
    else if(s == "sha3_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_384);
    else if(s == "sha3_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_512);
    else if(s == "keccak_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_224);
    else if(s == "keccak_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_256);
    else if(s == "keccak_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_384);
    else if(s == "keccak_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_512);

    sum = sum.toHex();
    lista->append(sum);
    return true;
}

bool Hash::doChecksumText(const QString & in)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("algorithm");
    delete k;
    QByteArray sum;

    if(s == "md4")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Md4);
    else if(s == "md5")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Md5);
    else if(s == "sha1")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha1);
    else if(s == "sha224")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha224);
    else if(s == "sha256")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha256);
    else if(s == "sha384")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha384);
    else if(s == "sha512")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha512);
    else if(s == "sha3_224")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha3_224);
    else if(s == "sha3_256")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha3_256);
    else if(s == "sha3_384")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha3_384);
    else if(s == "sha3_512")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Sha3_512);
    else if(s == "keccak_224")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Keccak_224);
    else if(s == "keccak_256")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Keccak_256);
    else if(s == "keccak_384")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Keccak_384);
    else if(s == "keccak_512")
        sum = QCryptographicHash::hash(in.toUtf8(), QCryptographicHash::Keccak_512);

    sum = sum.toHex();
    ui->actionCopyHashSum->setEnabled(true);
    ui->lineEdit->setText(sum);
    return true;
}

bool Hash::doChecksumList(const QString & in, QString & ut, const std::string & s)
{
    QByteArray ba;
    QFile f(in.toUtf8());

    if(f.open(QFile::ReadOnly)) {
        ba = f.readAll();
        f.close();
    } else
        return false;

    QByteArray sum;

    if(s == "md4")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md4);
    else if(s == "md5")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Md5);
    else if(s == "sha1")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha1);
    else if(s == "sha224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha224);
    else if(s == "sha256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha256);
    else if(s == "sha384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha384);
    else if(s == "sha512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha512);
    else if(s == "sha3_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_224);
    else if(s == "sha3_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_256);

    if(s == "sha3_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_384);
    else if(s == "sha3_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Sha3_512);
    else if(s == "keccak_224")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_224);
    else if(s == "keccak_256")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_256);
    else if(s == "keccak_384")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_384);
    else if(s == "keccak_512")
        sum = QCryptographicHash::hash(ba, QCryptographicHash::Keccak_512);

    sum = sum.toHex();
    //ut = (QString) sum;
    ut = sum;
    return true;
}

bool Hash::doCompare(const QString & in1, const QString & in2)
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    std::string s = k->getConf("algorithm");
    delete k;
    QByteArray ba1, ba2;
    QFile f1(in1);

    if(f1.open(QFile::ReadOnly)) {
        ba1 = f1.readAll();
        f1.close();
    } else
        return false;

    QFile f2(in2);

    if(f2.open(QFile::ReadOnly)) {
        ba2 = f2.readAll();
        f2.close();
    } else
        return false;

    int pos = in1.lastIndexOf("/");
    QString filnamnet1 = in1.mid(pos + 1, -1);
    pos = in2.lastIndexOf("/");
    QString filnamnet2 = in2.mid(pos + 1, -1);
    QByteArray sum1, sum2;

    if(s == "md4") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Md4);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Md4);
    } else if(s == "md5") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Md5);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Md5);
    } else if(s == "sha1") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha1);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha1);
    } else if(s == "sha224") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha224);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha224);
    } else if(s == "sha256") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha256);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha256);
    } else if(s == "sha384") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha384);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha384);
    } else  if(s == "sha512") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha512);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha512);
    } else  if(s == "sha3_224") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha3_224);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha3_224);
    } else if(s == "sha3_256") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha3_256);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha3_256);
    } else  if(s == "sha3_384") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha3_384);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha3_384);
    } else if(s == "sha3_512") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Sha3_512);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Sha3_512);
    } else if(s == "keccak_224") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Keccak_224);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Keccak_224);
    } else if(s == "keccak_256") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Keccak_256);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Keccak_256);
    } else if(s == "keccak_384") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Keccak_384);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Keccak_384);
    } else if(s == "keccak_512") {
        sum1 = QCryptographicHash::hash(ba1, QCryptographicHash::Keccak_512);
        sum2 = QCryptographicHash::hash(ba2, QCryptographicHash::Keccak_512);
    }

    sum1 = sum1.toHex();
    sum2 = sum2.toHex();

    if(sum1 == sum2) {
        tesp(tr("The files are identical."));
        ui->progressLabel->setText("\"" + filnamnet1 + tr("\" and \"") + filnamnet2 + tr("\" have these hash sums:") + " (" + tr("Algorithm: ") + QString::fromStdString(s) + ") " + tr(". The files are identical."));
        ui->statusBar->showMessage(tr("The files are identical."));
    }

    if(sum1 != sum2) {
        tesp(tr("The files are NOT identical."));
        ui->progressLabel->setText("\"" + filnamnet1 + tr("\" and \"") + filnamnet2 + tr("\" have these hash sums:") + " (" + tr("Algorithm: ") + QString::fromStdString(s) + ") " + tr(". The files are NOT identical."));
        ui->statusBar->showMessage(tr("The files are NOT identical."));
    }

    ui->progressLabel->setToolTip(QDir::toNativeSeparators(in1 + tr(" and ") + in2));
    ui->actionCopyPath->setEnabled(true);
    ui->actionSaveToFile->setEnabled(true);
    ui->actionCopyHashSum->setEnabled(true);
    ui->lineEdit->setText(sum1 + tr(" and ") + sum2);
    return true;
}

bool Hash::recentFileList(QString lastOpenfile)
{
    lastOpenfile = QDir::toNativeSeparators(lastOpenfile);
    recentFiles.append(lastOpenfile);
    return true;
}

void Hash::setStartConfig()
{
    algoritmer = QStringList() << "md4" << "md5" << "sha1" << "sha224" << "sha256" << "sha384" << "sha512" << "sha3_224" << "sha3_256" << "sha3_384" << "sha3_512";
    ui->setupUi(this);
    this->setWindowTitle(APPLICATION_NAME " " VERSION);
    hurmangaFiler = 0;
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    const QRect screenrect = QApplication::desktop()->screenGeometry();
    int w = screenrect.width();
    int h = screenrect.height();
    std::stringstream ss;
    std::string s;
    std::string sxwidth = k->getConf("x_lage", "100");
    std::string sxheight = k->getConf("y_lage", "100");
    int isxwidth, isxheight;
    ss << sxwidth;
    ss >> isxwidth;
    ss.clear();
    ss << sxheight;
    ss >> isxheight;
    ss.clear();
    int x = 0, y = 0;

    if(isxwidth < w  && isxheight < h && isxwidth >= 0 && isxheight >= 0) {
        x = isxwidth;
        y = isxheight;
    } else {
        x = 100;
        y = 100;
    }

    // Höjd och bredd
    sxwidth = k->getConf("bredd", "900");
    sxheight = k->getConf("hojd", "200");
    int hojd = 0, bredd = 0;
    ss << sxwidth;
    ss >> bredd;
    ss.clear();
    ss << sxheight;
    ss >> hojd;
    ss.clear();
    // ipac
    s = k->getConf("applicationsmenushortcut", "0");

    if(s == "0")
        ui->actionApplicationsMenuShortcut->setChecked(false);
    else if(s == "1")
        ui->actionApplicationsMenuShortcut->setChecked(true);

    s = k->getConf("desktopshortcut", "0");

    if(s == "0")
        ui->actionDesktopShortcut->setChecked(false);
    else if(s == "1")
        ui->actionDesktopShortcut->setChecked(true);

    // ---
    s = k->getConf("allwaysopenhomedirectory", "0");

    if(s == "0")
        ui->actionAllwaysOpenHomeDirectory->setChecked(false);
    else if(s == "1")
        ui->actionAllwaysOpenHomeDirectory->setChecked(true);

    s = k->getConf("fullpath", "1");

    if(s == "0")
        ui->actionShowFullPathInHashSumFile->setChecked(false);
    else if(s == "1")
        ui->actionShowFullPathInHashSumFile->setChecked(true);

    std::string lang = k->getConf("language");
    s = k->getConf("checkonstart", "1");

    if(s == "0")
        ui->actionCheckOnStart->setChecked(false);
    else if(s == "1") {
        ui->actionCheckOnStart->setChecked(true);
        auto *cu = new CheckForUpdates;
        s = k->getConf("silent");

        if(s == "true") {
            bool *sant = new bool(true);
            cu->checkOnStart(APPLICATION_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH, sant);
        } else {
            bool *falskt = new bool(false);
            cu->checkOnStart(APPLICATION_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH, falskt);
        }
    }

    s = k->getConf("saveChecksumListWhereFilesAre", "0");

    if(s == "0")
        ui->actionSaveChecksumListWhereFilesAre->setChecked(false);
    else if(s == "1")
        ui->actionSaveChecksumListWhereFilesAre->setChecked(true);

    s = k->getConf("writeDateTimeHashSumFile", "1");

    if(s == "0")
        ui->actionWriteTimeDateInHashSumFile->setChecked(false);
    else if(s == "1")
        ui->actionWriteTimeDateInHashSumFile->setChecked(true);

    std::string algrtm = k->getConf("algorithm", "md5");

    if(algrtm == "md4") {
        ui->actionMD4->setChecked(true);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "md5") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(true);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha1") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(true);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha224") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(true);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha256") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(true);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha384") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(true);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha512") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(true);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha3_224") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(true);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha3_256") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(true);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha3_384") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(true);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "sha3_512") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(true);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "keccak_224") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(true);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "keccak_256") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(true);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "keccak_384") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(true);
        ui->actionKeccak_512->setChecked(false);
    } else if(algrtm == "keccak_512") {
        ui->actionMD4->setChecked(false);
        ui->actionMD5->setChecked(false);
        ui->actionSHA1->setChecked(false);
        ui->actionSHA224->setChecked(false);
        ui->actionSHA256->setChecked(false);
        ui->actionSHA384->setChecked(false);
        ui->actionSHA512->setChecked(false);
        ui->actionSHA3_224->setChecked(false);
        ui->actionSHA3_256->setChecked(false);
        ui->actionSHA3_384->setChecked(false);
        ui->actionSHA3_512->setChecked(false);
        ui->actionKeccak_224->setChecked(false);
        ui->actionKeccak_256->setChecked(false);
        ui->actionKeccak_384->setChecked(false);
        ui->actionKeccak_512->setChecked(true);
    }

    s = k->getConf("defaultfilename", algrtm);
    ui->actionDefaultFileName->setText(tr("Default file name: ") + QString::fromStdString(s) + tr(" (Click to change)"));
    ui->statusBar->showMessage(tr("Algorithm: ") + QString::fromStdString(algrtm));
    s = k->getConf("usenativedialogs", "0");

    if(s == "0")
        ui->actionUseNativeDialogs->setChecked(false);
    else if(s == "1")
        ui->actionUseNativeDialogs->setChecked(true);

    s = k->getConf("alwaysopenthedisplaywindow", "0");

    if(s == "0")
        ui->actionAlwaysOpenTheDisplayWindow->setChecked(false);
    else if(s == "1")
        ui->actionAlwaysOpenTheDisplayWindow->setChecked(true);

    s = k->getConf("silent", "true");

    if(s == "false")
        ui->actionSilent->setChecked(false);
    else if(s == "true")
        ui->actionSilent->setChecked(true);

    this->setGeometry(x, y, bredd, hojd);
//    ui->progressLabel->setGeometry(5,0,1021,80);
    ui->progressLabel->setWordWrap(true);
//   ui->lineEdit->setGeometry(5,85,1021,30);
    s = k->getConf("MAX_RECENT_FILES", "10");
    ss << s;
    ss >> MAX_RECENT_FILES;
    ss.clear();
    delete k;
    const QString RECENT_FILE_FILE = QDir::toNativeSeparators(QDir::homePath() + "/." + APPLICATION_NAME + "/recentfiles.conf");
    QFile file(RECENT_FILE_FILE);

    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);

        while(!in.atEnd())
            recentFiles.append(in.readLine());
    }

    file.close();
// Windows
// #ifdef Q_WS_WIN
#ifdef Q_OS_WIN // QT5
    ui->actionUseNativeDialogs->setVisible(false);
    ui->actionCreateDebianMd5sums->setVisible(false);
    ui->actionCreateDebianMd5sumsAuto->setVisible(false);
//    ui->actionUninstall->setVisible(true);
//    ui->actionUninstall->setEnabled(true);
#endif
// Linux
#ifdef Q_OS_LINUX

    if(QTINSTALLERFRAMEWORK) {
        ui->actionUninstall->setVisible(true);
        ui->actionUninstall->setEnabled(true);
    }

#endif
// End Linux
}

void Hash::setEndConfig()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QRect rect;
    rect = this->geometry();
    int x_lage = rect.x();
    int y_lage = rect.y();
    int bredd = rect.width();
    int hojd = rect.height();
    std::stringstream ss;
    std::string str;
    ss.clear();
    ss << x_lage;
    ss >> str;
    k->setConf("x_lage", str);
    ss.clear();
    ss << y_lage;
    ss >> str;
    k->setConf("y_lage", str);
    ss.clear();
    ss << bredd;
    ss >> str;
    k->setConf("bredd", str);
    ss.clear();
    ss << hojd;
    ss >> str;
    k->setConf("hojd", str);
    ss.clear();
    ss << MAX_RECENT_FILES;
    ss >> str;
    k->setConf("MAX_RECENT_FILES", str);
    delete k;
    const QString RECENT_FILE_FILE = QDir::toNativeSeparators(QDir::homePath() + "/." + APPLICATION_NAME + "/recentfiles.conf");
    QFile file(RECENT_FILE_FILE);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        tesp(tr("Could not save a file to store hash sums in. Check your file permissions.") + file.errorString());
        QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
    }

    for(int i = 0; i < recentFiles.size(); ++i) {
        QTextStream out(&file);
        out << recentFiles.at(i) << "\n";
    }

    file.close();
}

/* draganddrop */
void Hash::dropEvent(QDropEvent * ev)
{
    lista = new QStringList;
    QStringList fillista;
    QString surl, spath;
    QList<QUrl> urls = ev->mimeData()->urls();
    int antalfiler = urls.size();
    spath = urls.at(0).toString(QUrl::RemoveFilename);
    spath = QDir::toNativeSeparators(spath);
    int hittat = spath.indexOf("file:\\\\\\");

    if(hittat >= 0)
        spath = spath.remove(hittat, 8);

    bool visa = ui->actionAlwaysOpenTheDisplayWindow->isChecked();

    foreach(QUrl url, urls) {
        QFileInfo fileInfo(url.fileName());
        QString filename(fileInfo.fileName());
        surl = url.toString(QUrl::PreferLocalFile);
        recentFileList(surl);
        surl = QDir::toNativeSeparators(surl);

        if((antalfiler > 1) || visa) {
            bool lyckades = doChecksum(surl, lista);

            if(!lyckades) {
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
                tesp(tr("No hash sum could be calculated"));
            } else {
                fillista.append(filename);
            }
        } else {
            bool lyckades = doChecksum(surl);

            if(!lyckades) {
                QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("No hash sum could be calculated"));
                tesp(tr("No hash sum could be calculated"));
            }
        }
    }

    if((antalfiler > 1) || visa) {
        QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
        k = new Config(conf_file_name);
        std::string algorithm = k->getConf("algorithm");
        QString qsalgorithm = QString::fromStdString(algorithm);
        delete k;
        d = new QDialog;
        ui2 = new Ui::Lic;
        //Font ui2
        int id = QFontDatabase::addApplicationFont(":/fonts/PTMono-Regular.ttf");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        // SLUT Font ui2
        ui2->setupUi(d);
        ui2->textBrowser->setFont(f);
        /* COLOR */
        QColor c(RED, GREEN, BLUE);
        QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
        p.setColor(QPalette::Base, c); // set color "Red" for textedit base
        ui2->textBrowser->setPalette(p); // change textedit palette
        /* */
        ui2->textBrowser->append(tr("Path: ") + "\"" + spath + "\"");
        ui2->textBrowser->append(tr("Algorithm: ") + qsalgorithm);

        for(int i = 0; i < lista->size(); i++)
            ui2->textBrowser->append(lista->at(i) + " " + fillista.at(i));

        d->setWindowFlags(Qt::WindowSystemMenuHint);
        d->setWindowTitle(APPLICATION_NAME " " VERSION);
        connect(ui2->pbClose, SIGNAL(clicked()), this, SLOT(stang()));
        d->show();
        connect(ui2->pbOpenFile, SIGNAL(pressed()), this, SLOT(save()));
    }
}

void Hash::save()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string save_path = k->getConf("save_path");
    QString qsave_path = QString::fromStdString(save_path);
    QFileInfo info(qsave_path);

    if(info.isReadable())
        sokvag = qsave_path;
    else
        sokvag = QDir::homePath();

    std::string usenativedialogs = k->getConf("usenativedialogs");
    QString textBrowserText = ui2->textBrowser->toPlainText();
    /* COLOR */
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui2->textBrowser->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c); // set color "Red" for textedit base
    ui2->textBrowser->setPalette(p); // change textedit palette
    /* */
    QFileDialog *dialog = new QFileDialog(this, tr("Save the hash summary list as a text file"), sokvag, tr("Any file (*)"));
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->setViewMode(QFileDialog::Detail);

    if(usenativedialogs == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(usenativedialogs == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    if(dialog->exec()) {
        QString qsdatetime = QDateTime::currentDateTime().toString(Qt::ISODate) + "\n";
        QStringList sl = dialog->selectedFiles();
        const QString &fileName = sl.at(0);

        if(fileName.isEmpty())
            return;

        QFile file(fileName);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            delete k;
            return;
        }

        QTextStream out(&file);
        out << qsdatetime;
        out << textBrowserText << endl;
        QDir dir;
        dir = dialog->directory();
        QString directoryName = dir.path();
        k->setConf("save_path", directoryName.toStdString());
    }

    delete k;
}

void Hash::saveOne()
{
    QString conf_file_name = QDir::toNativeSeparators("/" APPLICATION_NAME ".conf");
    k = new Config(conf_file_name);
    QString sokvag;
    std::string save_path = k->getConf("save_path");
    std::string algorithm = k->getConf("algorithm");
    QString qsalgorithm = QString::fromStdString(algorithm);
    QString qsave_path = QString::fromStdString(save_path);
    QFileInfo info(qsave_path);

    if(info.isReadable())
        sokvag = qsave_path;
    else
        sokvag = QDir::homePath();

    std::string usenativedialogs = k->getConf("usenativedialogs");
    QString shecksumman = ui->lineEdit->text();
    QString path = QDir::toNativeSeparators(ui->progressLabel->toolTip());
    QFileDialog *dialog = new QFileDialog(this, tr("Save the hash summary list as a text file"), sokvag, tr("Any file (*)"));
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->setViewMode(QFileDialog::Detail);

    if(usenativedialogs == "0")
        dialog->setOption(QFileDialog::DontUseNativeDialog, true);
    else if(usenativedialogs == "1")
        dialog->setOption(QFileDialog::DontUseNativeDialog, false);

    if(dialog->exec()) {
        QString qsdatetime = QDateTime::currentDateTime().toString(Qt::ISODate) + "\n";
        QStringList sl = dialog->selectedFiles();
        const QString &fileName = sl.at(0);

        if(fileName.isEmpty())
            return;

        QFile file(fileName);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            tesp(tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            QMessageBox::critical(this, APPLICATION_NAME " " VERSION, tr("Could not save a file to store hash sums in. Check your file permissions. ") + file.errorString());
            delete k;
            return;
        }

        QTextStream out(&file);
        out << qsdatetime;
        out << tr("Algorithm: ") + qsalgorithm << endl;
        out << path << endl;
        out << shecksumman << endl;
        QDir dir;
        dir = dialog->directory();
        QString directoryName = dir.path();
        k->setConf("save_path", directoryName.toStdString());
    }

    delete k;
}

void Hash::dragEnterEvent(QDragEnterEvent * ev)
{
    ev->accept();
}
