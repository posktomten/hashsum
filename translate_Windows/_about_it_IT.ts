<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="50"/>
        <source>About </source>
        <translation>Info su </translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">Copyright ©</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licenza:</translation>
    </message>
    <message>
        <source> GPL Version 3</source>
        <translation type="vanished"> GPL Version 3</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>Copyright © </source>
        <translatorcomment>Copyright ©</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>License: </source>
        <translation>Licenza: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>viene distribuito nella speranza che possa essere utile, ma SENZA ALCUNA GARANZIA; senza nemmeno la garanzia implicita di COMMERCIABILITÀ o IDONEITÀ PER UNO SCOPO PARTICOLARE.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>See the GNU General Public License version 3 for more details.</source>
        <translation>Per maggiori dettagli vedi la GNU General Public License versione 3.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>Source code</source>
        <translation>Codice sorgente</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Website</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="55"/>
        <source>Version history</source>
        <translation>Cronolgia versioni</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Created: </source>
        <translation>Creato: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Compiled on: </source>
        <translation>Compilato: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source> is in the folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Compiler:</source>
        <translation>Compilatore:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="70"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Compilatore: MinGW (GCC per Windows) versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="76"/>
        <source>Compiler: Micrsoft Visual C++ version </source>
        <translation>Compilatore: C++ di Micrsoft Visual versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="77"/>
        <source>&lt;br&gt;Full version number: </source>
        <translation>&lt;br&gt;Numero versione completa: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <location filename="../about.cpp" line="82"/>
        <location filename="../about.cpp" line="84"/>
        <location filename="../about.cpp" line="86"/>
        <location filename="../about.cpp" line="88"/>
        <source>Programming language: C++</source>
        <translation>Linguaggio programmazione: C++</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <source>C++ version: C++20</source>
        <translation>C++ version: C++20</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="82"/>
        <source>C++ version: C++17</source>
        <translation>C++ version: C++17</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>C++ version: C++14</source>
        <translation>C++ version: C++14</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>C++ version: C++11</source>
        <translation>C++ version: C++11</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="88"/>
        <source>C++ version: Unknown</source>
        <translatorcomment>C++ version: Unknown</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <source>Version: C++20</source>
        <translation type="vanished">Version: C++20</translation>
    </message>
    <message>
        <source>Version: C++17</source>
        <translation type="vanished">Version: C++17</translation>
    </message>
    <message>
        <source>Version: C++14</source>
        <translation type="vanished">Version: C++14</translation>
    </message>
    <message>
        <source>Version: C++11</source>
        <translation type="vanished">Version: C++11</translation>
    </message>
    <message>
        <source>Version: Unknown</source>
        <translation type="vanished">Version: Unknown</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <source>Application framework: Qt version </source>
        <translation>Framework applicazione: Qt versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="92"/>
        <source>Processor architecture: 32-bit</source>
        <translation>Architettura processore: 32 bit</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="95"/>
        <source>Processor architecture: 64-bit</source>
        <translation>Architettura processore: 64 bit</translation>
    </message>
</context>
</TS>
