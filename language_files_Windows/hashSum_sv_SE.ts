<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="48"/>
        <location filename="../checkforupdates.cpp" line="110"/>
        <location filename="../checkforupdates.cpp" line="113"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen Internet-anslutning hittades.
Kontrollera dina Internet-inställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <location filename="../checkforupdates.cpp" line="119"/>
        <source>Where is a new version of hashSum. Latest Version: </source>
        <translation>Det finns en ny verion av hashSum. Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <source>, Please download. </source>
        <translation>, Vänligen ladda ner. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>&lt;br&gt;There is a new version of </source>
        <translation>&lt;br&gt;Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>.&lt;br&gt;Latest version: </source>
        <translation>.&lt;br&gt;Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source> is newer than the latest official version </source>
        <translation> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="71"/>
        <source>You have the latest version of hashsum.</source>
        <translation>Du har senaste versionen av hashSum.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="79"/>
        <location filename="../checkforupdates.cpp" line="125"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det blev fel när versionen kollades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>Latest Version </source>
        <translation>Senaste version </translation>
    </message>
    <message>
        <source>
There is a later version of </source>
        <oldsource>
There is a newer version of </oldsource>
        <translation type="vanished">
Det finns en senare version av </translation>
    </message>
    <message>
        <source>Latest version: </source>
        <translation type="vanished">Senaste versionen: </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Ladda ner från</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="45"/>
        <source>No Internet connection was found. Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades. Kontrollera dina internetinställningar och din brandvägg.</translation>
    </message>
    <message>
        <source>Where is a newer version of hashSum, Please download.</source>
        <translation type="vanished">Det finns en nyare version av hashSum, vänligen ladda ner.</translation>
    </message>
    <message>
        <source>Please </source>
        <translation type="vanished">Vänligen </translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">Ladda ner</translation>
    </message>
    <message>
        <source>
There is a newer version of </source>
        <translation type="vanished">
Det finns en nyare version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="63"/>
        <source>Your version of hashsum is newer than the latest official version.</source>
        <translation>Din version av hashSum är nyare än den senaste officiella versionen.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source>
Your version of </source>
        <translation>
Din version av </translation>
    </message>
    <message>
        <source> is later than the latest official version </source>
        <translation type="vanished"> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="74"/>
        <source>
You have the latest version of </source>
        <translation>
Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="82"/>
        <location filename="../checkforupdates.cpp" line="128"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det blev ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="165"/>
        <source>New updates:</source>
        <translation>Nya uppdateringar:</translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation>Uppdrag</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumman har kopierats till klippbordet</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Dubbelklicka för att kopiera till klippbordet</translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1054"/>
        <location filename="../hash.cpp" line="1055"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Dubbelklicka för att kopiera till klippbordet</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation>&amp;Algoritm</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="140"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="151"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="160"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigera</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="169"/>
        <source>&amp;Text</source>
        <translation>&amp;Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="423"/>
        <source>Hash sum</source>
        <oldsource>Checksum</oldsource>
        <translation>Hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="208"/>
        <source>Check for updates</source>
        <translation>Kolla efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation>Senast öppnade filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="176"/>
        <source>Speech</source>
        <translation>Tal</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="196"/>
        <source>Check file or files</source>
        <oldsource>Check file</oldsource>
        <translation>Kontrollera fil eller filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="217"/>
        <location filename="../hash.ui" line="220"/>
        <location filename="../hash.ui" line="223"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="231"/>
        <source>Check for program updates at program start</source>
        <translation>Kolla efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="243"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="246"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="254"/>
        <source>MD4</source>
        <translation>MD4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="262"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="270"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="275"/>
        <source>Compare two files</source>
        <oldsource>Compare</oldsource>
        <translation>Jämför två filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="284"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="293"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="298"/>
        <location filename="../hash.ui" line="301"/>
        <source>Create hash sum list</source>
        <oldsource>Create hash sum list 44</oldsource>
        <translation>Skapa hashsummalista</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="309"/>
        <source>Save hash sum list where the files are</source>
        <translatorcomment>Spara hashsummalistan där filerna finns</translatorcomment>
        <translation>Spara hashsummalistan där filerna är</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="319"/>
        <source>Create hash sum lists, recursively</source>
        <translation>Skapa hashsummalistor, rekursivt</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="327"/>
        <source>Write date and time in the hash sum file</source>
        <oldsource>Write time and date in the hash sum file</oldsource>
        <translation>Skriv datum och tid i hashsummafilen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="336"/>
        <source>Find changes</source>
        <translation>Hitta förändringar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="344"/>
        <source>Use native dialogs</source>
        <translation>Använd operativsystemets dialogrutor</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="352"/>
        <source>Allways open the home directory</source>
        <translation>Öppna alltid hemkatalogen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="357"/>
        <location filename="../hash.ui" line="428"/>
        <source>Compare with a given hash sum</source>
        <translation>Jämför med en given hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="365"/>
        <source>Save as text (*.*)</source>
        <oldsource>Save as text (*)</oldsource>
        <translation>Spara som text (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="373"/>
        <source>Save as pdf (*.pdf)</source>
        <translation>Spara som pdf (*.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="381"/>
        <source>Color pdf</source>
        <oldsource>Print Color pdf</oldsource>
        <translation>pdf i färg</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="393"/>
        <source>Copy Path</source>
        <translation>Kopiera sökvägen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="405"/>
        <source>Copy Hash Sum</source>
        <translation>Kopiera hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="413"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <oldsource>Save as text and pdf (* and *.pdf)</oldsource>
        <translation>Spara som text och pdf (*.* och *.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="418"/>
        <source>Hash sum from text</source>
        <translation>Hashsumma från text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="437"/>
        <location filename="../hash.ui" line="440"/>
        <location filename="../hash.ui" line="443"/>
        <source>Help...</source>
        <oldsource>Help</oldsource>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="449"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="458"/>
        <source>Create Debian md5sums</source>
        <oldsource>Create debian md5sums</oldsource>
        <translation>Skapa Debian md5sums</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="466"/>
        <source>Show full path in the hash sum file</source>
        <translation>Visa hela sökvägen i hashsummefilen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="475"/>
        <source>Create Debian md5sums (auto)</source>
        <translation>Skapa Debian md5sums (automatiskt)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="484"/>
        <source>Greek (Not complete)</source>
        <translation>Grekiska (Inte komplett)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="588"/>
        <source>German (Not complete)</source>
        <translation>Tyska (Inte fullständig)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="658"/>
        <location filename="../hash.cpp" line="308"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="669"/>
        <source>Mainteance Tool...</source>
        <translation>Underhållsverktyget...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="680"/>
        <source>Desktop Shortcut</source>
        <translation>Genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="700"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="714"/>
        <source>Silent</source>
        <translation>Tyst</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Grekiska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="492"/>
        <source>SHA224</source>
        <translation>SHA224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="500"/>
        <source>SHA256</source>
        <translation>SHA256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="508"/>
        <source>SHA384</source>
        <translation>SHA384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="516"/>
        <source>SHA512</source>
        <translation>SHA512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="524"/>
        <source>SHA3_224</source>
        <translation>SHA3_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="532"/>
        <source>SHA3_256</source>
        <translation>SHA3_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="540"/>
        <source>SHA3_384</source>
        <translation>SHA3_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="551"/>
        <source>SHA3_512</source>
        <translation>SHA3_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="560"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="569"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="574"/>
        <source>Open Hash Sum List</source>
        <translation>Öppna hashsummelistan</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="579"/>
        <source>Open Comparison list</source>
        <translation>Öppna jämförelselistan</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Tyska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="596"/>
        <source>Save to file</source>
        <translation>Spara till fil</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="604"/>
        <source>Keccak_224</source>
        <translation>Keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="612"/>
        <source>Keccak_256</source>
        <translation>Keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="620"/>
        <source>Keccak_384</source>
        <translation>Keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="628"/>
        <source>Keccak_512</source>
        <translation>Keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="633"/>
        <source>Find identical files</source>
        <translation>Sök efter identiska filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="638"/>
        <source>Are there duplicates?</source>
        <translation>Finns det dubletter?</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="646"/>
        <source>Always open the display window</source>
        <translation>Öppna alltid visningsfönstret</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="314"/>
        <source>Default file name</source>
        <translation>Standard filnamn</translation>
    </message>
    <message>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <oldsource>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </oldsource>
        <translation type="vanished">Detta program är fri programvara; du kan distribuera det och/eller modifiera det under villkoren i GNU General Public License, publicerad av Free Software Foundation, antingen version </translation>
    </message>
    <message>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation type="vanished"> av licensen, eller (om du så vill) någon senare version Detta program distribueras i hopp om att det ska vara användbart, men UTAN NÅGON GARANTI, även utan underföstådd garanti om SÄLJBARHET eller LÄMPLIGHET FÖR ETT VISST SYFTE Se GNU General Public License för fler detaljer. Du bör ha fått en kopia av GNU General Public License tillsammans med detta program, om inte, skriv till Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1304"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1311"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1311"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1332"/>
        <location filename="../hash.cpp" line="1368"/>
        <location filename="../hash.cpp" line="1404"/>
        <location filename="../hash.cpp" line="1440"/>
        <location filename="../hash.cpp" line="1476"/>
        <location filename="../hash.cpp" line="1512"/>
        <location filename="../hash.cpp" line="1548"/>
        <location filename="../hash.cpp" line="1584"/>
        <location filename="../hash.cpp" line="1620"/>
        <location filename="../hash.cpp" line="1656"/>
        <location filename="../hash.cpp" line="1692"/>
        <location filename="../hash.cpp" line="1728"/>
        <location filename="../hash.cpp" line="1766"/>
        <location filename="../hash.cpp" line="1802"/>
        <location filename="../hash.cpp" line="1838"/>
        <location filename="../hash.cpp" line="1972"/>
        <location filename="../hash.cpp" line="2627"/>
        <location filename="../hash.cpp" line="2943"/>
        <location filename="../hash.cpp" line="3013"/>
        <location filename="../hash.cpp" line="3098"/>
        <location filename="../hash.cpp" line="3333"/>
        <location filename="../hash.cpp" line="3339"/>
        <location filename="../hash.cpp" line="3707"/>
        <location filename="../hash.cpp" line="3882"/>
        <location filename="../hash.cpp" line="4002"/>
        <source>Algorithm: </source>
        <translation>Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1352"/>
        <location filename="../hash.cpp" line="1388"/>
        <location filename="../hash.cpp" line="1424"/>
        <location filename="../hash.cpp" line="1460"/>
        <location filename="../hash.cpp" line="1496"/>
        <location filename="../hash.cpp" line="1532"/>
        <location filename="../hash.cpp" line="1568"/>
        <location filename="../hash.cpp" line="1604"/>
        <location filename="../hash.cpp" line="1640"/>
        <location filename="../hash.cpp" line="1676"/>
        <location filename="../hash.cpp" line="1712"/>
        <location filename="../hash.cpp" line="1748"/>
        <location filename="../hash.cpp" line="1786"/>
        <location filename="../hash.cpp" line="1822"/>
        <location filename="../hash.cpp" line="1858"/>
        <location filename="../hash.cpp" line="2283"/>
        <location filename="../hash.cpp" line="2290"/>
        <location filename="../hash.cpp" line="3706"/>
        <source>Default file name: </source>
        <translation>Standard filnamn: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1089"/>
        <location filename="../hash.cpp" line="1887"/>
        <location filename="../hash.cpp" line="1888"/>
        <source>Open a file to calculate the hash sum</source>
        <oldsource>Open a file to calculate the checksum</oldsource>
        <translation>Öppna en fil för att beräkna hashsumman</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="833"/>
        <location filename="../hash.cpp" line="1091"/>
        <location filename="../hash.cpp" line="1890"/>
        <location filename="../hash.cpp" line="2010"/>
        <location filename="../hash.cpp" line="2054"/>
        <location filename="../hash.cpp" line="2124"/>
        <location filename="../hash.cpp" line="2316"/>
        <location filename="../isthereaduplicate.cpp" line="45"/>
        <source>All Files (*)</source>
        <oldsource>All Files (*.*)</oldsource>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1123"/>
        <location filename="../hash.cpp" line="1124"/>
        <location filename="../hash.cpp" line="1929"/>
        <location filename="../hash.cpp" line="1930"/>
        <location filename="../hash.cpp" line="1941"/>
        <location filename="../hash.cpp" line="2561"/>
        <location filename="../hash.cpp" line="3845"/>
        <location filename="../hash.cpp" line="3846"/>
        <location filename="../hash.cpp" line="3854"/>
        <location filename="../hash.cpp" line="3855"/>
        <location filename="../isthereaduplicate.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="81"/>
        <source>No hash sum could be calculated</source>
        <oldsource>No checksum could be calculated</oldsource>
        <translation>Ingen hashsumma kunde beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2097"/>
        <location filename="../hash.cpp" line="2098"/>
        <source>The files could not be compared.</source>
        <translation>Filerna kunde inte jämföras.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2314"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <oldsource>Open Files to Create a List of Checksums</oldsource>
        <translation>Öppna filer för att skapa en lista med hashsummor</translation>
    </message>
    <message>
        <source> You have chosen to use native dialogs. It may not work with your operating system, Linux. You have the option to uncheck &quot;Use native dialogs&quot; in the tools menu.</source>
        <translation type="vanished"> Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem, Linux. Du har möjlighet att bocka ur &quot;Använd operativsystemets dialogrutor&quot; på verktygsmenyn.</translation>
    </message>
    <message>
        <source>You have chosen to use native dialogs. It may not work with your operating system, Linux. You have the option to uncheck &quot;Use native dialogs&quot; in the tools menu.</source>
        <translation type="vanished">Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem, Linux. Du har möjlighet att bocka ur &quot;Använd operativsystemets dialogrutor&quot; på verktygsmenyn.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="46"/>
        <location filename="../isthereaduplicate.cpp" line="94"/>
        <source>Select a folder to Find identical files</source>
        <translation>Välj en folder för att söke efter identiska filer</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="48"/>
        <location filename="../isthereaduplicate.cpp" line="96"/>
        <source>All Files (*.*)</source>
        <translation>Alla filer (*.*)</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="83"/>
        <location filename="../duplicates.cpp" line="84"/>
        <location filename="../isthereaduplicate.cpp" line="124"/>
        <location filename="../isthereaduplicate.cpp" line="125"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation>Kunde inte kolla hashsumman. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2513"/>
        <location filename="../hash.cpp" line="2516"/>
        <location filename="../hash.cpp" line="2523"/>
        <location filename="../hash.cpp" line="2525"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>I have been looking into </source>
        <translation>Jag har letat i </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1971"/>
        <location filename="../hash.cpp" line="2519"/>
        <location filename="../hash.cpp" line="3881"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>Path: </source>
        <translation>Sökväg: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <source>Path: &quot;</source>
        <translation>Sökväg: &quot;</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="130"/>
        <location filename="../duplicates.cpp" line="131"/>
        <location filename="../isthereaduplicate.cpp" line="173"/>
        <location filename="../isthereaduplicate.cpp" line="174"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation>Jag har kollat i alla foldrar och kollat alla filer! Uppdraget är utfört.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="239"/>
        <location filename="../isthereaduplicate.cpp" line="196"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="241"/>
        <source>Recursively from </source>
        <translation>Rekursivt från </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="264"/>
        <location filename="../duplicates.cpp" line="265"/>
        <source>The number of files with one or more duplicates: </source>
        <translation>Antalet filer med en eller flera dubletter: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="269"/>
        <location filename="../isthereaduplicate.cpp" line="222"/>
        <source>Duplicate files</source>
        <translation>Identiska filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2468"/>
        <location filename="../hash.cpp" line="2639"/>
        <location filename="../hash.cpp" line="2955"/>
        <location filename="../hash.cpp" line="3025"/>
        <source> No hash sum could be calculated</source>
        <translation> Ingen hashsumma kunde beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2648"/>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2891"/>
        <location filename="../hash.cpp" line="2893"/>
        <location filename="../hash.cpp" line="2962"/>
        <location filename="../hash.cpp" line="2979"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="3032"/>
        <location filename="../hash.cpp" line="3041"/>
        <source>Hash sum for </source>
        <translation>Hashsumma för </translation>
    </message>
    <message>
        <source>Text Files(*)</source>
        <oldsource>Text Files(*.txt)</oldsource>
        <translation type="obsolete">Textfiler(*.)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2648"/>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2962"/>
        <location filename="../hash.cpp" line="3032"/>
        <source> files was successfully calculated</source>
        <translation> filer kunde framgångsrikt beräknas</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../hash.cpp" line="2513"/>
        <location filename="../hash.cpp" line="2523"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation> mapp efter filer. Jag har lyckats att beräkna hashsumman i alla mappar som innehåller filer.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2516"/>
        <location filename="../hash.cpp" line="2525"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation> mappar efter filer. Jag har lyckats att beräkna hashsumman i alla mappar som innehåller filer.
</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2528"/>
        <location filename="../hash.cpp" line="2529"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation>Jag har kollat alla foldrar och skapat alla filer! Uppdraget slutfört.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2875"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="3033"/>
        <source>Copyright </source>
        <translation>Copyright </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="240"/>
        <location filename="../hash.cpp" line="2653"/>
        <location filename="../hash.cpp" line="2878"/>
        <location filename="../hash.cpp" line="2966"/>
        <location filename="../hash.cpp" line="3036"/>
        <location filename="../isthereaduplicate.cpp" line="205"/>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2861"/>
        <source>Files not tested</source>
        <translation>Ej testade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2978"/>
        <location filename="../hash.cpp" line="3040"/>
        <source>File: </source>
        <translation>Fil: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2008"/>
        <source>Open two files to compare their hash sums.</source>
        <translation>Öppna två filer för att jämföra deras hashsummor.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2122"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation>Välj en földer för att rekursivt generera hashsummalistor</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="865"/>
        <location filename="../hash.cpp" line="2153"/>
        <location filename="../hash.cpp" line="2154"/>
        <location filename="../hash.cpp" line="2453"/>
        <location filename="../hash.cpp" line="2842"/>
        <location filename="../hash.cpp" line="2938"/>
        <location filename="../hash.cpp" line="3008"/>
        <location filename="../hash.cpp" line="3806"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <oldsource>Could not open a file to store hash sums in. Check your file permissions.</oldsource>
        <translation>Kan inte spara en fil med hashsummor. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2513"/>
        <location filename="../hash.cpp" line="2516"/>
        <location filename="../hash.cpp" line="2523"/>
        <location filename="../hash.cpp" line="2525"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>(Algorithm: </source>
        <translation>(Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2648"/>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2891"/>
        <location filename="../hash.cpp" line="2893"/>
        <location filename="../hash.cpp" line="2962"/>
        <location filename="../hash.cpp" line="2979"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="3032"/>
        <location filename="../hash.cpp" line="3041"/>
        <source> of </source>
        <translation> av </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2913"/>
        <source>Save the file with the hash sums</source>
        <translation>Spara filen med hashsummorna</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2891"/>
        <location filename="../hash.cpp" line="2893"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="3041"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation> filer blev framgångsrikt beräknade (Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source>Welcome to hashSum. I use the </source>
        <translation>Välkommen till hashSum. Jag använder </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source> algorithm when calculating hash sums.</source>
        <translation> algoritmen när jag beräknar hashsummor.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="72"/>
        <source>My name is </source>
        <translation>Mitt namn är </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="102"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="302"/>
        <source>To the website</source>
        <translation>Till webbsidan</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="305"/>
        <source>Download the latest version</source>
        <translation>Ladda ner senaste versionen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="310"/>
        <source>Download the new version</source>
        <translation>Ladda ner den nya versionen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="331"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Ett oväntat felinträffade&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;kan inten hittas eller är ingen körbar fil.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="394"/>
        <source>&quot;Control Panel\All Control Panel Items\Programs and Features&quot; cannot be found</source>
        <translation>&quot;Kontrollpanelen\Alla objekt på kontrollpanelen\Program och funktioner&quot; kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="409"/>
        <source>MaintenanceTool cannot be found</source>
        <translation>Hittar inte Underhålsverktyget</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="831"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <oldsource>Select the &quot;usr&quot; directory to start create checksums recursively</oldsource>
        <translation>Välj &quot;usr&quot; katalogen för att starta att skapa hashsummor rekursivt</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="881"/>
        <source>md5sums file has been successfully created!</source>
        <translation>md5sums filen har skapats!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="884"/>
        <source>md5sums file has been successfully created in </source>
        <translation>md5sums filen har framgångsrikt skapats i </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="920"/>
        <source>Compare with this hash sum...</source>
        <translation>Jämför med denna hashsumma...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="937"/>
        <source>...with hash sum for this text:</source>
        <translation>... med hashsumma för denna text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="949"/>
        <location filename="../hash.cpp" line="1016"/>
        <source> characters. Please reduce the length of the text.</source>
        <translation> tecken. Vänligen reducera textens längd.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="964"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation>Det är inte samma hashsumma som du jämför med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="965"/>
        <location filename="../hash.cpp" line="966"/>
        <source>Hash sums are NOT equal!</source>
        <translation>Hashsummarna är INTE lika!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="970"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation>Det är samma hashsumma som den du jämför med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="971"/>
        <location filename="../hash.cpp" line="972"/>
        <source>Hash sums are equal!</source>
        <translation>Hashsummorna är lika!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="982"/>
        <location filename="../hash.cpp" line="984"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation> Blanktecken är inte borttagna när hashsumman beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1025"/>
        <source>Hashsum could not be calculated. An unknown error has occurred.</source>
        <translation>Hashsuman kunde inte beräknas. Ett okänt fel har uppstått.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1061"/>
        <location filename="../hash.cpp" line="1062"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumman har kopierats till klippbordet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1127"/>
        <location filename="../hash.cpp" line="1131"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation> Och hashsumman du jämför med är: &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1127"/>
        <source>&quot; and they are the same</source>
        <translation>&quot; och de är lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1128"/>
        <location filename="../hash.cpp" line="1129"/>
        <source>The hash sums are equal</source>
        <translation>Hashsummorna är lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1131"/>
        <source>&quot; and they are NOT the same</source>
        <translation>&quot; och de är INTE lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1132"/>
        <source>The hash sums are not equal</source>
        <translation>Hashsummorna är inte lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1133"/>
        <source>The hash sums are NOT equal</source>
        <translation>Hashsummorna är INTE lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1145"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation>Du har valt att använda operativsystemets dialogrutor. Det kanske inte alltid fungerar med ditt operativsystem.</translation>
    </message>
    <message>
        <source>This is the text to speech edition of hashsum. Version </source>
        <translation type="vanished">Detta är text till tal-versionen av hashsum.Version </translation>
    </message>
    <message>
        <source>Default file name is md4. The md4 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är md4. Algoritmen md4 kommer att användas.</translation>
    </message>
    <message>
        <source>The md4 algorithm will be used.</source>
        <translation type="vanished">Algoritmen md4 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is md5. The md5 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är md5. Algoritmen md5 kommer att användas.</translation>
    </message>
    <message>
        <source>The md5 algorithm will be used.</source>
        <translation type="vanished">Algoritmen md5 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is SHA1. The SHA1 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är SHA1. Algoritmen SHA1 kommer att användas.</translation>
    </message>
    <message>
        <source>The SHA1 algorithm will be used.</source>
        <translation type="vanished">Algoritmen SHA1 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha224. The sha224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha224. Algoritmen sha224 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha256. The sha256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha256. Algoritmen sha256 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha384. The sha384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha384. Algoritmen sha384 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha512. The sha512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha512. Algoritmen sha512 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha512 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_224. The sha3_224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_224. Algoritmen sha3_224 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_256. The sha3_256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_256. Algoritmen sha3_256 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_384. The sha3_384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_384. Algoritmen sha3_384 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_512. The sha3_512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_512. Algoritmen sha3_512 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_512 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_224. The keccak_224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_224. Algoritmen keccak_224 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_256. The keccak_256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_256. Algoritmen keccak_256 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_384. The keccak_384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_384. Algoritmen keccak_384 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_512. The keccak_512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_512. Algoritmen keccak_512 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_512 kommer att användas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1940"/>
        <source>No hash sum could be calculated.</source>
        <translation>Ingen hashsumma kunde beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1944"/>
        <source>hash sum has been calculated.</source>
        <translation>hashsumman beräknades.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1949"/>
        <source>hash sums have been calculated.</source>
        <translation>hashsummman har beräknats.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2033"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Du måste välja exakt två filer.
Två filer i denna mapp eller en fil i denna mapp och en andra fil i en annan mapp som du väljer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2034"/>
        <source>You must select exactly two files. Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Du måste välja exakt två filer. Två filer i den här mappen eller en fil i den här mappen och en andra fil i en annan valfri mapp.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2051"/>
        <location filename="../hash.cpp" line="2052"/>
        <source>Open the second file to compare with.</source>
        <translation>Öppna den andra filen att jämföra med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2069"/>
        <location filename="../hash.cpp" line="2070"/>
        <source>You must select exactly one file.</source>
        <translation>Du måste välja exakt en fil.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2081"/>
        <location filename="../hash.cpp" line="2082"/>
        <source>You have compared the same file with itself.</source>
        <oldsource>You have examined the same file with itself.</oldsource>
        <translation>Du har jämfört samma fil med sig själv.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2167"/>
        <location filename="../hash.cpp" line="2192"/>
        <location filename="../hash.cpp" line="2218"/>
        <location filename="../hash.cpp" line="2244"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet behöver startas om för att språkändringen ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2168"/>
        <location filename="../hash.cpp" line="2193"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2245"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="306"/>
        <location filename="../hash.cpp" line="922"/>
        <location filename="../hash.cpp" line="939"/>
        <location filename="../hash.cpp" line="1002"/>
        <location filename="../hash.cpp" line="1070"/>
        <location filename="../hash.cpp" line="2169"/>
        <location filename="../hash.cpp" line="2194"/>
        <location filename="../hash.cpp" line="2220"/>
        <location filename="../hash.cpp" line="2246"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1000"/>
        <source>Calculate the hash sum of this text:</source>
        <translation>Beräkna hashsumman av denna text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="923"/>
        <location filename="../hash.cpp" line="940"/>
        <location filename="../hash.cpp" line="1003"/>
        <location filename="../hash.cpp" line="1071"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="517"/>
        <location filename="../hash.cpp" line="638"/>
        <source> is not found</source>
        <translation> kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="447"/>
        <location filename="../hash.cpp" line="529"/>
        <location filename="../hash.cpp" line="650"/>
        <source>Open the hashSum file</source>
        <translation>Öppna hashSummefilen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="449"/>
        <location filename="../hash.cpp" line="531"/>
        <location filename="../hash.cpp" line="652"/>
        <source>Text files (*)</source>
        <translation>Textfiler (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="482"/>
        <location filename="../hash.cpp" line="565"/>
        <location filename="../hash.cpp" line="605"/>
        <location filename="../hash.cpp" line="693"/>
        <location filename="../hash.cpp" line="728"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="758"/>
        <source>The number of recently opened files to be displayed...</source>
        <oldsource>Set number of recently opened files shown...</oldsource>
        <translation>Antalet nyligen öppnade filer som skall visas...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="761"/>
        <source>Clear the list of recently opened files</source>
        <translation>Rensa listan med nyligen öppnade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="768"/>
        <source>The file</source>
        <translation>Filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="768"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation>kan inte hittas. Filen kommer att tas bort från listan över nyligen öppnade filer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="797"/>
        <source>Set number of Recent Files: </source>
        <oldsource>Set number of Recent Files: (0-30)</oldsource>
        <translation>Ställ in antalet senast öppnade filer som visas: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="866"/>
        <location filename="../hash.cpp" line="2454"/>
        <location filename="../hash.cpp" line="2843"/>
        <location filename="../hash.cpp" line="2939"/>
        <location filename="../hash.cpp" line="3009"/>
        <location filename="../hash.cpp" line="3807"/>
        <location filename="../hash.cpp" line="3937"/>
        <location filename="../hash.cpp" line="3938"/>
        <location filename="../hash.cpp" line="3994"/>
        <location filename="../hash.cpp" line="3995"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation>Det gick inte att spara filen för att lagra hash-summor. Kontrollera dina filbehörigheter. </translation>
    </message>
    <message>
        <source>Help with the program and all settings and menus can be found here:</source>
        <oldsource>Help with the program and all settings and menus can be found here:&lt;br&gt;</oldsource>
        <translation type="vanished">Hjälp med programmet och alla inställningar och menyer hittar du här:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="911"/>
        <source>The program&apos;s website can be found here:</source>
        <translation>Programmets hemsida hittar du här:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="949"/>
        <location filename="../hash.cpp" line="950"/>
        <location filename="../hash.cpp" line="1016"/>
        <location filename="../hash.cpp" line="1017"/>
        <source> can handle texts up to </source>
        <oldsource> can handle texts of up to </oldsource>
        <translation> kan hantera texter upp till </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="950"/>
        <location filename="../hash.cpp" line="1017"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translation> tecken.
Minska längden på texten.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="958"/>
        <location filename="../hash.cpp" line="959"/>
        <location filename="../hash.cpp" line="982"/>
        <location filename="../hash.cpp" line="984"/>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="989"/>
        <location filename="../hash.cpp" line="1026"/>
        <location filename="../hash.cpp" line="1035"/>
        <location filename="../hash.cpp" line="1037"/>
        <location filename="../hash.cpp" line="1040"/>
        <location filename="../hash.cpp" line="1042"/>
        <source>Hash sum calculated for:</source>
        <translation>Hashsumma som beräknas för:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="958"/>
        <location filename="../hash.cpp" line="959"/>
        <location filename="../hash.cpp" line="1026"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation>kunde inte beräknas. Ett okänt fel har inträffat.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="982"/>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="1035"/>
        <location filename="../hash.cpp" line="1040"/>
        <source>characters.</source>
        <translation>tecken.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="984"/>
        <location filename="../hash.cpp" line="989"/>
        <location filename="../hash.cpp" line="1037"/>
        <location filename="../hash.cpp" line="1042"/>
        <source>characters</source>
        <translation>tecken</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="992"/>
        <source>Hash sums are compared!</source>
        <translation>Hashsummorna är jämförda!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1035"/>
        <location filename="../hash.cpp" line="1037"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Blanktecken är inte borttagna när hashsumman beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1045"/>
        <location filename="../hash.cpp" line="1046"/>
        <source>Hash sum has been calculated!</source>
        <translation>Hashsumman är beräknad!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1068"/>
        <source>Compare with this hash sum:</source>
        <translation>Jämför med denna hashsumma:</translation>
    </message>
    <message>
        <source>You have chosen to use native dialogs. It may not work with your operating system.</source>
        <translation type="vanished">Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1211"/>
        <source>The license file is not found</source>
        <translation>Licensfilen kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1257"/>
        <source>The version history file is not found</source>
        <translation>Versionshistorikfilen hittas inte</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1294"/>
        <source>This is </source>
        <translation>Detta är </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1301"/>
        <location filename="../hash.cpp" line="1302"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1301"/>
        <source> for the Greek translation.</source>
        <translation> för den grekiska översättningen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1302"/>
        <source> for the German translation.</source>
        <translation> för den tyska översättningen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1312"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation>Ett program för att beräkna hashsummor och för att jämföra filer.</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source>This program uses Qt version </source>
        <translation>Detta program använder Qt version </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source> running on </source>
        <translation> som körs på </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="40"/>
        <location filename="../system.cpp" line="44"/>
        <location filename="../system.cpp" line="48"/>
        <location filename="../system.cpp" line="124"/>
        <source> Compiled by</source>
        <translation> Kompilerad med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="56"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1352"/>
        <location filename="../hash.cpp" line="1388"/>
        <location filename="../hash.cpp" line="1424"/>
        <location filename="../hash.cpp" line="1460"/>
        <location filename="../hash.cpp" line="1496"/>
        <location filename="../hash.cpp" line="1532"/>
        <location filename="../hash.cpp" line="1568"/>
        <location filename="../hash.cpp" line="1604"/>
        <location filename="../hash.cpp" line="1640"/>
        <location filename="../hash.cpp" line="1676"/>
        <location filename="../hash.cpp" line="1712"/>
        <location filename="../hash.cpp" line="1748"/>
        <location filename="../hash.cpp" line="1786"/>
        <location filename="../hash.cpp" line="1822"/>
        <location filename="../hash.cpp" line="1858"/>
        <source>(Click to change)</source>
        <translation>(Klicka för att ändra)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1355"/>
        <source>I use the algorithm md4</source>
        <translation>Jag använder algoritmen md4</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1391"/>
        <source>I use the algorithm md5</source>
        <translation>Jag använder algoritmen md5</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1427"/>
        <source>I use the algorithm sha1</source>
        <translation>Jag använder algoritmen sha1</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1463"/>
        <source>I use the algorithm sha224</source>
        <translation>Jag använder algoritmen sha224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1499"/>
        <source>I use the algorithm sha256</source>
        <translation>Jag använder algoritmen sha256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1535"/>
        <source>I use the algorithm sha384</source>
        <translation>Jag använder algoritmen sha384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1571"/>
        <source>I use the algorithm sha512</source>
        <translation>Jag använder algoritmen sha512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1607"/>
        <source>I use the algorithm sha3_224</source>
        <translation>Jag använder algoritmen sha3_224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1643"/>
        <source>I use the algorithm sha3_256</source>
        <translation>Jag använder algoritmen sha3_256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1679"/>
        <source>I use the algorithm sha3_384</source>
        <translation>Jag använder algoritmen sha3_384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1715"/>
        <source>I use the algorithm sha3_512</source>
        <translation>Jag använder algoritmen sha3_512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1751"/>
        <source>I use the algorithm keccak_224</source>
        <translation>Jag använder algoritmen keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1789"/>
        <source>I use the algorithm keccak_256</source>
        <translation>Jag använder algoritmen keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1825"/>
        <source>I use the algorithm keccak_384</source>
        <translation>Jag använder algoritmen keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1861"/>
        <source>I use the algorithm keccak_512</source>
        <translation>Jag använder algoritmen keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2007"/>
        <source>Open the first file</source>
        <translation>Öppna den första filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2164"/>
        <source>Please restart hashsum</source>
        <translation>Vänligen starta om hashsum</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2283"/>
        <location filename="../hash.cpp" line="2290"/>
        <location filename="../hash.cpp" line="3706"/>
        <source> (Click to change)</source>
        <translation> (Klicka för att ändra)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2365"/>
        <location filename="../hash.cpp" line="2387"/>
        <location filename="../hash.cpp" line="2405"/>
        <source>Did you select all files you want to choose?</source>
        <translation>Har du valt alla filer du vill välja?</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2366"/>
        <location filename="../hash.cpp" line="2388"/>
        <location filename="../hash.cpp" line="2406"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2367"/>
        <location filename="../hash.cpp" line="2389"/>
        <location filename="../hash.cpp" line="2407"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2427"/>
        <source>Save the Debian md5sums file</source>
        <translation>Spara Debian md5sums-filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2462"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation>Detta är ingen giltig sökväg för att bygga ett debian-paket!</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../hash.cpp" line="2532"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation>Uppdrag utfört! Jag har beräknat hashsumman för </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../hash.cpp" line="2532"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source> files.</source>
        <translation> filer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2544"/>
        <source>This is not a valid path to build a Debian package!</source>
        <oldsource>This is not a valid path to build a debian package!!</oldsource>
        <translation>Detta är inte en giltig sökväg för att bygga ett Debian paket!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2630"/>
        <source>Files in:</source>
        <translation>Filer i:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2675"/>
        <source>Open the old hash sum file to compare with</source>
        <translation>Öppna den gammla hashsummefilen för att jämföra med</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2702"/>
        <source>Hash sum could not be estimated</source>
        <translation>Hashsumman kunde inte beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2712"/>
        <source>Could not open </source>
        <translation>Kunde inte öppna </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2712"/>
        <source> with hash sums. Make sure the file exists and check your file permissions.</source>
        <translation> med hashsummor. Kontrollera att filen finns och kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source>Could not open &quot;</source>
        <translation>Kunde inte öppna &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translation>&quot; med hashsummor.
Se till att filen finns och kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2728"/>
        <location filename="../hash.cpp" line="2729"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation>Den gamla hashsummefilen innehåller ingen sökväg till filerna. Du kan inte jämföra!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2847"/>
        <source>Unchanged files</source>
        <translation>Oförändrade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2854"/>
        <source>Changed files</source>
        <translation>Ändrade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2868"/>
        <source>New files</source>
        <translation>Nya filer</translation>
    </message>
    <message>
        <source>CPU architecture:</source>
        <oldsource>CPU architecture</oldsource>
        <translation type="vanished">CPU-arkitektur:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3916"/>
        <location filename="../hash.cpp" line="3973"/>
        <source>Save the hash summary list as a text file</source>
        <translation>Spara hashlistan som en textfil</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3916"/>
        <location filename="../hash.cpp" line="3973"/>
        <source>Any file (*)</source>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10</source>
        <translation type="vanished">Windows operativsystem version 10.0, motsvararande Windows 10</translation>
    </message>
    <message>
        <source>Text Files (*.*)</source>
        <oldsource>Text Files (*)</oldsource>
        <translation type="obsolete">Textfiler (*.*)</translation>
    </message>
    <message>
        <source>pdf Files (*.pdf)</source>
        <oldsource>PDF Files(*.pdf)</oldsource>
        <translation type="obsolete">pdf filer (*.pdf)</translation>
    </message>
    <message>
        <source>Could not save a file to store Recent File list. Check your file permissions.</source>
        <translation type="vanished">Det gick inte att spara filen som lagrar dina senast öppnade filer. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <source>Unknown Windows version.</source>
        <translation type="vanished">Okänd Windowsversion.</translation>
    </message>
    <message>
        <source>MS-DOS-based version of Windows.</source>
        <translation type="vanished">MS-DOS baserad version av Windows.</translation>
    </message>
    <message>
        <source>NT-based version of Windows.</source>
        <translation type="vanished">NT baserad version av Windows.</translation>
    </message>
    <message>
        <source>CE-based version of Windows.</source>
        <translation type="vanished">CE baserad version av Windows.</translation>
    </message>
    <message>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation type="vanished">Windows 3.1 med 32-bitars operativsystem.</translation>
    </message>
    <message>
        <source>Windows 95 operating system.</source>
        <translation type="vanished">Windows 95 operativsystem.</translation>
    </message>
    <message>
        <source>Windows 98 operating system.</source>
        <translation type="vanished">Windows 98 operativsystem.</translation>
    </message>
    <message>
        <source>Windows Me operating system.</source>
        <translation type="vanished">Windows ME operativsystem.</translation>
    </message>
    <message>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation type="vanished">Windows operativsystem 4.0, motsvarande Windows NT.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation type="vanished">Windows operativsystem version 5.0, motsvararande Windows 2000.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation type="vanished">Windows operativsystem version 5.1, motsvararande Windows XP.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation type="vanished">Windows operativsystem version 5.2, motsvararande Windows Server 2003, Windows Server 2003 R2, Windows Home Server och Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation type="vanished">Windows operativsystem version 6.0, motsvararande Windows Vista och Windows Server 2008.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation type="vanished">Windows operativsystem version 6.1, motsvararande Windows 7 och Windows Server 2008 R2.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.2, corresponds to Windows 8.</source>
        <translation type="vanished">Windows operativsystem version 6.2, motsvararande Windows 8.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.3, corresponds to Windows 8.1.</source>
        <translation type="vanished">Windows operativsystem version 6.2, motsvararande Windows 8.1.</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10.</source>
        <translation type="obsolete">Windows operativsystem version 10.0, motsvararande Windows 10.</translation>
    </message>
    <message>
        <source>Windows CE operating system.</source>
        <translation type="vanished">Windows CE operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE .NET operating system.</source>
        <translation type="vanished">Windows CE.NET operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE 5.x operating system.</source>
        <translation type="vanished">Windows CE 5.x operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE 6.x operating system.</source>
        <translation type="vanished">Windows CE 6.x operativsystem.</translation>
    </message>
    <message>
        <source>You seem to be running </source>
        <translation type="obsolete">Du verkar köra </translation>
    </message>
    <message>
        <source>Architecture instruction set: </source>
        <translation type="vanished">Arkitekturens instruktionsuppsättning: </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompilerad med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="120"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="129"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilatot.</translation>
    </message>
    <message>
        <source>Screen resolution:</source>
        <translation type="vanished">Skärmupplösning:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2426"/>
        <source>Files (*.*)</source>
        <oldsource>Files (*)</oldsource>
        <translation>Filer (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2893"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation>Hashsummorna har jämförts med de gammla värdena i filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2677"/>
        <location filename="../hash.cpp" line="2912"/>
        <source>Text Files (*)</source>
        <translation>Textfiler (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2891"/>
        <source>Hash Sum values have been compared to old values in the file </source>
        <translation>Hashsummor har jämförts med gamla värden i filen </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2979"/>
        <source> files was successfully calculated. Algorithm: </source>
        <translation> filer beräknades framgångsrikt. Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3098"/>
        <source>&quot; has hash sum</source>
        <translation>&quot; har hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3333"/>
        <source>. The files are identical.</source>
        <translation>. Filerna är identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3333"/>
        <location filename="../hash.cpp" line="3339"/>
        <source>&quot; have these hash sums:</source>
        <translation>&quot;.har dessa hashsummor:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3332"/>
        <location filename="../hash.cpp" line="3334"/>
        <source>The files are identical.</source>
        <translation>Filerna är identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3339"/>
        <source>. The files are NOT identical.</source>
        <translation>. Filerna är INTE identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3338"/>
        <location filename="../hash.cpp" line="3340"/>
        <source>The files are NOT identical.</source>
        <translation>Filerna är INTE identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2279"/>
        <source>Default file name:</source>
        <translation>Standard filnamn:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3333"/>
        <location filename="../hash.cpp" line="3339"/>
        <source>&quot; and &quot;</source>
        <translation>&quot; och &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3343"/>
        <location filename="../hash.cpp" line="3347"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>Open the file you want to find duplicates for</source>
        <translation>Öppna filen du vill söka efter dubletter till</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source>I have calculated the hash sum of </source>
        <translation>Jag har beräknat hashsummorna för </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="207"/>
        <source>Identical files with </source>
        <translation>Identiska filer med </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="209"/>
        <source>Recursively searched from </source>
        <translation>Rekursivt sökt från </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source>I found </source>
        <translation>Jag hittade </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source> duplicates.</source>
        <translation> dubletter.</translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Öppna fil</translation>
    </message>
</context>
</TS>
