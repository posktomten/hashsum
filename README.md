# hashSum
Coppyright Ingemar Ceicer 2011-2024 GPL v3

@posktomten

## Read more and download: [Wiki](https://gitlab.com/posktomten/hashsum/-/wikis/home)

A program to compare files and to calculate hash sums

	Choose whether you want to calculate the hash sums, or compare two files.
	You can choose the calculation algorithm. (MD4, MD5, SHA1, SHA224, SHA256, SHA384, SHA512, SHA3_224, SHA3_256, SHA3_384, SHA3_512, KECCAK_224, KECCAK_256, KECCAK_384 and KECCAK_512).
	Ability to search for duplicates for a specific file.
	Ability to search recursively for file duplicates.
	You can create lists of checksums that are stored as a text file.
	You can recursively create lists of hash sum files.
	Ability to search recursively for file duplicates.
	You can compare against a selected hash sum.
	You can check which files have changed, who's new and what has been lost since the last time you checked.
	You can choose the operating system's "Open" and "Save" dialogues, or the program's own lists of checksums can be auto saved.
	You can calculate and compare hashsummor for text strings.
	You can choose a default filename to save the checksum lists.
	Swedish, Greek and English menus.
	The program can check for updates.
	The program supports "Drag and Drop".
	Create Debian md5sums.
	Create Debian md5sums (auto).
    
Uses Qt5.15.13 Open Source, works with Linux, Windows and Mac OS. (Have never tried to compile the source code with a Mac computer)


The program uses these libraries:<br>
- [libabout](https://gitlab.com/posktomten/libabout)<br>
- [download_install](https://gitlab.com/posktomten/download_offline_installer)<br>
- [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates)<br>
- [libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage)<br>
- [texttospeech](https://gitlab.com/posktomten/texttospeech)<br>

