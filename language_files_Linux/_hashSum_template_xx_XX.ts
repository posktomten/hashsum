<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <source>No Internet connection was found. Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="49"/>
        <location filename="../checkforupdates.cpp" line="113"/>
        <location filename="../checkforupdates.cpp" line="116"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="56"/>
        <location filename="../checkforupdates.cpp" line="124"/>
        <source>Where is a new version of hashSum. Latest Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="56"/>
        <source>, Please download. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="59"/>
        <source>&lt;br&gt;There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="59"/>
        <source>.&lt;br&gt;Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="64"/>
        <source>Your version of hashsum is newer than the latest official version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="67"/>
        <source>
Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="67"/>
        <source> is newer than the latest official version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="72"/>
        <source>You have the latest version of hashsum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="75"/>
        <source>
You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="80"/>
        <location filename="../checkforupdates.cpp" line="130"/>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="83"/>
        <location filename="../checkforupdates.cpp" line="133"/>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="127"/>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="127"/>
        <source>Latest Version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="170"/>
        <source>New updates:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="140"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="151"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="160"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="169"/>
        <source>&amp;Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="176"/>
        <source>Speech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="196"/>
        <source>Check file or files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="208"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="217"/>
        <location filename="../hash.ui" line="220"/>
        <location filename="../hash.ui" line="223"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="231"/>
        <source>Check for program updates at program start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="243"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="246"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="254"/>
        <source>MD4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="262"/>
        <source>MD5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="270"/>
        <source>SHA1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="275"/>
        <source>Compare two files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="284"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="293"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="298"/>
        <location filename="../hash.ui" line="301"/>
        <source>Create hash sum list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="309"/>
        <source>Save hash sum list where the files are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="314"/>
        <source>Default file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="319"/>
        <source>Create hash sum lists, recursively</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="327"/>
        <source>Write date and time in the hash sum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="336"/>
        <source>Find changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="344"/>
        <source>Use native dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="352"/>
        <source>Allways open the home directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="357"/>
        <location filename="../hash.ui" line="428"/>
        <source>Compare with a given hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="365"/>
        <source>Save as text (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="373"/>
        <source>Save as pdf (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="381"/>
        <source>Color pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="393"/>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="405"/>
        <source>Copy Hash Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="413"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="418"/>
        <source>Hash sum from text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="423"/>
        <source>Hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="437"/>
        <location filename="../hash.ui" line="440"/>
        <location filename="../hash.ui" line="443"/>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="449"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="458"/>
        <source>Create Debian md5sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="466"/>
        <source>Show full path in the hash sum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="475"/>
        <source>Create Debian md5sums (auto)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="484"/>
        <source>Greek (Not complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="492"/>
        <source>SHA224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="500"/>
        <source>SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="508"/>
        <source>SHA384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="516"/>
        <source>SHA512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="524"/>
        <source>SHA3_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="532"/>
        <source>SHA3_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="540"/>
        <source>SHA3_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="551"/>
        <source>SHA3_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="560"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="569"/>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="574"/>
        <source>Open Hash Sum List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="579"/>
        <source>Open Comparison list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="588"/>
        <source>German (Not complete)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="596"/>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="604"/>
        <source>Keccak_224</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="612"/>
        <source>Keccak_256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="620"/>
        <source>Keccak_384</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="628"/>
        <source>Keccak_512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="633"/>
        <source>Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="638"/>
        <source>Are there duplicates?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="646"/>
        <source>Always open the display window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="658"/>
        <location filename="../hash.cpp" line="309"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="669"/>
        <source>Mainteance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="680"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="700"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.ui" line="714"/>
        <source>Silent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="46"/>
        <location filename="../isthereaduplicate.cpp" line="94"/>
        <source>Select a folder to Find identical files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="48"/>
        <location filename="../isthereaduplicate.cpp" line="96"/>
        <source>All Files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="83"/>
        <location filename="../duplicates.cpp" line="84"/>
        <location filename="../isthereaduplicate.cpp" line="124"/>
        <location filename="../isthereaduplicate.cpp" line="125"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2530"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../hash.cpp" line="2540"/>
        <location filename="../hash.cpp" line="2542"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>I have been looking into </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../hash.cpp" line="2530"/>
        <location filename="../hash.cpp" line="2540"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2530"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../hash.cpp" line="2540"/>
        <location filename="../hash.cpp" line="2542"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>(Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../hash.cpp" line="2542"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <source>Path: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="130"/>
        <location filename="../duplicates.cpp" line="131"/>
        <location filename="../isthereaduplicate.cpp" line="173"/>
        <location filename="../isthereaduplicate.cpp" line="174"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../hash.cpp" line="2549"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../hash.cpp" line="2549"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source> files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="239"/>
        <location filename="../isthereaduplicate.cpp" line="196"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="240"/>
        <location filename="../hash.cpp" line="2670"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3050"/>
        <location filename="../isthereaduplicate.cpp" line="205"/>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="241"/>
        <source>Recursively from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="264"/>
        <location filename="../duplicates.cpp" line="265"/>
        <source>The number of files with one or more duplicates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="269"/>
        <location filename="../isthereaduplicate.cpp" line="222"/>
        <source>Duplicate files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="55"/>
        <source>Welcome to hashSum. I use the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="55"/>
        <source> algorithm when calculating hash sums.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="73"/>
        <source>My name is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="103"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="303"/>
        <source>To the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="306"/>
        <source>Download the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="307"/>
        <location filename="../hash.cpp" line="921"/>
        <location filename="../hash.cpp" line="938"/>
        <location filename="../hash.cpp" line="997"/>
        <location filename="../hash.cpp" line="1061"/>
        <location filename="../hash.cpp" line="2187"/>
        <location filename="../hash.cpp" line="2212"/>
        <location filename="../hash.cpp" line="2238"/>
        <location filename="../hash.cpp" line="2264"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="311"/>
        <source>Download the new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="332"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="334"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="334"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="395"/>
        <source>&quot;Control Panel\All Control Panel Items\Programs and Features&quot; cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="410"/>
        <source>MaintenanceTool cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="448"/>
        <location filename="../hash.cpp" line="530"/>
        <location filename="../hash.cpp" line="651"/>
        <source>Open the hashSum file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="450"/>
        <location filename="../hash.cpp" line="532"/>
        <location filename="../hash.cpp" line="653"/>
        <source>Text files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="483"/>
        <location filename="../hash.cpp" line="566"/>
        <location filename="../hash.cpp" line="606"/>
        <location filename="../hash.cpp" line="694"/>
        <location filename="../hash.cpp" line="729"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="518"/>
        <location filename="../hash.cpp" line="639"/>
        <source> is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="759"/>
        <source>The number of recently opened files to be displayed...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="762"/>
        <source>Clear the list of recently opened files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="769"/>
        <source>The file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="769"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="798"/>
        <source>Set number of Recent Files: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="832"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="834"/>
        <location filename="../hash.cpp" line="1082"/>
        <location filename="../hash.cpp" line="1909"/>
        <location filename="../hash.cpp" line="2029"/>
        <location filename="../hash.cpp" line="2073"/>
        <location filename="../hash.cpp" line="2144"/>
        <location filename="../hash.cpp" line="2334"/>
        <location filename="../isthereaduplicate.cpp" line="45"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="866"/>
        <location filename="../hash.cpp" line="2471"/>
        <location filename="../hash.cpp" line="2859"/>
        <location filename="../hash.cpp" line="2954"/>
        <location filename="../hash.cpp" line="3023"/>
        <location filename="../hash.cpp" line="3821"/>
        <location filename="../hash.cpp" line="3952"/>
        <location filename="../hash.cpp" line="3953"/>
        <location filename="../hash.cpp" line="4009"/>
        <location filename="../hash.cpp" line="4010"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="880"/>
        <source>md5sums file has been successfully created!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="883"/>
        <source>md5sums file has been successfully created in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="910"/>
        <source>The program&apos;s website can be found here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="919"/>
        <source>Compare with this hash sum...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="922"/>
        <location filename="../hash.cpp" line="939"/>
        <location filename="../hash.cpp" line="998"/>
        <location filename="../hash.cpp" line="1062"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="936"/>
        <source>...with hash sum for this text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="948"/>
        <location filename="../hash.cpp" line="1011"/>
        <source> can handle texts up to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="948"/>
        <location filename="../hash.cpp" line="1011"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="956"/>
        <location filename="../hash.cpp" line="977"/>
        <location filename="../hash.cpp" line="979"/>
        <location filename="../hash.cpp" line="982"/>
        <location filename="../hash.cpp" line="984"/>
        <location filename="../hash.cpp" line="1019"/>
        <location filename="../hash.cpp" line="1028"/>
        <location filename="../hash.cpp" line="1030"/>
        <location filename="../hash.cpp" line="1033"/>
        <location filename="../hash.cpp" line="1035"/>
        <source>Hash sum calculated for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="956"/>
        <location filename="../hash.cpp" line="1019"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="961"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="962"/>
        <source>Hash sums are NOT equal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="966"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="967"/>
        <source>Hash sums are equal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="977"/>
        <location filename="../hash.cpp" line="982"/>
        <location filename="../hash.cpp" line="1028"/>
        <location filename="../hash.cpp" line="1033"/>
        <source>characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="977"/>
        <location filename="../hash.cpp" line="979"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="979"/>
        <location filename="../hash.cpp" line="984"/>
        <location filename="../hash.cpp" line="1030"/>
        <location filename="../hash.cpp" line="1035"/>
        <source>characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="987"/>
        <source>Hash sums are compared!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="995"/>
        <source>Calculate the hash sum of this text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1028"/>
        <location filename="../hash.cpp" line="1030"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1039"/>
        <source>Hash sum has been calculated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1047"/>
        <source>Double-click to copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1053"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1059"/>
        <source>Compare with this hash sum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1080"/>
        <location filename="../hash.cpp" line="1907"/>
        <source>Open a file to calculate the hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1114"/>
        <location filename="../hash.cpp" line="1115"/>
        <location filename="../hash.cpp" line="1948"/>
        <location filename="../hash.cpp" line="1949"/>
        <location filename="../hash.cpp" line="1960"/>
        <location filename="../hash.cpp" line="2578"/>
        <location filename="../hash.cpp" line="3860"/>
        <location filename="../hash.cpp" line="3861"/>
        <location filename="../hash.cpp" line="3869"/>
        <location filename="../hash.cpp" line="3870"/>
        <location filename="../isthereaduplicate.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="81"/>
        <source>No hash sum could be calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1118"/>
        <location filename="../hash.cpp" line="1122"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1118"/>
        <source>&quot; and they are the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1119"/>
        <location filename="../hash.cpp" line="1120"/>
        <source>The hash sums are equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1122"/>
        <source>&quot; and they are NOT the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1123"/>
        <source>The hash sums are not equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1124"/>
        <source>The hash sums are NOT equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1136"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1202"/>
        <source>The license file is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1248"/>
        <source>The version history file is not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1285"/>
        <source>This is the text to speech edition of hashsum. Version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1292"/>
        <location filename="../hash.cpp" line="1293"/>
        <source>Many thanks to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1292"/>
        <source> for the Greek translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source> for the German translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1295"/>
        <source>Phone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1302"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1302"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1303"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1323"/>
        <location filename="../hash.cpp" line="1361"/>
        <location filename="../hash.cpp" line="1399"/>
        <location filename="../hash.cpp" line="1437"/>
        <location filename="../hash.cpp" line="1475"/>
        <location filename="../hash.cpp" line="1513"/>
        <location filename="../hash.cpp" line="1551"/>
        <location filename="../hash.cpp" line="1589"/>
        <location filename="../hash.cpp" line="1627"/>
        <location filename="../hash.cpp" line="1665"/>
        <location filename="../hash.cpp" line="1703"/>
        <location filename="../hash.cpp" line="1741"/>
        <location filename="../hash.cpp" line="1781"/>
        <location filename="../hash.cpp" line="1819"/>
        <location filename="../hash.cpp" line="1857"/>
        <location filename="../hash.cpp" line="1991"/>
        <location filename="../hash.cpp" line="2644"/>
        <location filename="../hash.cpp" line="2958"/>
        <location filename="../hash.cpp" line="3027"/>
        <location filename="../hash.cpp" line="3112"/>
        <location filename="../hash.cpp" line="3347"/>
        <location filename="../hash.cpp" line="3353"/>
        <location filename="../hash.cpp" line="3722"/>
        <location filename="../hash.cpp" line="3897"/>
        <location filename="../hash.cpp" line="4017"/>
        <source>Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1342"/>
        <source>Default file name is md4. The md4 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1344"/>
        <location filename="../hash.cpp" line="1382"/>
        <location filename="../hash.cpp" line="1420"/>
        <location filename="../hash.cpp" line="1458"/>
        <location filename="../hash.cpp" line="1496"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1572"/>
        <location filename="../hash.cpp" line="1610"/>
        <location filename="../hash.cpp" line="1648"/>
        <location filename="../hash.cpp" line="1686"/>
        <location filename="../hash.cpp" line="1724"/>
        <location filename="../hash.cpp" line="1762"/>
        <location filename="../hash.cpp" line="1802"/>
        <location filename="../hash.cpp" line="1840"/>
        <location filename="../hash.cpp" line="1878"/>
        <location filename="../hash.cpp" line="2301"/>
        <location filename="../hash.cpp" line="2308"/>
        <location filename="../hash.cpp" line="3721"/>
        <source>Default file name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1344"/>
        <location filename="../hash.cpp" line="1382"/>
        <location filename="../hash.cpp" line="1420"/>
        <location filename="../hash.cpp" line="1458"/>
        <location filename="../hash.cpp" line="1496"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1572"/>
        <location filename="../hash.cpp" line="1610"/>
        <location filename="../hash.cpp" line="1648"/>
        <location filename="../hash.cpp" line="1686"/>
        <location filename="../hash.cpp" line="1724"/>
        <location filename="../hash.cpp" line="1762"/>
        <location filename="../hash.cpp" line="1802"/>
        <location filename="../hash.cpp" line="1840"/>
        <location filename="../hash.cpp" line="1878"/>
        <source>(Click to change)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1346"/>
        <source>The md4 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1380"/>
        <source>Default file name is md5. The md5 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1384"/>
        <source>The md5 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1418"/>
        <source>Default file name is SHA1. The SHA1 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1422"/>
        <source>The SHA1 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1456"/>
        <source>Default file name is sha224. The sha224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1460"/>
        <source>The sha224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1494"/>
        <source>Default file name is sha256. The sha256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1498"/>
        <source>The sha256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1532"/>
        <source>Default file name is sha384. The sha384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1536"/>
        <source>The sha384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1570"/>
        <source>Default file name is sha512. The sha512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1574"/>
        <source>The sha512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1608"/>
        <source>Default file name is sha3_224. The sha3_224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1612"/>
        <source>The sha3_224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1646"/>
        <source>Default file name is sha3_256. The sha3_256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1650"/>
        <source>The sha3_256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1684"/>
        <source>Default file name is sha3_384. The sha3_384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1688"/>
        <source>The sha3_384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1722"/>
        <source>Default file name is sha3_512. The sha3_512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1726"/>
        <source>The sha3_512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1760"/>
        <source>Default file name is keccak_224. The keccak_224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1764"/>
        <source>The keccak_224 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1800"/>
        <source>Default file name is keccak_256. The keccak_256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1804"/>
        <source>The keccak_256 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1838"/>
        <source>Default file name is keccak_384. The keccak_384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1842"/>
        <source>The keccak_384 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1876"/>
        <source>Default file name is keccak_512. The keccak_512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1880"/>
        <source>The keccak_512 algorithm will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1959"/>
        <source>No hash sum could be calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1963"/>
        <source>hash sum has been calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1968"/>
        <source>hash sums have been calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1990"/>
        <location filename="../hash.cpp" line="2536"/>
        <location filename="../hash.cpp" line="3896"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2026"/>
        <source>Open the first file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2027"/>
        <source>Open two files to compare their hash sums.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2052"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2053"/>
        <source>You must select exactly two files. Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2070"/>
        <location filename="../hash.cpp" line="2071"/>
        <source>Open the second file to compare with.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2088"/>
        <location filename="../hash.cpp" line="2089"/>
        <source>You must select exactly one file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2100"/>
        <location filename="../hash.cpp" line="2101"/>
        <source>You have compared the same file with itself.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2116"/>
        <location filename="../hash.cpp" line="2117"/>
        <source>The files could not be compared.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2142"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2173"/>
        <location filename="../hash.cpp" line="3822"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2185"/>
        <location filename="../hash.cpp" line="2210"/>
        <location filename="../hash.cpp" line="2236"/>
        <location filename="../hash.cpp" line="2262"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2186"/>
        <location filename="../hash.cpp" line="2211"/>
        <location filename="../hash.cpp" line="2237"/>
        <location filename="../hash.cpp" line="2263"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2297"/>
        <source>Default file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2301"/>
        <location filename="../hash.cpp" line="2308"/>
        <location filename="../hash.cpp" line="3721"/>
        <source> (Click to change)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2332"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2383"/>
        <location filename="../hash.cpp" line="2405"/>
        <location filename="../hash.cpp" line="2423"/>
        <source>Did you select all files you want to choose?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2384"/>
        <location filename="../hash.cpp" line="2406"/>
        <location filename="../hash.cpp" line="2424"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2385"/>
        <location filename="../hash.cpp" line="2407"/>
        <location filename="../hash.cpp" line="2425"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2444"/>
        <source>Files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2445"/>
        <source>Save the Debian md5sums file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2479"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2485"/>
        <location filename="../hash.cpp" line="2656"/>
        <location filename="../hash.cpp" line="2970"/>
        <location filename="../hash.cpp" line="3039"/>
        <source> No hash sum could be calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2545"/>
        <location filename="../hash.cpp" line="2546"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2561"/>
        <source>This is not a valid path to build a Debian package!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2647"/>
        <source>Files in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2665"/>
        <location filename="../hash.cpp" line="2666"/>
        <location filename="../hash.cpp" line="2907"/>
        <location filename="../hash.cpp" line="2909"/>
        <location filename="../hash.cpp" line="2977"/>
        <location filename="../hash.cpp" line="2994"/>
        <location filename="../hash.cpp" line="2995"/>
        <location filename="../hash.cpp" line="3046"/>
        <location filename="../hash.cpp" line="3055"/>
        <source>Hash sum for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2665"/>
        <location filename="../hash.cpp" line="2666"/>
        <location filename="../hash.cpp" line="2907"/>
        <location filename="../hash.cpp" line="2909"/>
        <location filename="../hash.cpp" line="2977"/>
        <location filename="../hash.cpp" line="2994"/>
        <location filename="../hash.cpp" line="2995"/>
        <location filename="../hash.cpp" line="3046"/>
        <location filename="../hash.cpp" line="3055"/>
        <source> of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2665"/>
        <location filename="../hash.cpp" line="2666"/>
        <location filename="../hash.cpp" line="2977"/>
        <location filename="../hash.cpp" line="3046"/>
        <source> files was successfully calculated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2667"/>
        <location filename="../hash.cpp" line="2891"/>
        <location filename="../hash.cpp" line="2978"/>
        <location filename="../hash.cpp" line="3047"/>
        <source>Copyright </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2692"/>
        <source>Open the old hash sum file to compare with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2694"/>
        <location filename="../hash.cpp" line="2928"/>
        <source>Text Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2719"/>
        <source>Hash sum could not be estimated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2729"/>
        <source>Could not open </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2729"/>
        <source> with hash sums. Make sure the file exists and check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2730"/>
        <source>Could not open &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2730"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2745"/>
        <location filename="../hash.cpp" line="2746"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2863"/>
        <source>Unchanged files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2870"/>
        <source>Changed files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2877"/>
        <source>Files not tested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2884"/>
        <source>New files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2907"/>
        <source>Hash Sum values have been compared to old values in the file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2908"/>
        <location filename="../hash.cpp" line="2993"/>
        <location filename="../hash.cpp" line="3054"/>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2907"/>
        <location filename="../hash.cpp" line="2909"/>
        <location filename="../hash.cpp" line="2995"/>
        <location filename="../hash.cpp" line="3055"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2909"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2929"/>
        <source>Save the file with the hash sums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2994"/>
        <source> files was successfully calculated. Algorithm: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3112"/>
        <source>&quot; has hash sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3347"/>
        <location filename="../hash.cpp" line="3353"/>
        <source>&quot; and &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3347"/>
        <location filename="../hash.cpp" line="3353"/>
        <source>&quot; have these hash sums:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3347"/>
        <source>. The files are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3346"/>
        <location filename="../hash.cpp" line="3348"/>
        <source>The files are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3353"/>
        <source>. The files are NOT identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3352"/>
        <location filename="../hash.cpp" line="3354"/>
        <source>The files are NOT identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3357"/>
        <location filename="../hash.cpp" line="3361"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3931"/>
        <location filename="../hash.cpp" line="3988"/>
        <source>Save the hash summary list as a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3931"/>
        <location filename="../hash.cpp" line="3988"/>
        <source>Any file (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>Open the file you want to find duplicates for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source>I have calculated the hash sum of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="207"/>
        <source>Identical files with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="209"/>
        <source>Recursively searched from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source>I found </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source> duplicates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source>This program uses Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source> running on </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="40"/>
        <location filename="../system.cpp" line="44"/>
        <location filename="../system.cpp" line="48"/>
        <location filename="../system.cpp" line="124"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="56"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="120"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="129"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
