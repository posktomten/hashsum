<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="48"/>
        <location filename="../checkforupdates.cpp" line="110"/>
        <location filename="../checkforupdates.cpp" line="113"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen Internet-anslutning hittades.
Kontrollera dina Internet-inställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <location filename="../checkforupdates.cpp" line="119"/>
        <source>Where is a new version of hashSum. Latest Version: </source>
        <translation>Det finns en ny verion av hashSum. Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="55"/>
        <source>, Please download. </source>
        <translation>, Vänligen ladda ner. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>&lt;br&gt;There is a new version of </source>
        <translation>&lt;br&gt;Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <source>.&lt;br&gt;Latest version: </source>
        <translation>.&lt;br&gt;Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source> is newer than the latest official version </source>
        <translation> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="71"/>
        <source>You have the latest version of hashsum.</source>
        <translation>Du har senaste versionen av hashSum.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="79"/>
        <location filename="../checkforupdates.cpp" line="125"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det blev fel när versionen kollades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="122"/>
        <source>Latest Version </source>
        <translation>Senaste version </translation>
    </message>
    <message>
        <source>
There is a later version of </source>
        <oldsource>
There is a newer version of </oldsource>
        <translation type="vanished">
Det finns en senare version av </translation>
    </message>
    <message>
        <source>Latest version: </source>
        <translation type="vanished">Senaste versionen: </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Ladda ner från</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="45"/>
        <source>No Internet connection was found. Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades. Kontrollera dina internetinställningar och din brandvägg.</translation>
    </message>
    <message>
        <source>Where is a newer version of hashSum, Please download.</source>
        <translation type="vanished">Det finns en nyare version av hashSum, vänligen ladda ner.</translation>
    </message>
    <message>
        <source>Please </source>
        <translation type="vanished">Vänligen </translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="vanished">Ladda ner</translation>
    </message>
    <message>
        <source>
There is a newer version of </source>
        <translation type="vanished">
Det finns en nyare version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="63"/>
        <source>Your version of hashsum is newer than the latest official version.</source>
        <translation>Din version av hashSum är nyare än den senaste officiella versionen.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <source>
Your version of </source>
        <translation>
Din version av </translation>
    </message>
    <message>
        <source> is later than the latest official version </source>
        <translation type="vanished"> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="74"/>
        <source>
You have the latest version of </source>
        <translation>
Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="82"/>
        <location filename="../checkforupdates.cpp" line="128"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det blev ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="165"/>
        <source>New updates:</source>
        <translation>Nya uppdateringar:</translation>
    </message>
</context>
<context>
    <name>ExtQLineEdit</name>
    <message>
        <location filename="../extqlineedit.cpp" line="36"/>
        <location filename="../extqlineedit.cpp" line="48"/>
        <source>Mission</source>
        <translation>Uppdrag</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="39"/>
        <location filename="../extqlineedit.cpp" line="49"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumman har kopierats till klippbordet</translation>
    </message>
    <message>
        <location filename="../extqlineedit.cpp" line="50"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Dubbelklicka för att kopiera till klippbordet</translation>
    </message>
</context>
<context>
    <name>Hash</name>
    <message>
        <location filename="../hash.ui" line="14"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1057"/>
        <location filename="../hash.cpp" line="1058"/>
        <source>Double-click to copy to clipboard</source>
        <translation>Dubbelklicka för att kopiera till klippbordet</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="77"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="102"/>
        <source>&amp;Algorithm</source>
        <translation>&amp;Algoritm</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="122"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="140"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="152"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="161"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigera</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="170"/>
        <source>&amp;Text</source>
        <translation>&amp;Text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="424"/>
        <source>Hash sum</source>
        <oldsource>Checksum</oldsource>
        <translation>Hashsumma</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">Kolla efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="81"/>
        <source>Recent Files</source>
        <translation>Senast öppnade filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="177"/>
        <source>Speech</source>
        <translation>Tal</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="197"/>
        <source>Check file or files</source>
        <oldsource>Check file</oldsource>
        <translation>Kontrollera fil eller filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="209"/>
        <source>Check for updates...</source>
        <translation>Kolla efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="218"/>
        <location filename="../hash.ui" line="221"/>
        <location filename="../hash.ui" line="224"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="232"/>
        <source>Check for program updates at program start</source>
        <translation>Kolla efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="244"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="247"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="255"/>
        <source>MD4</source>
        <translation>MD4</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="263"/>
        <source>MD5</source>
        <translation>MD5</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="271"/>
        <source>SHA1</source>
        <translation>SHA1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="276"/>
        <source>Compare two files</source>
        <oldsource>Compare</oldsource>
        <translation>Jämför två filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="285"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="294"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="299"/>
        <location filename="../hash.ui" line="302"/>
        <source>Create hash sum list</source>
        <oldsource>Create hash sum list 44</oldsource>
        <translation>Skapa hashsummalista</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="310"/>
        <source>Save hash sum list where the files are</source>
        <translatorcomment>Spara hashsummalistan där filerna finns</translatorcomment>
        <translation>Spara hashsummalistan där filerna är</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="320"/>
        <source>Create hash sum lists, recursively</source>
        <translation>Skapa hashsummalistor, rekursivt</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="328"/>
        <source>Write date and time in the hash sum file</source>
        <oldsource>Write time and date in the hash sum file</oldsource>
        <translation>Skriv datum och tid i hashsummafilen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="337"/>
        <source>Find changes</source>
        <translation>Hitta förändringar</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="345"/>
        <source>Use native dialogs</source>
        <translation>Använd operativsystemets dialogrutor</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="353"/>
        <source>Allways open the home directory</source>
        <translation>Öppna alltid hemkatalogen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="358"/>
        <location filename="../hash.ui" line="429"/>
        <source>Compare with a given hash sum</source>
        <translation>Jämför med en given hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="366"/>
        <source>Save as text (*.*)</source>
        <oldsource>Save as text (*)</oldsource>
        <translation>Spara som text (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="374"/>
        <source>Save as pdf (*.pdf)</source>
        <translation>Spara som pdf (*.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="382"/>
        <source>Color pdf</source>
        <oldsource>Print Color pdf</oldsource>
        <translation>pdf i färg</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="394"/>
        <source>Copy Path</source>
        <translation>Kopiera sökvägen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="406"/>
        <source>Copy Hash Sum</source>
        <translation>Kopiera hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="414"/>
        <source>Save as text and pdf (*.* and *.pdf)</source>
        <oldsource>Save as text and pdf (* and *.pdf)</oldsource>
        <translation>Spara som text och pdf (*.* och *.pdf)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="419"/>
        <source>Hash sum from text</source>
        <translation>Hashsumma från text</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="438"/>
        <location filename="../hash.ui" line="441"/>
        <location filename="../hash.ui" line="444"/>
        <source>Help...</source>
        <oldsource>Help</oldsource>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="450"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="459"/>
        <source>Create Debian md5sums</source>
        <oldsource>Create debian md5sums</oldsource>
        <translation>Skapa Debian md5sums</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="467"/>
        <source>Show full path in the hash sum file</source>
        <translation>Visa hela sökvägen i hashsummefilen</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="476"/>
        <source>Create Debian md5sums (auto)</source>
        <translation>Skapa Debian md5sums (automatiskt)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="485"/>
        <source>Greek (Not complete)</source>
        <translation>Grekiska (Inte komplett)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="561"/>
        <source>License...</source>
        <translation>Licens...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="570"/>
        <source>Version history...</source>
        <translation>Versionshistorik...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="589"/>
        <source>German (Not complete)</source>
        <translation>Tyska (Inte fullständig)</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="659"/>
        <location filename="../hash.cpp" line="308"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="670"/>
        <source>Mainteance Tool...</source>
        <translation>Underhållsverktyget...</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="681"/>
        <source>Desktop Shortcut</source>
        <translation>Genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="701"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="715"/>
        <source>Silent</source>
        <translation>Tyst</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="724"/>
        <source>About Qt...</source>
        <translation>Om Qt...</translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="vanished">Grekiska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="493"/>
        <source>SHA224</source>
        <translation>SHA224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="501"/>
        <source>SHA256</source>
        <translation>SHA256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="509"/>
        <source>SHA384</source>
        <translation>SHA384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="517"/>
        <source>SHA512</source>
        <translation>SHA512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="525"/>
        <source>SHA3_224</source>
        <translation>SHA3_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="533"/>
        <source>SHA3_256</source>
        <translation>SHA3_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="541"/>
        <source>SHA3_384</source>
        <translation>SHA3_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="552"/>
        <source>SHA3_512</source>
        <translation>SHA3_512</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Licens</translation>
    </message>
    <message>
        <source>Version history</source>
        <translation type="vanished">Versionshistorik</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="575"/>
        <source>Open Hash Sum List</source>
        <translation>Öppna hashsummelistan</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="580"/>
        <source>Open Comparison list</source>
        <translation>Öppna jämförelselistan</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Tyska</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="597"/>
        <source>Save to file</source>
        <translation>Spara till fil</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="605"/>
        <source>Keccak_224</source>
        <translation>Keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="613"/>
        <source>Keccak_256</source>
        <translation>Keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="621"/>
        <source>Keccak_384</source>
        <translation>Keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="629"/>
        <source>Keccak_512</source>
        <translation>Keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="634"/>
        <source>Find identical files</source>
        <translation>Sök efter identiska filer</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="639"/>
        <source>Are there duplicates?</source>
        <translation>Finns det dubletter?</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="647"/>
        <source>Always open the display window</source>
        <translation>Öppna alltid visningsfönstret</translation>
    </message>
    <message>
        <location filename="../hash.ui" line="315"/>
        <source>Default file name</source>
        <translation>Standard filnamn</translation>
    </message>
    <message>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <oldsource>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </oldsource>
        <translation type="vanished">Detta program är fri programvara; du kan distribuera det och/eller modifiera det under villkoren i GNU General Public License, publicerad av Free Software Foundation, antingen version </translation>
    </message>
    <message>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation type="vanished"> av licensen, eller (om du så vill) någon senare version Detta program distribueras i hopp om att det ska vara användbart, men UTAN NÅGON GARANTI, även utan underföstådd garanti om SÄLJBARHET eller LÄMPLIGHET FÖR ETT VISST SYFTE Se GNU General Public License för fler detaljer. Du bör ha fått en kopia av GNU General Public License tillsammans med detta program, om inte, skriv till Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <source>Phone: </source>
        <translation type="vanished">Telefon: </translation>
    </message>
    <message>
        <source>About </source>
        <translation type="vanished">Om </translation>
    </message>
    <message>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation type="vanished">&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1334"/>
        <location filename="../hash.cpp" line="1370"/>
        <location filename="../hash.cpp" line="1406"/>
        <location filename="../hash.cpp" line="1442"/>
        <location filename="../hash.cpp" line="1478"/>
        <location filename="../hash.cpp" line="1514"/>
        <location filename="../hash.cpp" line="1550"/>
        <location filename="../hash.cpp" line="1586"/>
        <location filename="../hash.cpp" line="1622"/>
        <location filename="../hash.cpp" line="1658"/>
        <location filename="../hash.cpp" line="1694"/>
        <location filename="../hash.cpp" line="1730"/>
        <location filename="../hash.cpp" line="1768"/>
        <location filename="../hash.cpp" line="1804"/>
        <location filename="../hash.cpp" line="1840"/>
        <location filename="../hash.cpp" line="1973"/>
        <location filename="../hash.cpp" line="2628"/>
        <location filename="../hash.cpp" line="2944"/>
        <location filename="../hash.cpp" line="3014"/>
        <location filename="../hash.cpp" line="3099"/>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <location filename="../hash.cpp" line="3708"/>
        <location filename="../hash.cpp" line="3883"/>
        <location filename="../hash.cpp" line="4003"/>
        <source>Algorithm: </source>
        <translation>Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1354"/>
        <location filename="../hash.cpp" line="1390"/>
        <location filename="../hash.cpp" line="1426"/>
        <location filename="../hash.cpp" line="1462"/>
        <location filename="../hash.cpp" line="1498"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1570"/>
        <location filename="../hash.cpp" line="1606"/>
        <location filename="../hash.cpp" line="1642"/>
        <location filename="../hash.cpp" line="1678"/>
        <location filename="../hash.cpp" line="1714"/>
        <location filename="../hash.cpp" line="1750"/>
        <location filename="../hash.cpp" line="1788"/>
        <location filename="../hash.cpp" line="1824"/>
        <location filename="../hash.cpp" line="1860"/>
        <location filename="../hash.cpp" line="2284"/>
        <location filename="../hash.cpp" line="2291"/>
        <location filename="../hash.cpp" line="3707"/>
        <source>Default file name: </source>
        <translation>Standard filnamn: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1092"/>
        <location filename="../hash.cpp" line="1888"/>
        <location filename="../hash.cpp" line="1889"/>
        <source>Open a file to calculate the hash sum</source>
        <oldsource>Open a file to calculate the checksum</oldsource>
        <translation>Öppna en fil för att beräkna hashsumman</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="836"/>
        <location filename="../hash.cpp" line="1094"/>
        <location filename="../hash.cpp" line="1891"/>
        <location filename="../hash.cpp" line="2011"/>
        <location filename="../hash.cpp" line="2055"/>
        <location filename="../hash.cpp" line="2125"/>
        <location filename="../hash.cpp" line="2317"/>
        <location filename="../isthereaduplicate.cpp" line="45"/>
        <source>All Files (*)</source>
        <oldsource>All Files (*.*)</oldsource>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1126"/>
        <location filename="../hash.cpp" line="1127"/>
        <location filename="../hash.cpp" line="1930"/>
        <location filename="../hash.cpp" line="1931"/>
        <location filename="../hash.cpp" line="1942"/>
        <location filename="../hash.cpp" line="2562"/>
        <location filename="../hash.cpp" line="3846"/>
        <location filename="../hash.cpp" line="3847"/>
        <location filename="../hash.cpp" line="3855"/>
        <location filename="../hash.cpp" line="3856"/>
        <location filename="../isthereaduplicate.cpp" line="80"/>
        <location filename="../isthereaduplicate.cpp" line="81"/>
        <source>No hash sum could be calculated</source>
        <oldsource>No checksum could be calculated</oldsource>
        <translation>Ingen hashsumma kunde beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2098"/>
        <location filename="../hash.cpp" line="2099"/>
        <source>The files could not be compared.</source>
        <translation>Filerna kunde inte jämföras.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2315"/>
        <source>Open Files to Create a List of Hash Sums</source>
        <oldsource>Open Files to Create a List of Checksums</oldsource>
        <translation>Öppna filer för att skapa en lista med hashsummor</translation>
    </message>
    <message>
        <source> You have chosen to use native dialogs. It may not work with your operating system, Linux. You have the option to uncheck &quot;Use native dialogs&quot; in the tools menu.</source>
        <translation type="vanished"> Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem, Linux. Du har möjlighet att bocka ur &quot;Använd operativsystemets dialogrutor&quot; på verktygsmenyn.</translation>
    </message>
    <message>
        <source>You have chosen to use native dialogs. It may not work with your operating system, Linux. You have the option to uncheck &quot;Use native dialogs&quot; in the tools menu.</source>
        <translation type="vanished">Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem, Linux. Du har möjlighet att bocka ur &quot;Använd operativsystemets dialogrutor&quot; på verktygsmenyn.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="46"/>
        <location filename="../isthereaduplicate.cpp" line="94"/>
        <source>Select a folder to Find identical files</source>
        <translation>Välj en folder för att söke efter identiska filer</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="48"/>
        <location filename="../isthereaduplicate.cpp" line="96"/>
        <source>All Files (*.*)</source>
        <translation>Alla filer (*.*)</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="83"/>
        <location filename="../duplicates.cpp" line="84"/>
        <location filename="../isthereaduplicate.cpp" line="124"/>
        <location filename="../isthereaduplicate.cpp" line="125"/>
        <source>Could not check the hash sum. Check your file permissions.</source>
        <translation>Kunde inte kolla hashsumman. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>I have been looking into </source>
        <translation>Jag har letat i </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1972"/>
        <location filename="../hash.cpp" line="2520"/>
        <location filename="../hash.cpp" line="3882"/>
        <location filename="../isthereaduplicate.cpp" line="162"/>
        <source>Path: </source>
        <translation>Sökväg: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="119"/>
        <source>Path: &quot;</source>
        <translation>Sökväg: &quot;</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="130"/>
        <location filename="../duplicates.cpp" line="131"/>
        <location filename="../isthereaduplicate.cpp" line="173"/>
        <location filename="../isthereaduplicate.cpp" line="174"/>
        <source>I have checked all folders and checked all hash sums! Mission accomplished.</source>
        <translation>Jag har kollat i alla foldrar och kollat alla filer! Uppdraget är utfört.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="239"/>
        <location filename="../isthereaduplicate.cpp" line="196"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="241"/>
        <source>Recursively from </source>
        <translation>Rekursivt från </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="264"/>
        <location filename="../duplicates.cpp" line="265"/>
        <source>The number of files with one or more duplicates: </source>
        <translation>Antalet filer med en eller flera dubletter: </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="269"/>
        <location filename="../isthereaduplicate.cpp" line="222"/>
        <source>Duplicate files</source>
        <translation>Identiska filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2469"/>
        <location filename="../hash.cpp" line="2640"/>
        <location filename="../hash.cpp" line="2956"/>
        <location filename="../hash.cpp" line="3026"/>
        <source> No hash sum could be calculated</source>
        <translation> Ingen hashsumma kunde beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3033"/>
        <location filename="../hash.cpp" line="3042"/>
        <source>Hash sum for </source>
        <translation>Hashsumma för </translation>
    </message>
    <message>
        <source>Text Files(*)</source>
        <oldsource>Text Files(*.txt)</oldsource>
        <translation type="obsolete">Textfiler(*.)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="3033"/>
        <source> files was successfully calculated</source>
        <translation> filer kunde framgångsrikt beräknas</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <source> folder for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation> mapp efter filer. Jag har lyckats att beräkna hashsumman i alla mappar som innehåller filer.
</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source> folders for files. I have managed to calculate the hash sum in all folders containing files.
</source>
        <translation> mappar efter filer. Jag har lyckats att beräkna hashsumman i alla mappar som innehåller filer.
</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2529"/>
        <location filename="../hash.cpp" line="2530"/>
        <source>I have checked all folders and created all the files! Mission accomplished.</source>
        <translation>Jag har kollat alla foldrar och skapat alla filer! Uppdraget slutfört.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2651"/>
        <location filename="../hash.cpp" line="2876"/>
        <location filename="../hash.cpp" line="2964"/>
        <location filename="../hash.cpp" line="3034"/>
        <source>Copyright </source>
        <translation>Copyright </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="240"/>
        <location filename="../hash.cpp" line="2654"/>
        <location filename="../hash.cpp" line="2879"/>
        <location filename="../hash.cpp" line="2967"/>
        <location filename="../hash.cpp" line="3037"/>
        <location filename="../isthereaduplicate.cpp" line="205"/>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2862"/>
        <source>Files not tested</source>
        <translation>Ej testade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2893"/>
        <location filename="../hash.cpp" line="2979"/>
        <location filename="../hash.cpp" line="3041"/>
        <source>File: </source>
        <translation>Fil: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2009"/>
        <source>Open two files to compare their hash sums.</source>
        <translation>Öppna två filer för att jämföra deras hashsummor.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2123"/>
        <source>Select a folder to recursively generate hash sum lists</source>
        <translation>Välj en földer för att rekursivt generera hashsummalistor</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="868"/>
        <location filename="../hash.cpp" line="2154"/>
        <location filename="../hash.cpp" line="2155"/>
        <location filename="../hash.cpp" line="2454"/>
        <location filename="../hash.cpp" line="2843"/>
        <location filename="../hash.cpp" line="2939"/>
        <location filename="../hash.cpp" line="3009"/>
        <location filename="../hash.cpp" line="3807"/>
        <source>Could not save a file to store hash sums in. Check your file permissions.</source>
        <oldsource>Could not open a file to store hash sums in. Check your file permissions.</oldsource>
        <translation>Kan inte spara en fil med hashsummor. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="112"/>
        <location filename="../duplicates.cpp" line="115"/>
        <location filename="../duplicates.cpp" line="123"/>
        <location filename="../duplicates.cpp" line="126"/>
        <location filename="../hash.cpp" line="2514"/>
        <location filename="../hash.cpp" line="2517"/>
        <location filename="../hash.cpp" line="2524"/>
        <location filename="../hash.cpp" line="2526"/>
        <location filename="../isthereaduplicate.cpp" line="155"/>
        <location filename="../isthereaduplicate.cpp" line="158"/>
        <location filename="../isthereaduplicate.cpp" line="166"/>
        <location filename="../isthereaduplicate.cpp" line="169"/>
        <source>(Algorithm: </source>
        <translation>(Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2649"/>
        <location filename="../hash.cpp" line="2650"/>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2963"/>
        <location filename="../hash.cpp" line="2980"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3033"/>
        <location filename="../hash.cpp" line="3042"/>
        <source> of </source>
        <translation> av </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2914"/>
        <source>Save the file with the hash sums</source>
        <translation>Spara filen med hashsummorna</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2892"/>
        <location filename="../hash.cpp" line="2894"/>
        <location filename="../hash.cpp" line="2981"/>
        <location filename="../hash.cpp" line="3042"/>
        <source> files was successfully calculated (Algorithm: </source>
        <translation> filer blev framgångsrikt beräknade (Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source>Welcome to hashSum. I use the </source>
        <translation>Välkommen till hashSum. Jag använder </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="54"/>
        <source> algorithm when calculating hash sums.</source>
        <translation> algoritmen när jag beräknar hashsummor.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="72"/>
        <source>My name is </source>
        <translation>Mitt namn är </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="102"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="302"/>
        <source>To the website</source>
        <translation>Till webbsidan</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="305"/>
        <source>Download the latest version</source>
        <translation>Ladda ner senaste versionen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="310"/>
        <source>Download the new version</source>
        <translation>Ladda ner den nya versionen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="331"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Ett oväntat felinträffade&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="333"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;kan inten hittas eller är ingen körbar fil.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="397"/>
        <source>&quot;Control Panel\All Control Panel Items\Programs and Features&quot; cannot be found</source>
        <translation>&quot;Kontrollpanelen\Alla objekt på kontrollpanelen\Program och funktioner&quot; kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="412"/>
        <source>MaintenanceTool cannot be found</source>
        <translation>Hittar inte Underhålsverktyget</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="834"/>
        <source>Select the &quot;usr&quot; directory to start create hash sums recursively</source>
        <oldsource>Select the &quot;usr&quot; directory to start create checksums recursively</oldsource>
        <translation>Välj &quot;usr&quot; katalogen för att starta att skapa hashsummor rekursivt</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="884"/>
        <source>md5sums file has been successfully created!</source>
        <translation>md5sums filen har skapats!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="887"/>
        <source>md5sums file has been successfully created in </source>
        <translation>md5sums filen har framgångsrikt skapats i </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="923"/>
        <source>Compare with this hash sum...</source>
        <translation>Jämför med denna hashsumma...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="940"/>
        <source>...with hash sum for this text:</source>
        <translation>... med hashsumma för denna text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="952"/>
        <location filename="../hash.cpp" line="1019"/>
        <source> characters. Please reduce the length of the text.</source>
        <translation> tecken. Vänligen reducera textens längd.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="967"/>
        <source>It is NOT the same as the hash sum you compare to.</source>
        <translation>Det är inte samma hashsumma som du jämför med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="968"/>
        <location filename="../hash.cpp" line="969"/>
        <source>Hash sums are NOT equal!</source>
        <translation>Hashsummarna är INTE lika!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="973"/>
        <source>It is the same as the hash sum you compare to.</source>
        <translation>Det är samma hashsumma som den du jämför med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="974"/>
        <location filename="../hash.cpp" line="975"/>
        <source>Hash sums are equal!</source>
        <translation>Hashsummorna är lika!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="987"/>
        <source> Whitespace is not removed when the hash sums is calculated.</source>
        <translation> Blanktecken är inte borttagna när hashsumman beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1028"/>
        <source>Hashsum could not be calculated. An unknown error has occurred.</source>
        <translation>Hashsuman kunde inte beräknas. Ett okänt fel har uppstått.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1064"/>
        <location filename="../hash.cpp" line="1065"/>
        <source>Hash sum has been copied to the clipboard</source>
        <translation>Hashsumman har kopierats till klippbordet</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1130"/>
        <location filename="../hash.cpp" line="1134"/>
        <source> And the hash sum you compare to is: &quot;</source>
        <translation> Och hashsumman du jämför med är: &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1130"/>
        <source>&quot; and they are the same</source>
        <translation>&quot; och de är lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1131"/>
        <location filename="../hash.cpp" line="1132"/>
        <source>The hash sums are equal</source>
        <translation>Hashsummorna är lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1134"/>
        <source>&quot; and they are NOT the same</source>
        <translation>&quot; och de är INTE lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1135"/>
        <source>The hash sums are not equal</source>
        <translation>Hashsummorna är inte lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1136"/>
        <source>The hash sums are NOT equal</source>
        <translation>Hashsummorna är INTE lika</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1148"/>
        <source>You have chosen to use native dialogs. It may not always work with your operating system.</source>
        <translation>Du har valt att använda operativsystemets dialogrutor. Det kanske inte alltid fungerar med ditt operativsystem.</translation>
    </message>
    <message>
        <source>This is the text to speech edition of hashsum. Version </source>
        <translation type="vanished">Detta är text till tal-versionen av hashsum.Version </translation>
    </message>
    <message>
        <source>Default file name is md4. The md4 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är md4. Algoritmen md4 kommer att användas.</translation>
    </message>
    <message>
        <source>The md4 algorithm will be used.</source>
        <translation type="vanished">Algoritmen md4 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is md5. The md5 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är md5. Algoritmen md5 kommer att användas.</translation>
    </message>
    <message>
        <source>The md5 algorithm will be used.</source>
        <translation type="vanished">Algoritmen md5 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is SHA1. The SHA1 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är SHA1. Algoritmen SHA1 kommer att användas.</translation>
    </message>
    <message>
        <source>The SHA1 algorithm will be used.</source>
        <translation type="vanished">Algoritmen SHA1 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha224. The sha224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha224. Algoritmen sha224 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha256. The sha256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha256. Algoritmen sha256 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha384. The sha384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha384. Algoritmen sha384 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha512. The sha512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha512. Algoritmen sha512 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha512 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_224. The sha3_224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_224. Algoritmen sha3_224 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_256. The sha3_256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_256. Algoritmen sha3_256 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_384. The sha3_384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_384. Algoritmen sha3_384 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is sha3_512. The sha3_512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är sha3_512. Algoritmen sha3_512 kommer att användas.</translation>
    </message>
    <message>
        <source>The sha3_512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen sha3_512 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_224. The keccak_224 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_224. Algoritmen keccak_224 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_224 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_224 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_256. The keccak_256 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_256. Algoritmen keccak_256 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_256 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_256 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_384. The keccak_384 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_384. Algoritmen keccak_384 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_384 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_384 kommer att användas.</translation>
    </message>
    <message>
        <source>Default file name is keccak_512. The keccak_512 algorithm will be used.</source>
        <translation type="vanished">Standardfilnamnet är keccak_512. Algoritmen keccak_512 kommer att användas.</translation>
    </message>
    <message>
        <source>The keccak_512 algorithm will be used.</source>
        <translation type="vanished">Algoritmen keccak_512 kommer att användas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1941"/>
        <source>No hash sum could be calculated.</source>
        <translation>Ingen hashsumma kunde beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1945"/>
        <source>hash sum has been calculated.</source>
        <translation>hashsumman beräknades.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1950"/>
        <source>hash sums have been calculated.</source>
        <translation>hashsummman har beräknats.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2034"/>
        <source>You must select exactly two files.
Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Du måste välja exakt två filer.
Två filer i denna mapp eller en fil i denna mapp och en andra fil i en annan mapp som du väljer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2035"/>
        <source>You must select exactly two files. Two files in this folder or one file in this folder and a second file in another folder of your choice.</source>
        <translation>Du måste välja exakt två filer. Två filer i den här mappen eller en fil i den här mappen och en andra fil i en annan valfri mapp.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2052"/>
        <location filename="../hash.cpp" line="2053"/>
        <source>Open the second file to compare with.</source>
        <translation>Öppna den andra filen att jämföra med.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2070"/>
        <location filename="../hash.cpp" line="2071"/>
        <source>You must select exactly one file.</source>
        <translation>Du måste välja exakt en fil.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2082"/>
        <location filename="../hash.cpp" line="2083"/>
        <source>You have compared the same file with itself.</source>
        <oldsource>You have examined the same file with itself.</oldsource>
        <translation>Du har jämfört samma fil med sig själv.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2168"/>
        <location filename="../hash.cpp" line="2193"/>
        <location filename="../hash.cpp" line="2219"/>
        <location filename="../hash.cpp" line="2245"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet behöver startas om för att språkändringen ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2169"/>
        <location filename="../hash.cpp" line="2194"/>
        <location filename="../hash.cpp" line="2220"/>
        <location filename="../hash.cpp" line="2246"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="306"/>
        <location filename="../hash.cpp" line="925"/>
        <location filename="../hash.cpp" line="942"/>
        <location filename="../hash.cpp" line="1005"/>
        <location filename="../hash.cpp" line="1073"/>
        <location filename="../hash.cpp" line="2170"/>
        <location filename="../hash.cpp" line="2195"/>
        <location filename="../hash.cpp" line="2221"/>
        <location filename="../hash.cpp" line="2247"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1003"/>
        <source>Calculate the hash sum of this text:</source>
        <translation>Beräkna hashsumman av denna text:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="926"/>
        <location filename="../hash.cpp" line="943"/>
        <location filename="../hash.cpp" line="1006"/>
        <location filename="../hash.cpp" line="1074"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="520"/>
        <location filename="../hash.cpp" line="641"/>
        <source> is not found</source>
        <translation> kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="450"/>
        <location filename="../hash.cpp" line="532"/>
        <location filename="../hash.cpp" line="653"/>
        <source>Open the hashSum file</source>
        <translation>Öppna hashSummefilen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="452"/>
        <location filename="../hash.cpp" line="534"/>
        <location filename="../hash.cpp" line="655"/>
        <source>Text files (*)</source>
        <translation>Textfiler (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="485"/>
        <location filename="../hash.cpp" line="568"/>
        <location filename="../hash.cpp" line="608"/>
        <location filename="../hash.cpp" line="696"/>
        <location filename="../hash.cpp" line="731"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="761"/>
        <source>The number of recently opened files to be displayed...</source>
        <oldsource>Set number of recently opened files shown...</oldsource>
        <translation>Antalet nyligen öppnade filer som skall visas...</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="764"/>
        <source>Clear the list of recently opened files</source>
        <translation>Rensa listan med nyligen öppnade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="771"/>
        <source>The file</source>
        <translation>Filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="771"/>
        <source>can not be found. The file will be removed from the list of recently opened files.</source>
        <translation>kan inte hittas. Filen kommer att tas bort från listan över nyligen öppnade filer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="800"/>
        <source>Set number of Recent Files: </source>
        <oldsource>Set number of Recent Files: (0-30)</oldsource>
        <translation>Ställ in antalet senast öppnade filer som visas: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="869"/>
        <location filename="../hash.cpp" line="2455"/>
        <location filename="../hash.cpp" line="2844"/>
        <location filename="../hash.cpp" line="2940"/>
        <location filename="../hash.cpp" line="3010"/>
        <location filename="../hash.cpp" line="3808"/>
        <location filename="../hash.cpp" line="3938"/>
        <location filename="../hash.cpp" line="3939"/>
        <location filename="../hash.cpp" line="3995"/>
        <location filename="../hash.cpp" line="3996"/>
        <source>Could not save a file to store hash sums in. Check your file permissions. </source>
        <translation>Det gick inte att spara filen för att lagra hash-summor. Kontrollera dina filbehörigheter. </translation>
    </message>
    <message>
        <source>Help with the program and all settings and menus can be found here:</source>
        <oldsource>Help with the program and all settings and menus can be found here:&lt;br&gt;</oldsource>
        <translation type="vanished">Hjälp med programmet och alla inställningar och menyer hittar du här:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="914"/>
        <source>The program&apos;s website can be found here:</source>
        <translation>Programmets hemsida hittar du här:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="952"/>
        <location filename="../hash.cpp" line="953"/>
        <location filename="../hash.cpp" line="1019"/>
        <location filename="../hash.cpp" line="1020"/>
        <source> can handle texts up to </source>
        <oldsource> can handle texts of up to </oldsource>
        <translation> kan hantera texter upp till </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="953"/>
        <location filename="../hash.cpp" line="1020"/>
        <source> characters.
Please reduce the length of the text.</source>
        <translation> tecken.
Minska längden på texten.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="961"/>
        <location filename="../hash.cpp" line="962"/>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="990"/>
        <location filename="../hash.cpp" line="992"/>
        <location filename="../hash.cpp" line="1029"/>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1040"/>
        <location filename="../hash.cpp" line="1043"/>
        <location filename="../hash.cpp" line="1045"/>
        <source>Hash sum calculated for:</source>
        <translation>Hashsumma som beräknas för:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="961"/>
        <location filename="../hash.cpp" line="962"/>
        <location filename="../hash.cpp" line="1029"/>
        <source>could not be calculated. An unknown error has occurred.</source>
        <translation>kunde inte beräknas. Ett okänt fel har inträffat.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="985"/>
        <location filename="../hash.cpp" line="990"/>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1043"/>
        <source>characters.</source>
        <translation>tecken.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="987"/>
        <location filename="../hash.cpp" line="992"/>
        <location filename="../hash.cpp" line="1040"/>
        <location filename="../hash.cpp" line="1045"/>
        <source>characters</source>
        <translation>tecken</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="995"/>
        <source>Hash sums are compared!</source>
        <translation>Hashsummorna är jämförda!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1038"/>
        <location filename="../hash.cpp" line="1040"/>
        <source>Whitespace is not removed when the hash sums is calculated.</source>
        <translation>Blanktecken är inte borttagna när hashsumman beräknas.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1048"/>
        <location filename="../hash.cpp" line="1049"/>
        <source>Hash sum has been calculated!</source>
        <translation>Hashsumman är beräknad!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1071"/>
        <source>Compare with this hash sum:</source>
        <translation>Jämför med denna hashsumma:</translation>
    </message>
    <message>
        <source>You have chosen to use native dialogs. It may not work with your operating system.</source>
        <translation type="vanished">Du har valt att använda operativsystemets dialogrutor. Det kanske inte fungerar med ditt operativsystem.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1214"/>
        <source>The license file is not found</source>
        <translation>Licensfilen kan inte hittas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1260"/>
        <source>The version history file is not found</source>
        <translation>Versionshistorikfilen hittas inte</translation>
    </message>
    <message>
        <source>This is </source>
        <translation type="vanished">Detta är </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source> for the Greek translation.</source>
        <translation> för den grekiska översättningen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1293"/>
        <source> for the German translation.</source>
        <translation> för den tyska översättningen.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1292"/>
        <source>A program to calculate hash sums and compare files.</source>
        <translation>Ett program för att beräkna hashsummor och för att jämföra filer.</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source>This program uses Qt version </source>
        <translation>Detta program använder Qt version </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="25"/>
        <source> running on </source>
        <translation> som körs på </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="40"/>
        <location filename="../system.cpp" line="44"/>
        <location filename="../system.cpp" line="48"/>
        <location filename="../system.cpp" line="124"/>
        <source> Compiled by</source>
        <translation> Kompilerad med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="56"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1354"/>
        <location filename="../hash.cpp" line="1390"/>
        <location filename="../hash.cpp" line="1426"/>
        <location filename="../hash.cpp" line="1462"/>
        <location filename="../hash.cpp" line="1498"/>
        <location filename="../hash.cpp" line="1534"/>
        <location filename="../hash.cpp" line="1570"/>
        <location filename="../hash.cpp" line="1606"/>
        <location filename="../hash.cpp" line="1642"/>
        <location filename="../hash.cpp" line="1678"/>
        <location filename="../hash.cpp" line="1714"/>
        <location filename="../hash.cpp" line="1750"/>
        <location filename="../hash.cpp" line="1788"/>
        <location filename="../hash.cpp" line="1824"/>
        <location filename="../hash.cpp" line="1860"/>
        <source>(Click to change)</source>
        <translation>(Klicka för att ändra)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1357"/>
        <source>I use the algorithm md4</source>
        <translation>Jag använder algoritmen md4</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1393"/>
        <source>I use the algorithm md5</source>
        <translation>Jag använder algoritmen md5</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1429"/>
        <source>I use the algorithm sha1</source>
        <translation>Jag använder algoritmen sha1</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1465"/>
        <source>I use the algorithm sha224</source>
        <translation>Jag använder algoritmen sha224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1501"/>
        <source>I use the algorithm sha256</source>
        <translation>Jag använder algoritmen sha256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1537"/>
        <source>I use the algorithm sha384</source>
        <translation>Jag använder algoritmen sha384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1573"/>
        <source>I use the algorithm sha512</source>
        <translation>Jag använder algoritmen sha512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1609"/>
        <source>I use the algorithm sha3_224</source>
        <translation>Jag använder algoritmen sha3_224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1645"/>
        <source>I use the algorithm sha3_256</source>
        <translation>Jag använder algoritmen sha3_256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1681"/>
        <source>I use the algorithm sha3_384</source>
        <translation>Jag använder algoritmen sha3_384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1717"/>
        <source>I use the algorithm sha3_512</source>
        <translation>Jag använder algoritmen sha3_512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1753"/>
        <source>I use the algorithm keccak_224</source>
        <translation>Jag använder algoritmen keccak_224</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1791"/>
        <source>I use the algorithm keccak_256</source>
        <translation>Jag använder algoritmen keccak_256</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1827"/>
        <source>I use the algorithm keccak_384</source>
        <translation>Jag använder algoritmen keccak_384</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="1863"/>
        <source>I use the algorithm keccak_512</source>
        <translation>Jag använder algoritmen keccak_512</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2008"/>
        <source>Open the first file</source>
        <translation>Öppna den första filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2165"/>
        <source>Please restart hashsum</source>
        <translation>Vänligen starta om hashsum</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2284"/>
        <location filename="../hash.cpp" line="2291"/>
        <location filename="../hash.cpp" line="3707"/>
        <source> (Click to change)</source>
        <translation> (Klicka för att ändra)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2366"/>
        <location filename="../hash.cpp" line="2388"/>
        <location filename="../hash.cpp" line="2406"/>
        <source>Did you select all files you want to choose?</source>
        <translation>Har du valt alla filer du vill välja?</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2367"/>
        <location filename="../hash.cpp" line="2389"/>
        <location filename="../hash.cpp" line="2407"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2368"/>
        <location filename="../hash.cpp" line="2390"/>
        <location filename="../hash.cpp" line="2408"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2428"/>
        <source>Save the Debian md5sums file</source>
        <translation>Spara Debian md5sums-filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2463"/>
        <source>This is not a valid path to build a debian package!</source>
        <translation>Detta är ingen giltig sökväg för att bygga ett debian-paket!</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <source>Mission accomplished! I have calculated the hash sum of </source>
        <translation>Uppdrag utfört! Jag har beräknat hashsumman för </translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="134"/>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../hash.cpp" line="2533"/>
        <location filename="../isthereaduplicate.cpp" line="177"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source> files.</source>
        <translation> filer.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2545"/>
        <source>This is not a valid path to build a Debian package!</source>
        <oldsource>This is not a valid path to build a debian package!!</oldsource>
        <translation>Detta är inte en giltig sökväg för att bygga ett Debian paket!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2631"/>
        <source>Files in:</source>
        <translation>Filer i:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2676"/>
        <source>Open the old hash sum file to compare with</source>
        <translation>Öppna den gammla hashsummefilen för att jämföra med</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2703"/>
        <source>Hash sum could not be estimated</source>
        <translation>Hashsumman kunde inte beräknas</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source>Could not open </source>
        <translation>Kunde inte öppna </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2713"/>
        <source> with hash sums. Make sure the file exists and check your file permissions.</source>
        <translation> med hashsummor. Kontrollera att filen finns och kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2714"/>
        <source>Could not open &quot;</source>
        <translation>Kunde inte öppna &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2714"/>
        <source>&quot; with hash sums.
Make sure the file exists and check your file permissions.</source>
        <translation>&quot; med hashsummor.
Se till att filen finns och kontrollera dina filbehörigheter.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2729"/>
        <location filename="../hash.cpp" line="2730"/>
        <source>The old hash sum file contains no path to the files. You can not compare!</source>
        <translation>Den gamla hashsummefilen innehåller ingen sökväg till filerna. Du kan inte jämföra!</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2848"/>
        <source>Unchanged files</source>
        <translation>Oförändrade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2855"/>
        <source>Changed files</source>
        <translation>Ändrade filer</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2869"/>
        <source>New files</source>
        <translation>Nya filer</translation>
    </message>
    <message>
        <source>CPU architecture:</source>
        <oldsource>CPU architecture</oldsource>
        <translation type="vanished">CPU-arkitektur:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3917"/>
        <location filename="../hash.cpp" line="3974"/>
        <source>Save the hash summary list as a text file</source>
        <translation>Spara hashlistan som en textfil</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3917"/>
        <location filename="../hash.cpp" line="3974"/>
        <source>Any file (*)</source>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10</source>
        <translation type="vanished">Windows operativsystem version 10.0, motsvararande Windows 10</translation>
    </message>
    <message>
        <source>Text Files (*.*)</source>
        <oldsource>Text Files (*)</oldsource>
        <translation type="obsolete">Textfiler (*.*)</translation>
    </message>
    <message>
        <source>pdf Files (*.pdf)</source>
        <oldsource>PDF Files(*.pdf)</oldsource>
        <translation type="obsolete">pdf filer (*.pdf)</translation>
    </message>
    <message>
        <source>Could not save a file to store Recent File list. Check your file permissions.</source>
        <translation type="vanished">Det gick inte att spara filen som lagrar dina senast öppnade filer. Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <source>Unknown Windows version.</source>
        <translation type="vanished">Okänd Windowsversion.</translation>
    </message>
    <message>
        <source>MS-DOS-based version of Windows.</source>
        <translation type="vanished">MS-DOS baserad version av Windows.</translation>
    </message>
    <message>
        <source>NT-based version of Windows.</source>
        <translation type="vanished">NT baserad version av Windows.</translation>
    </message>
    <message>
        <source>CE-based version of Windows.</source>
        <translation type="vanished">CE baserad version av Windows.</translation>
    </message>
    <message>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation type="vanished">Windows 3.1 med 32-bitars operativsystem.</translation>
    </message>
    <message>
        <source>Windows 95 operating system.</source>
        <translation type="vanished">Windows 95 operativsystem.</translation>
    </message>
    <message>
        <source>Windows 98 operating system.</source>
        <translation type="vanished">Windows 98 operativsystem.</translation>
    </message>
    <message>
        <source>Windows Me operating system.</source>
        <translation type="vanished">Windows ME operativsystem.</translation>
    </message>
    <message>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation type="vanished">Windows operativsystem 4.0, motsvarande Windows NT.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation type="vanished">Windows operativsystem version 5.0, motsvararande Windows 2000.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation type="vanished">Windows operativsystem version 5.1, motsvararande Windows XP.</translation>
    </message>
    <message>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation type="vanished">Windows operativsystem version 5.2, motsvararande Windows Server 2003, Windows Server 2003 R2, Windows Home Server och Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation type="vanished">Windows operativsystem version 6.0, motsvararande Windows Vista och Windows Server 2008.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation type="vanished">Windows operativsystem version 6.1, motsvararande Windows 7 och Windows Server 2008 R2.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.2, corresponds to Windows 8.</source>
        <translation type="vanished">Windows operativsystem version 6.2, motsvararande Windows 8.</translation>
    </message>
    <message>
        <source>Windows operating system version 6.3, corresponds to Windows 8.1.</source>
        <translation type="vanished">Windows operativsystem version 6.2, motsvararande Windows 8.1.</translation>
    </message>
    <message>
        <source>Windows operating system version 10.0, corresponds to Windows 10.</source>
        <translation type="obsolete">Windows operativsystem version 10.0, motsvararande Windows 10.</translation>
    </message>
    <message>
        <source>Windows CE operating system.</source>
        <translation type="vanished">Windows CE operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE .NET operating system.</source>
        <translation type="vanished">Windows CE.NET operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE 5.x operating system.</source>
        <translation type="vanished">Windows CE 5.x operativsystem.</translation>
    </message>
    <message>
        <source>Windows CE 6.x operating system.</source>
        <translation type="vanished">Windows CE 6.x operativsystem.</translation>
    </message>
    <message>
        <source>You seem to be running </source>
        <translation type="obsolete">Du verkar köra </translation>
    </message>
    <message>
        <source>Architecture instruction set: </source>
        <translation type="vanished">Arkitekturens instruktionsuppsättning: </translation>
    </message>
    <message>
        <location filename="../system.cpp" line="27"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompilerad med</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="120"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../system.cpp" line="129"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilatot.</translation>
    </message>
    <message>
        <source>Screen resolution:</source>
        <translation type="vanished">Skärmupplösning:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2427"/>
        <source>Files (*.*)</source>
        <oldsource>Files (*)</oldsource>
        <translation>Filer (*.*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2894"/>
        <source>Hash Sum values have been compared to old values in the file</source>
        <translation>Hashsummorna har jämförts med de gammla värdena i filen</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2678"/>
        <location filename="../hash.cpp" line="2913"/>
        <source>Text Files (*)</source>
        <translation>Textfiler (*)</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2892"/>
        <source>Hash Sum values have been compared to old values in the file </source>
        <translation>Hashsummor har jämförts med gamla värden i filen </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2980"/>
        <source> files was successfully calculated. Algorithm: </source>
        <translation> filer beräknades framgångsrikt. Algoritm: </translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3099"/>
        <source>&quot; has hash sum</source>
        <translation>&quot; har hashsumma</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <source>. The files are identical.</source>
        <translation>. Filerna är identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <source>&quot; have these hash sums:</source>
        <translation>&quot;.har dessa hashsummor:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3333"/>
        <location filename="../hash.cpp" line="3335"/>
        <source>The files are identical.</source>
        <translation>Filerna är identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3340"/>
        <source>. The files are NOT identical.</source>
        <translation>. Filerna är INTE identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3339"/>
        <location filename="../hash.cpp" line="3341"/>
        <source>The files are NOT identical.</source>
        <translation>Filerna är INTE identiska.</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="2280"/>
        <source>Default file name:</source>
        <translation>Standard filnamn:</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3334"/>
        <location filename="../hash.cpp" line="3340"/>
        <source>&quot; and &quot;</source>
        <translation>&quot; och &quot;</translation>
    </message>
    <message>
        <location filename="../hash.cpp" line="3344"/>
        <location filename="../hash.cpp" line="3348"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="43"/>
        <source>Open the file you want to find duplicates for</source>
        <translation>Öppna filen du vill söka efter dubletter till</translation>
    </message>
    <message>
        <location filename="../duplicates.cpp" line="135"/>
        <location filename="../isthereaduplicate.cpp" line="178"/>
        <source>I have calculated the hash sum of </source>
        <translation>Jag har beräknat hashsummorna för </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="207"/>
        <source>Identical files with </source>
        <translation>Identiska filer med </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="209"/>
        <source>Recursively searched from </source>
        <translation>Rekursivt sökt från </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source>I found </source>
        <translation>Jag hittade </translation>
    </message>
    <message>
        <location filename="../isthereaduplicate.cpp" line="217"/>
        <location filename="../isthereaduplicate.cpp" line="218"/>
        <source> duplicates.</source>
        <translation> dubletter.</translation>
    </message>
</context>
<context>
    <name>Lic</name>
    <message>
        <location filename="../license.ui" line="38"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../license.ui" line="81"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../license.ui" line="94"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Öppna fil</translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer att skapas utan ikon.</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="48"/>
        <location filename="../createshortcut.cpp" line="111"/>
        <location filename="../removeshortcut.cpp" line="38"/>
        <location filename="../removeshortcut.cpp" line="60"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="47"/>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer inte att skapas</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="110"/>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation>Genvägen kunde inte skapas.&lt;br&gt;Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="37"/>
        <location filename="../removeshortcut.cpp" line="59"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation> kan inte tas bort.&lt;br&gt;Vänligen kontrollera dina filrättigheter.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="31"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="47"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../about.cpp" line="50"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>Copyright © </source>
        <translation>Copyright © </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>License: </source>
        <translation>Licens: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>distribueras i hopp om att den ska vara användbar, men UTAN NÅGON GARANTI; utan ens underförstådd garanti för SÄLJBARHET eller LÄMPLIGHET FÖR ETT SÄRSKILT SYFTE.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>See the GNU General Public License version 3 for more details.</source>
        <translation>Se GNU General Public License version 3 för mer information.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Website</source>
        <translation>Webbsida</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="55"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Created: </source>
        <translation>Skapad: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Compiled on: </source>
        <translation>Kompilerad på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source> is in the folder:</source>
        <translation> finns i foldern:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="56"/>
        <source>Runs on: </source>
        <translation>Körs på: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="70"/>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Kompilator: MinGW (GCC för Windows) version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="76"/>
        <source>Compiler: Micrsoft Visual C++ version </source>
        <translation>Kompilator: Micrsoft Visual C++ version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="77"/>
        <source>&lt;br&gt;Full version number: </source>
        <translation>&lt;br&gt;Fullständigt versionsnummer: </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <location filename="../about.cpp" line="82"/>
        <location filename="../about.cpp" line="84"/>
        <location filename="../about.cpp" line="86"/>
        <location filename="../about.cpp" line="88"/>
        <source>Programming language: C++</source>
        <translation>Programmeringsspråk: C++</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="80"/>
        <source>C++ version: C++20</source>
        <translation>C++ version: C++20</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="82"/>
        <source>C++ version: C++17</source>
        <translation>C++ version: C++17</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>C++ version: C++14</source>
        <translation>C++ version: C++14</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>C++ version: C++11</source>
        <translation>C++ version: C++11</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="88"/>
        <source>C++ version: Unknown</source>
        <translation>C++ version: Okänd</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <source>Application framework: Qt version </source>
        <translation>Applikationsramverk: Qt version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="92"/>
        <source>Processor architecture: 32-bit</source>
        <translation>Processorarkitektur: 32-bitar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="95"/>
        <source>Processor architecture: 64-bit</source>
        <translation>Processorarkitektur: 64-bitar</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licens:</translation>
    </message>
    <message>
        <source> GPL Version 3</source>
        <translation type="vanished"> GPL Version 3</translation>
    </message>
    <message>
        <source>Copyright ©</source>
        <translation type="vanished">Copyright ©</translation>
    </message>
    <message>
        <source>Running on: </source>
        <translation type="vanished">Körs på: </translation>
    </message>
    <message>
        <source> is in the folder</source>
        <translation type="vanished"> finns i mappen</translation>
    </message>
    <message>
        <source>Version: C++20</source>
        <translation type="vanished">Version: C++20</translation>
    </message>
    <message>
        <source>Version: C++17</source>
        <translation type="vanished">Version: C++17</translation>
    </message>
    <message>
        <source>Version: C++14</source>
        <translation type="vanished">Version: C++14</translation>
    </message>
    <message>
        <source>Version: C++11</source>
        <translation type="vanished">Version: C++11</translation>
    </message>
    <message>
        <source>Version: Unknown</source>
        <translation type="vanished">Version: Okänd</translation>
    </message>
</context>
</TS>
