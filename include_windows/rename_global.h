// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          rename
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#if !defined(RENAME_GLOBALH)
#define RENAME_GLOBALH
#include <QtCore/QtGlobal>
#if defined(RENAME_LIBRARY)
#define RENAME_EXPORT Q_DECL_EXPORT
#else
#define RENAME_EXPORT Q_DECL_IMPORT
#endif // RENAME_LIBRARY
#endif // RENAME_GLOBALH

