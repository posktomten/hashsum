// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          rename
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/downloadunpack
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef RENAME_H
#define RENAME_H

#include <QObject>

#if defined(Q_OS_LINUX)
#if defined(RENAME_LIBRARY)
#include "rename_global.h"
class RENAME_EXPORT Rename : public QObject
#else
class Rename : public QObject
#endif // RENAME_LIBRARY
#else
class Rename : public QObject
#endif // Q_OS_LINUX

{
    Q_OBJECT
public:
    explicit Rename(QObject *parent = nullptr);
    bool reName(const QString &oldname, const QString &newname);


signals:

};

#endif // RENAME_H
