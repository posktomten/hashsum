//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CREATESHORTCUT (Windows)
//          Copyright (C) 2022 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcreateshortcut
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#if !defined(CREATECHORTCUT_GLOBALH)
#define CREATECHORTCUT_GLOBALH
#include <QtCore/QtGlobal>

#if defined(CREATECHORTCUT_LIBRARY)
#define CREATECHORTCUT_EXPORT Q_DECL_EXPORT
#else
#define CREATECHORTCUT_EXPORT Q_DECL_IMPORT
#endif // CREATECHORTCUT_LIBRARY
#endif // CREATECHORTCUT_GLOBALH
